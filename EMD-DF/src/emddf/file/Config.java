/*    */ package emddf.file;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Config
/*    */ {
/*    */   public String surf_initial_timestamp;
/*    */   
/*    */ 
/*    */   public String surf_timezone;
/*    */   
/*    */ 
/*    */   public float surf_sampling_rate;
/*    */   
/*    */   public float[] surf_channel_callibration;
/*    */   
/*    */   //public int num_channels;
/*    */   
/*    */   //public int sample_size;
/*    */   
/*    */   //public float sampling_rate;
/*    */   



		public Config() {
				
		}
			
			
			
 
		public Config(String surf_initial_timestamp, String surf_timezone, 
				   float surf_sampling_rate, float[] surf_channel_callibration){
		   
			
			this.surf_initial_timestamp = surf_initial_timestamp;
		    this.surf_timezone = surf_timezone;
		    this.surf_sampling_rate = surf_sampling_rate;
		     
		    //this.num_channels = num_channels;
		    //this.sampling_rate = sampling_rate;
		    //this.sample_size = sample_size;
		     
		    
			// validate the number of channels and calibration constants		 
		    
		    /*if (surf_channel_callibration.length == this.num_channels) {
		       this.surf_channel_callibration = surf_channel_callibration;
		    } else {
		       this.surf_channel_callibration = new float[num_channels];
		      
		      for (int i = 0; i < num_channels; i++) {
		        this.surf_channel_callibration[i] = 1.0F;
		      }
		    }*/
		  
		}

		public boolean isEMDDF() {
			return (this.surf_initial_timestamp != null &&
					this.surf_timezone != null &&
					this.surf_sampling_rate > 0  &&
					this.surf_channel_callibration != null);
		}

			
			/**
			 * This method returns the surf_initial_timestamp of the audio file.
			 * @return surf_initial_timestamp
			 */
			public String getSurfInitialTimestamp() {
				return surf_initial_timestamp;
			}
			
			
			
			/**
			 * This method returns the surf_timezone of the audio file.
			 * @return surf_timezone
			 */
			public String getSurfTimezone() {
				return surf_timezone;
			}
			
			
			
			
			
			
			/**
			 * This method returns the surf_sampling_rate of the audio file.
			 * @return surf_sampling_rate
			 */
			public float getSurfSamplingRate() {
				return surf_sampling_rate;
			}
			
			
			
			
			/**
			 * This method returns the surf_channel_callibration of the audio file.
			 * @return surf_channel_callibration
			 */
			public float[] getSurfChannelCallibration() {
				return surf_channel_callibration;
			}
			
			
			
			
			/**
			 * This method returns the num_channels of the audio file.
			 * @return num_channels
			 */
			//public int getNumChannels() {
			//	return num_channels;
			//}
			
			
			
			
			/**
			 * This method returns the sample_size of the audio file.
			 * @return sample_size
			 */
			//public int getSampleSize() {
			//	return sample_size;
			//}
			
			
			
			
			
			/**
			 * This method returns the sampling_rate of the file.
			 * @return sampling_rate
			 */
			//public float getSamplingRate() {
			//	return sampling_rate;
			//}


}


/* Location:              /Users/lucaspereira/Desktop/emdf.df.jar!/emddf/file/Config.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */