/*    */ package emddf.file;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Info
/*    */ {
/* 11 */   public String file_creator = "";
/* 12 */   public String commissioner = "";
/* 13 */   public String comments = "";
/* 14 */   public String copyright = "";
/* 15 */   public String creation_date = "";
/* 16 */   public String keywords = "";
/* 17 */   public String name = "";
/* 18 */   public String product = "";
/* 19 */   public String subject = "";
/* 20 */   public String software = "";
/* 21 */   public String source = "";
/* 22 */   public String source_form = "";
		   public String archival_location = "";
/*    */   

			/**
			 * Returns how the information will be displayed when printing this object.
			 * It's a string that represents this object (Info).
			 */
/*    */   public String toString()
/*    */   {
/* 26 */     String out = "file_creator: " + this.file_creator + "\n" + 
/* 27 */       "comissioner: " + this.commissioner + "\n" + 
/* 28 */       "comments: " + this.comments + "\n" + 
/* 29 */       "copyright: " + this.copyright + "\n" + 
/* 30 */       "creation_date: " + this.creation_date + "\n" + 
/* 31 */       "keywords: " + this.keywords + "\n" + 
/* 32 */       "name: " + this.name + "\n" + 
/* 33 */       "product: " + this.product + "\n" + 
/* 34 */       "subject: " + this.subject + "\n" + 
/* 35 */       "software: " + this.software + "\n" + 
/* 36 */       "source: " + this.source + "\n" + 
/* 37 */       "source_form: " + this.source_form + "\n" +
			   "archival_location: " + this.archival_location + "\n";
/* 38 */     return out;
/*    */   }



			/**
			 * This method returns the creator of the file.
			 * @return file_creator
			 */
			public String getFileCreator() {
				return file_creator;
			}
			
			
			
			/**
			 * This method returns the comissioner of the audio file.
			 * @return commissioner
			 */
			public String getComissioner() {
				return commissioner;
			}
			
			
			
			
			/**
			 * This method returns the archival location of the audio file.
			 * @return archival_location
			 */
			public String getArchivalLocation() {
				return archival_location;
			}
			
			
			
			
			/**
			 * This method returns the comments of the audio file.
			 * @return comments
			 */
			public String getComments() {
				return comments;
			}
			
			
			
			/**
			 * This method returns the copyright of the audio file.
			 * @return copyright
			 */
			public String getCopyright() {
				return copyright;
			}
			
			
			
			
			/**
			 * This method returns the creation date of the audio file.
			 * @return creation_date
			 */
			public String getCreationDate() {
				return creation_date;
			}
			
			
			
			
			
			/**
			 * This method returns the keywords of the audio file.
			 * @return keywords
			 */
			public String getKeywords() {
				return keywords;
			}
			
			
			
			/**
			 * This method returns the name of the audio file.
			 * @return name
			 */
			public String getName() {
				return name;
			}
			
			
			
			
			/**
			 * This method returns the product of the audio file.
			 * @return product
			 */
			public String getProduct() {
				return product;
			}
			
			
			
			
			
			
			/**
			 * This method returns the subject of the audio file.
			 * @return subject
			 */
			public String getSubject() {
				return subject;
			}
			
			
			
			
			
			/**
			 * This method returns the software of the audio file.
			 * @return software
			 */
			public String getSoftware() {
				return software;
			}
			
			
			
			
			
			/**
			 * This method returns the source of the audio file.
			 * @return source
			 */
			public String getSource() {
				return source;
			}
			
			
			
			
			
			/**
			 * This method returns the source form of the audio file.
			 * @return source_form
			 */
			public String getSourceForm() {
				return source_form;
			}


/*    */ }


/* Location:              /Users/lucaspereira/Desktop/emdf.df.jar!/emddf/file/Info.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */