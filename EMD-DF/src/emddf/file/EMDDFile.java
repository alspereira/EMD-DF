/*
 *  AudioFile.java
 *  (ScissLib)
 *
 *  Copyright (c) 2004-2013 Hanns Holger Rutz. All rights reserved.
 *
 *	This library is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 *	License as published by the Free Software Foundation; either
 *	version 2.1 of the License, or (at your option) any later version.
 *
 *	This library is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public
 *	License along with this library; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *	For further information, please contact Hanns Holger Rutz at
 *	contact@sciss.de
 */


package emddf.file;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import de.sciss.io.AudioFileDescr;
import de.sciss.io.IOUtil;
import de.sciss.io.InterleavedStreamFile;
import de.sciss.io.Marker;
import de.sciss.io.Region;
import de.sciss.io.Span;

//import de.sciss.app.AbstractApplication;

/**
 *	The <code>AudioFile</code> allows reading and writing
 *	of sound files. It wraps a <code>RandomAccessFile</code>
 *	and delegates the I/O to subclasses which deal with
 *	the specific sample format and endianess.
 *	<p>
 *	Currently supported formats are: AIFF, IRCAM,
 *  NeXT/Sun (.au), WAVE, and Wave64. Supported resolutions are
 *  8/16/24/32 bit integer and 32/64 bit floating point.
 *  However not all audio formats support all bit depths.
 *  <p>
 *	Not all format combinations are supported, for example
 *	the rather exotic little-endian AIFF, but also
 *	little-endian SND, WAVE 8-bit.
 *	<p>
 *  In order to simplify communication with CSound,
 *  raw output files are supported, raw input files however
 *  are not recognized.
 *  <p>
 *  To create a new <code>AudioFile</code> you call
 *  one of its static methods <code>openAsRead</code> or
 *  <code>openAsWrite</code>. The format description
 *  is handled by an <code>AudioFileDescr</code> object.
 *	This object also contains information about what special
 *	tags are read/written for which format. For example,
 *	AIFF can read/write markers, and application-specific
 *	chunk, and a gain tag. WAVE can read/write markers and
 *	regions, and a gain tag, etc.
 *	<p>
 *	The <code>AudioFile</code> implements the generic
 *	interface <code>InterleavedStreamFile</code> (which
 *	is likely to be modified in the future) to allow
 *	clients to deal more easily with different sorts
 *	of streaming files, not just audio files.
 *
 *  @author		Hanns Holger Rutz
 *  @version	0.38, 26-Jun-09
 *
 *  @see		AudioFileDescr
 *
 *  TODO		more flexible handling of endianess,
 *				at least SND and IRCAM should support both
 *				versions.
 *
 *	TODO		more tags, like peak information and
 *				channel panning.
 *
 *	TODO		(faster) low-level direct file-to-file
 *				copy in the copyFrames method
 */
public class EMDDFile
implements InterleavedStreamFile {
	private static final int MODE_READONLY   = 0; // Read only access
	private static final int MODE_READWRITE  = 1; // Read and Write access

	protected final RandomAccessFile	raf;
	protected final FileChannel			fch;
	private final int					mode;

	protected EMDDFileDescr				afd;
	private AudioFileHeader				afh;
	
	protected ByteBuffer				byteBuf;
	private int							byteBufCapacity;
	protected int						bytesPerFrame;
	protected int						frameBufCapacity;
	private BufferHandler				bh;
	protected int						channels;
	private long						framePosition;
	
	private long						updateTime;
	private long						updateLen;
	private long						updateStep;
	
	// -------- public Methods --------

	/**
	 *  Opens an audio file for reading.
	 *
	 *  @param		f   the path name of the file
	 *  @return		a new <code>AudioFile</code> object
	 *				whose header is already parsed and can
	 *				be obtained through the <code>getDescr</code> method.
	 *
	 *  @throws IOException if the file was not found, could not be read
	 *						or has an unknown or unsupported format
	 */
	public static EMDDFile openAsRead( File f ) 
	throws IOException {
		// Receives a file in the arguments and creates a SURF File with that file and read only mode.
		final EMDDFile sf	= new EMDDFile( f, MODE_READONLY );
		// Creates the description of the audio file
		sf.afd				= new EMDDFileDescr();
		// Adds the file to the description of the audio file
		sf.afd.file			= f;
		// Adds its type (TYPE_UNKNOWN or TYPE_WAVE) to the description of the audio file
		
		String fileExtension = (String) f.toString().split("\\.")[1];
		
		// Raw audio files doesn't have header, so, it's not possible to retrieve its type
		if (fileExtension.equals("raw")) {
			sf.afd.type 		= sf.getDescr().TYPE_RAW;
			sf.afd.channels = 2;
			sf.afd.bitsPerSample = 16;
			sf.afd.rate = 60;
			sf.afd.sampleFormat = 0;
			sf.afd.length = 60000000;
		} else {
			// TYPE_WAVE or W64 if some conditions verified (header says the type of the file) 
			sf.afd.type			= sf.retrieveType();
		}
		
		
		// Creates the header of the audio file
		sf.afh				= sf.createHeader();
		// Reads all information on the header
		sf.afh.readHeader( sf.afd );
		// Initializes the variables of the audio file (channels, bytesPerFrame, bh, etc)
		sf.init();
		// Moves the file pointer to the index 0 (the method calculates the physical pointer)
		sf.seekFrame( 0 );
		return sf;
	}
	
	/**
	 *  Opens an audio file for reading/writing. The pathname
	 *	is determined by the <code>file</code> field of the provided <code>SURFFileDescr</code>.
	 *	If a file denoted by this path already exists, it will be
	 *	deleted before opening.
	 *	<p>
	 *	Note that the initial audio file header is written immediately.
	 *	Special tags for the header thus need to be set in the <code>SURFFileDescr</code>
	 *	before calling this method, including markers and regions. It is not
	 *	possible to write markers and regions after the file has been opened
	 *	(since the header size has to be constant).
	 *
	 *  @param  afd format and resolution of the new audio file.
	 *				the header is immediately written to the hard-disc
	 *
	 *  @throws IOException if the file could not be created or the
	 *						format is unsupported
	 */
	public static EMDDFile openAsWrite( EMDDFileDescr afd )
	throws IOException {
		// Receives a descriptor (instead of a file) but the descriptor has the file saved.
		// If the file (saved in the descriptor) already exists, it deletes
		if( afd.file.exists() ) afd.file.delete();
		// Creates one audio file (SURFFile) with the file and the read write mode.
		final EMDDFile sf	= new EMDDFile( afd.file, MODE_READWRITE );
		// The descriptor of this audio file is the descriptor sent as an argument (because of the file, its name, etc)
		sf.afd				= afd;
		// The initial length of the audio file is 0
		afd.length			= 0;
		// Creates the header of the audio file
		sf.afh				= sf.createHeader();
		// Writes the header of the audio file
		sf.afh.writeHeader( sf.afd );
		// Initializes the variables of the audio file (channels, bytesPerFrame, bh, etc)
		sf.init();
		// Moves the file pointer to the index 0 (the method calculates the physical pointer)
		sf.seekFrame( 0 );
		// Changes the value of variables
		sf.updateStep		= (long) afd.rate * 20;
		sf.updateLen		= sf.updateStep;
		sf.updateTime		= System.currentTimeMillis() + 10000;
		return sf;
	}
	
	
	
	/**
	 * This method is simillar to openAsWrite, but this one doesn't create headers. Just create the file with read and write mode.
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static EMDDFile openAsWriteWithoutWriteHeaders(File file, EMDDFileDescr afd) throws IOException {
		final EMDDFile sf	= new EMDDFile(file, MODE_READWRITE );
		sf.afd = afd;
		afd.length			= 0;
		sf.afh				= sf.createHeader();
		sf.afh.readHeader( sf.afd );
		sf.init();
		sf.seekFrame( 0 );
		return sf;
	}
	
	
	
	/**
	 *  Determines the type of audio file.
	 *
	 *  @param		f   the path name of the file
	 *  @return		the type code as defined in <code>SURFFileDescr</code>,
	 *				e.g. <code>TYPE_AIFF</code>. Returns <code>TYPE_UNKNOWN</code>
	 *				if the file could not be identified.
	 *
	 *  @throws IOException if the file could not be read
	 */
	public static int retrieveType( File f )
	throws IOException {
		// Creates audio file just for read
		final EMDDFile sf		= new EMDDFile( f, MODE_READONLY );
		// Gets the type of the audio file (can be UNKNOWN or WAVE)
		final int		type	= sf.retrieveType();
		sf.cleanUp(); // Closes the audio file
		return type;
	}
	
	
	/**
	 * Constructor of a audio file (SURF File).
	 * @param f The file to read
	 * @param mode Open mode either ReadWrite - 1 or ReadOnly - 0
	 * @throws IOException
	 */
	private EMDDFile( File f, int mode )
	throws IOException {
		// When creating a new audio file, it's necessary to specify the file and the mode that will be accessed.
		// The RandomAcessFile creates a random access file stream.
		raf			= new RandomAccessFile( f, mode == MODE_READWRITE ? "rw" : "r" );
		// getChannel returns the unique filechannel object associated with this file.
		// MORE INFO: https://www.tutorialspoint.com/java/io/java_io_randomaccessfile.htm
		fch			= raf.getChannel();
		// Changes the mode of the audio file
		this.mode   = mode;
	}

	/**
	 *  Returns a description of the audio file's format.
	 *  Fields which are guaranteed to be filled in, are
	 *  the type (use <code>getType</code>), <code>channels</code>,
	 *  <code>bitsPerSample</code>, <code>sampleFormat</code>,
	 *  <code>rate</code> and <code>length</code>.
	 *
	 *  @return an <code>AudioFileDescr</code> describing
	 *			this audio file.
	 *
	 *  @warning	the returned description is not immutable but
	 *				should be considered read only, do not modify it.
	 *				the fields may change dynamically if the file
	 *				is modified, e.g. the <code>length</code> field
	 *				for a writable file.
	 */
	public EMDDFileDescr getDescr() {
		// Returns all the description of the audio file
		return afd;
	}
	
	/**
	 *  Returns the file that was used to open
	 *  the audio file. Note that this simply returns
	 *	getDescr().file, so it's not a good idea to
	 *	modify this field after opening the audio file.
	 *
	 *  @return the <code>File</code> that was used in
	 *			the static constructor methods. Can be used
	 *			to query the pathname or to delete the file after
	 *			it has been closed
	 */
	public File getFile() {
		// Returns the file used to open the audio file
		return afd.file;
	}
	
	private void init() throws IOException {
		// Calculates/Changes the values of some variables.
		channels		= afd.channels;
		bytesPerFrame	= (afd.bitsPerSample >> 3) * channels; // Bytes per frame
		frameBufCapacity= Math.max( 1, 65536 / Math.max( 1, bytesPerFrame )); // Capacity of frames of the buffer
		byteBufCapacity = frameBufCapacity * bytesPerFrame; // Capacity of bytes of the buffer
		byteBuf			= ByteBuffer.allocateDirect( byteBufCapacity );
		byteBuf.order( afh.getByteOrder() );
		bh				= null;

		
		// The format (saved in the descriptor) can be integer or double
		// The bitsPerSample can be 8, 16, 24, 32 or 64
		// BufferHandler is an abstract class, so, depending on the format and bitsPerSample, points to a different class (that extends BufferHandler).
		// All this classes have readFrames and writeFrames methods, implemented in different ways. Each subclass has its own method implementation.
		switch( afd.sampleFormat ) {
		case EMDDFileDescr.FORMAT_INT:
			switch( afd.bitsPerSample ) {
			case 8:			// 8 bit int
				if( afh.isUnsignedPCM() ) {
					bh  = new UByteBufferHandler();
				} else {
					bh  = new ByteBufferHandler();
				}
				break;
			case 16:		// 16 bit int
				bh  = new ShortBufferHandler();
				break;
			case 24:		// 24 bit int
				if( afh.getByteOrder() == ByteOrder.BIG_ENDIAN ) {
					bh  = new ThreeByteBufferHandler();
				} else {
					bh  = new ThreeLittleByteBufferHandler();
				}
				break;
			case 32:		// 32 bit int
				bh  = new IntBufferHandler();
				break;
			}
			break;
		case EMDDFileDescr.FORMAT_FLOAT:
			switch( afd.bitsPerSample ) {
			case 32:		// 32 bit float
				bh  = new FloatBufferHandler();
				break;
			case 64:		// 64 bit float
				bh  = new DoubleBufferHandler();
				break;
			}
		}
		
		// If none of the previous cases 
		if( bh == null) throw new IOException( getResourceString( "errAudioFileEncoding" ));
	}

	
	
	/**
	 * This method allows to create the header, according to the type of the audio file.
	 * In this case, just the WAVE is supported.
	 * @return
	 * @throws IOException
	 */
	private AudioFileHeader createHeader() throws IOException {
		switch( afd.getType() ) {
		// If the type of the audio file is WAVE, it creates a WAVE header.
		// If the ttpe of the audio file is WAVE64 (represented by constant 5), it creates a WAVE64 header.
		case EMDDFileDescr.TYPE_WAVE:
			return new WAVEHeader();
		case EMDDFileDescr.TYPE_WAVE64:
            return new Wave64Header();
		case EMDDFileDescr.TYPE_RAW:
			return new RawHeader();
		default: 
			// If it's not a WAVE file or a WAVE64 file, one exception occurs
			// Audio file type not supported
			throw new IOException( getResourceString( "errAudioFileType" ));
		}
	}

	/**	
	 *	Reads file header in order to determine file type<br>
	 *  The information about the header can be found at:<br>
	 *  http://www.topherlee.com/software/pcm-tut-wavformat.html<br>
	 *  http://soundfile.sapp.org/doc/WaveFormat/
	 */
	private int retrieveType() throws IOException {
		long	len		= raf.length(); // Length of the file, measured in bytes
		long	oldpos	= raf.getFilePointer(); // Gets the current position of the pointer (offset from the beginning of the file - in bytes)
		int		magic;
		int		type	= EMDDFileDescr.TYPE_UNKNOWN; // Initially, file type is unknown

		if( len < 4 ) return type; // If the length of the file is less than 4 bytes, it means that it's not a RIFF file
		// MORE INFO ABOUT HEADER: http://www.topherlee.com/software/pcm-tut-wavformat.html
		// MORE INFO ABOUT HEADER: http://soundfile.sapp.org/doc/WaveFormat/

		// Moves the file pointer to position 0, which next read or write occurs
		raf.seek( 0L );
		
		// Reads 4 bytes (signed 32-bit integer) as an int, from the file, starting at the current file pointer (in this case, the pointer is at the position 0)
		// File pointer moves 4 bytes
		// 0-3 - Marks the file as a riff file.
		magic = raf.readInt(); 
		
		switch( magic ) {

		// Verifies if it's a RIFF file
		case WAVEHeader.RIFF_MAGIC:					// -------- probably WAVE --------

			if( len < 12 ) break; // If the length of the file is less than 12 bytes, it means that it's not a WAVE file

			// Reads 4 bytes (signed 32-bit integer) as an int, from the file, starting at the current file pointer
			// 4-7 - Size of the overall file (it's not important for this, but it moves the file pointer 4 bytes)
			raf.readInt();


			// Reads 4 bytes (signed 32-bit integer) as an int, from the file, starting at the current file pointer
			// 8-11 - File type header (32-bit integer) (probably "WAVE")
			magic = raf.readInt();


			switch( magic ) {
			// Verifies if it's a WAVE file
			case WAVEHeader.WAVE_MAGIC:
				// If it's a WAVE file, saves the type in a variable
				type = EMDDFileDescr.TYPE_WAVE;
				break;
			}
			
			
			break;
		case Wave64Header.RIFF_MAGIC1a: // -------- probably Wave64 --------
						
			if( (len < 40) || (raf.readInt() != Wave64Header.RIFF_MAGIC1b) || (raf.readLong() != Wave64Header.RIFF_MAGIC2) ) break;

	            raf.readLong(); // length

	            if( (raf.readLong() == Wave64Header.WAVE_MAGIC1) && (raf.readLong() == Wave64Header.WAVE_MAGIC2) ) {
	            	
	                type = EMDDFileDescr.TYPE_WAVE64;
	                
	            }
	            break;
	            
	            
		default:
			break;
		}

		// Moves the file pointer to the last position (to the position he was before been moved to position 0)
		raf.seek( oldpos );
		
		return type; // Returns the type of the file (will be UNKNOWN or WAVE)
	}

	
	/**
	 *  Moves the file pointer to a specific
	 *  frame.
	 *
	 *  @param  frame   the sample frame which should be
	 *					the new file position. this is really
	 *					the sample index and not the physical file pointer.
	 *  @throws IOException when a seek error occurs or you try to
	 *						seek past the file's end.
	 */
	public void seekFrame( long frame ) throws IOException {
		// This formula calculates the physical pointer according to the frame given (in bytes)
		// frame * bytesPerFrames returns the total bytes
		long physical	= afh.getSampleDataOffset() + frame * bytesPerFrame;
		// Points to the physical pointer (in bytes)
		raf.seek( physical );
		// The framePosition will save the current file pointer in frames
		framePosition = frame;
	}
	
	/**
	 *	Flushes pending buffer content, and 
	 *	updates the sound file header information
	 *	(i.e. length fields). Usually you
	 *	will not have to call this method directly,
	 *	unless you pause writing for some time
	 *	and want the file information to appear
	 *	as accurate as possible.
	 */
	public void flush() throws IOException {
		updateTime	= System.currentTimeMillis() + 10000;
		afd.length	= framePosition;
		afh.updateHeader( afd );
		updateLen	= framePosition + updateStep;
		fch.force( true );
	}
	
	
	/**
	 *  Returns the current file pointer in sample frames
	 *
	 *  @return		the sample frame index which is the offset
	 *				for the next read or write operation.
	 *
	 *  @throws IOException		when the position cannot be queried
	 */
	public long getFramePosition() throws IOException {
		// Returns the current file pointer in frames
		return( framePosition );
	}

	/**
	 *	Reads sample frames from the current position
	 *
	 *  @param  data	buffer to hold the frames read from hard-disc.
	 *					the samples will be de-interleaved such that
	 *					data[0][] holds the first channel, data[1][]
	 *					holds the second channel etc.
	 *					; it is allowed to have null arrays in the data
	 *					(e.g. data[0] == null), in which case these channels
	 *					are skipped when reading
	 *  @param  offset  offset in the buffer in sample frames, such
	 *					that the first frame of the first channel will
	 *					be placed in data[0][offset] etc.
	 *  @param  length  number of continuous frames to read.
	 *
	 *  @throws IOException if a read error or end-of-file occurs.
	 */
	public void readFrames( float[][] data, int offset, int length ) throws IOException {
		// Reads the frames from the current position
		bh.readFrames( data, offset, length );
		
		// Changes the frame position with the value received
		// Increases the file pointer in frames
		framePosition += length;
	}
	
	
	

	/**
	 *	Writes sample frames to the file starting at the current position.
	 *  If you write past the previous end of the file, the <code>length</code>
	 *  field of the internal <code>SURFFileDescr</code> is updated.
	 *  Since you get a reference from <code>getDescr</code> and not
	 *  a copy, using this reference to the description will automatically
	 *  give you the correct file length.
	 *
	 *  @param  data	buffer holding the frames to write to hard-disc.
	 *					the samples must be de-interleaved such that
	 *					data[0][] holds the first channel, data[1][]
	 *					holds the second channel etc.
	 *  @param  offset  offset in the buffer in sample frames, such
	 *					that he first frame of the first channel will
	 *					be read from data[0][offset] etc.
	 *  @param  length  number of continuous frames to write.
	 *
	 *  @throws IOException if a write error occurs.
	 */
	public void writeFrames( float[][] data, int offset, int length ) throws IOException {
		// Calls the writeFrames method, that depends on the subclass that BufferHandler points to.
		bh.writeFrames( data, offset, length );
		// Increases the file pointer in frames with the number of frames that will write.
		framePosition += length;

		
		// If the file pointer in frames is higher than the number of frames in the file
		if( framePosition > afd.length ) {
			if( (framePosition > updateLen) || (System.currentTimeMillis() > updateTime) ) {
				flush();
			} else {
				afd.length = framePosition;
			}
		}
	}
	
	/**
	 *	Returns the number of frames
	 *	in the file.
	 *
	 *	@return	the number of sample frames
	 *			in the file. includes pending
	 *			buffer content
	 *
	 *	@throws	IOException	this is never thrown
	 *			but declared as of the <code>InterleavedStreamFile</code>
	 *			interface
	 */
	public long getFrameNum() throws IOException {
		// Number of frames in the file
		return afd.length;
	}

	
	/**
	 * This method changes the number of frames in the file to the frame given in the parameter.
	 */
	public void setFrameNum( long frame ) throws IOException {
		// This formula calculates the physical pointer according to the frame given (in bytes)
		// frame * bytesPerFrames returns the total bytes
		final long physical	= afh.getSampleDataOffset() + frame * bytesPerFrame;

		raf.setLength( physical ); // Changes the size of the file to the frame given
		
		// If the file pointer in frames is higher than the frame given, file pointer in frames will be the frame given.
		// Once the number of frames of the file will be changed, it can't point to a frame higher than the number of frames.
		if( framePosition > frame ) framePosition = frame;
		
		// Changes the number of frames in the file 
		afd.length	= frame;
	}

	/**
	 *	Returns the number of channels
	 *	in the file.
	 *
	 *	@return	the number of channels
	 */
	public int getChannelNum() {
		// Number of channels in the file
		return afd.channels;
	}

	/**
	 *	Truncates the file to the size represented
	 *	by the current file position. The file
	 *	must have been opened in write mode.
	 *	Truncation occurs only if frames exist
	 *	beyond the current file position, which implicates
	 *	that you have set the position using <code>seekFrame</code>
	 *	to a location before the end of the file.
	 *	The header information is immediately updated.
	 *
	 *	@throws	IOException	if truncation fails
	 */
	public void truncate() throws IOException {
		// Truncates the channel size to the channel's file position
		// position() returns the channel's file position
		// MORE INFO AT: https://docs.oracle.com/javase/7/docs/api/java/nio/channels/FileChannel.html
		fch.truncate( fch.position() );
		
		// ???? 	
		if( framePosition != afd.length ) {
			afd.length	= framePosition;
			updateTime	= System.currentTimeMillis() + 10000;
			afh.updateHeader( afd );
			updateLen	= framePosition + updateStep;
		}	
	}

	/**
	 *	Copies sample frames from a source sound file
	 *	to a target file (either another sound file
	 *	or any other class implementing the
	 *	<code>InterleavedStreamFile</code> interface).
	 *	Both files must have the same number of channels.
	 *
	 *	@param	target	to file to copy to from this audio file
	 *	@param	length	the number of frames to copy. Reading
	 *					and writing begins at the current positions
	 *					of both files.
	 *
	 *	@throws	IOException	if a read or write error occurs
	 */
	public void copyFrames( InterleavedStreamFile target, long length ) throws IOException {
		
		int chunkLength; // Length of the chunk
		
		// Size of the buffer can be 8192 or lower.
		// If the number of frames to copy is greater than 8192, just copies 8192
		// If the number of frames to copy is lower than 8192, copies all frames
		int			tempBufSize	= (int) Math.min( length, 600000 );  // 8192 = 2^13 -< 600000
		
		// This temporary buffer is used to save the frames read from one audio file, before writes in the another one.
		float[][]	tempBuf		= new float[ channels ][ tempBufSize ];
		
		// While there's frames to copy
		while( length > 0 ) {
			
			// Size of the chunk can be the number of frames to copy (only needs to copy one time)
			// Or can be the size of the temporary buffer, if tempBufSize = 8192 and needs to copy more than 8192 frames (frames to copy > temporary buffer size)
			chunkLength	= (int) Math.min( length, tempBufSize );
			
    		// Reads the frames of this audio file from 0 to chunkLength, and saves in the array/buffer tempBuf
			this.readFrames( tempBuf, 0, chunkLength );
			
			// Writes in the audio file given in the parameters (target) the frames of tempBuf (saved previously) from 0 to chunkLength
			target.writeFrames( tempBuf, 0, chunkLength );
			
			// Since chunkLength frames have already been copied, just need to remove from the total frames to copy, until it reaches 0 (no more frames to copy).
			length -= chunkLength;
			
		}
	}

	/**
	 *  Flushes and closes the file
	 *
	 *  @throws IOException if an error occurs during buffer flush
	 *						or closing the file.
	 */
	public void close() throws IOException {
		// It throws an exception. It means that, when this method is called, needs to be in a try-catch statement (or eventually throws).
		
		if( mode == MODE_READWRITE ) { // If it's a read and write audio file
			fch.force( true ); // Forces any updates to this channel's file to be written to the storage device that contains it.
			afh.updateHeader( afd ); // Updates the header before closing
		}
		raf.close(); // Closes the random access file stream.
	}

	/**
	 *  Flushes and closes the file. As opposed
	 *	to <code>close()</code>, this does not
	 *	throw any exceptions but simply ignores any errors.
	 *
	 *	@see	#close()
	 */
	public void cleanUp() {
		try {
			close(); // Closes the random access file stream.
		}
		catch( IOException e ) { /* ignored */ }
	}
	
	/**
	 *  Reads metadata and comment annotations into the audio file
	 *  description if there are any. This method sets the 
	 *  <code>KEY_METADATA</code> or the <code>KEY_COMMENT</code> 
	 *  property of the afd, if metadata or comment annotations are available
	 *
	 *	@see	#getDescr()
	 *	@see	EMDDFileDescr#KEY_COMMENT
	 *  @see	EMDDFileDescr#KEY_METADATA
	 *  @see	EMDDFileDescr#KEY_NOTES
	 *
	 *	@throws	IOException	if a read or parsing error occurs
	 */
	public void readMetadata() throws IOException {
		// Reads metadata
		afh.readMetadata();
	}

	/**
	 *  Reads markers into the audio file description
	 *  if there are any. This method sets the <code>KEY_MARKERS</code>
	 *  property of the afd, if markers are available. It sets
	 *  the <code>KEY_LOOP</code> property if a loop span is available.
	 *
	 *	@see	#getDescr()
	 *	@see	AudioFileDescr#KEY_MARKERS
	 *	@see	AudioFileDescr#KEY_LOOP
	 *
	 *	@throws	IOException	if a read or parsing error occurs
	 */
	public void readMarkers() throws IOException {
		// Reads the markers
		afh.readMarkers();
		afh.readMissingDataChunks();
	}
	
	// create a method to read all the crap from the file

	
	/**
	 * Returns a string of the file "IOUtilStrings.properties" by giving a key.
	 * Ex. key = errAudioFileEncoding, returns "Unsupported audio file encoding."
	 * @param key
	 * @return the string associated with the key given.
	 */
	protected static final String getResourceString( String key ) {
		return IOUtil.getResourceString( key );
	}
	
	
	/**
	 * BufferHandler is an abstract class with an empty constructor and 2 methods: writeFrames (to write the frames on the audio file) and readFrames (to read the frames of the audio file).<br>
	 * This class has a lot of subclasses:<br>
	 * <ul>
	 * 	<li>ByteBufferHandler - integer 8 bit</li>
	 * 	<li>UByteBufferHandler - integer 8 bit</li>
	 * 	<li>ShortBufferHandler - integer 16 bit</li>
	 * 	<li>ThreeByteBufferHandler - integer 24 bits (big endian)</li>
	 * 	<li>ThreeLittleByteBufferHandler - integer 24 bits (little endian)</li>
	 * 	<li>IntBufferHandler - integer 32 bits</li>
	 * 	<li>FloatBufferHandler - double 32 bits</i>
	 * 	<li>DoubleBufferHandler - double 64 bits</li>
	 * </ul>
	 * <br>
	 * All these subclasses should implement both methods (readFrames and writeFrames) to don't be abstract too.<br>
	 * Every subclass has his own implementation of these methods. When creating a BufferHandler object, it's necessary to point to one of the subclasses, to know which of the methods is called.
	 */
	// -------- BufferHandler Classes --------
	
	private abstract class BufferHandler {
		protected BufferHandler() { /* empty */ }
		// writeFrames and readFrames are not implemented once the subclasses will implement it.
		protected abstract void writeFrames( float[][] frames, int off, int len ) throws IOException;
		protected abstract void readFrames( float[][] frames, int off, int len ) throws IOException;
	}
	
	
	
	private class ByteBufferHandler extends BufferHandler {
		private final byte[]	arrayBuf;

		protected ByteBufferHandler() {
			arrayBuf	= new byte[ byteBuf.capacity() ];
		}

		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;

			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						arrayBuf[ i ] = (byte) (b[ j ] * 0x7F);
					}
				}
				byteBuf.clear();
				byteBuf.put( arrayBuf, 0, m );
				byteBuf.flip();
				// Writes the information in the file channel
				// This file channel has an RandomAcessFile
				fch.write( byteBuf );
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				byteBuf.rewind().limit( m );
				// Reads the information of the fileChannel and saves it in the buffer
				fch.read( byteBuf );
				byteBuf.flip();
				byteBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						b[ j ]	= (float) arrayBuf[ i ] / 0x7F;
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	// float to byte = f*0x7F+0x80 (-1 ... +1 becomes 0x01 to 0xFF)
	// which is how libsndfile behaves
	private class UByteBufferHandler extends BufferHandler {
		private final byte[]	arrayBuf;

		protected UByteBufferHandler()
		{
			arrayBuf	= new byte[ byteBuf.capacity() ];
		}

		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;

			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						arrayBuf[ i ] = (byte) (b[ j ] * 0x7F + 0x80);
					}
				}
				byteBuf.clear();
				byteBuf.put( arrayBuf, 0, m );
				byteBuf.flip();
				fch.write( byteBuf );
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				byteBuf.rewind().limit( m );
				fch.read( byteBuf );
				byteBuf.flip();
				byteBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						if( arrayBuf[ i ] < 0 ) { // hmmm, java can't handle unsigned bytes
							b[ j ]	= (float) (0x80 + arrayBuf[ i ]) / 0x7F;
						} else {
							b[ j ]	= (float) (arrayBuf[ i ] - 0x80) / 0x7F;
						}
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	private class ShortBufferHandler extends BufferHandler {
		private final ShortBuffer	viewBuf;
		private final short[]		arrayBuf;
	
		protected ShortBufferHandler() {
			byteBuf.clear();
			viewBuf		= byteBuf.asShortBuffer();
			arrayBuf	= new short[ viewBuf.capacity() ];
		}

		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;

			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ]; // Saves all the samples of the channel
					if( b == null ) continue;
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						// b[j] is the sample value 
						// b is frames[channel], so b[j] is frames[channel][sample]
						// System.out.println(b[j] + " " + length);
						arrayBuf[ i ] = (short) (b[ j ] * 0x7FFF);
					}
				}
				viewBuf.clear(); // Clears the buffer
				viewBuf.put( arrayBuf, 0, m ); // Buffer to save the array
				
				// Rewinds this buffer. The position is set to zero and the mark is discarded.
				// Invoke this method before a sequence of channel-write or get operations, assuming that the limit has already been set appropriately.
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
								
				// System.out.println(byteBuf.getInt());
				
				fch.write( byteBuf ); // Writes on the fileChannel
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
				fch.read( byteBuf );
				viewBuf.clear();
				viewBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						b[ j ]	= (float) arrayBuf[ i ] / 0x7FFF;
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	/*
	 *  24bit big endian
	 */
	private class ThreeByteBufferHandler extends BufferHandler {
		private final byte[]		arrayBuf;
		private final int			chStep = (channels - 1) * 3;
	
		protected ThreeByteBufferHandler() {
			// note : it's *not* faster to use ByteBuffer.allocate()
			// and ByteBuffer.array() than this implementation
			// (using ByteBuffer.allocateDirect() and bulk get into a separate arrayBuf)
			arrayBuf	= new byte[ byteBuf.capacity() ];
		}

		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, k, m, ch, chunkLength;
			float[]	b;

			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					for( i = ch * 3, j = offset; i < m; i += chStep, j++ ) {
						k				= (int)  (b[ j ] * 0x7FFFFF);
						arrayBuf[ i++ ] = (byte) (k >> 16);
						arrayBuf[ i++ ] = (byte) (k >> 8);
						arrayBuf[ i++ ] = (byte)  k;
					}
				}
				byteBuf.clear();
				byteBuf.put( arrayBuf, 0, m );
				byteBuf.flip();
				fch.write( byteBuf );
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				byteBuf.rewind().limit( m );
				fch.read( byteBuf );
				byteBuf.flip();
				byteBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch * 3, j = offset; i < m; i += chStep, j++ ) {
						b[ j ]	= (float) ((arrayBuf[ i++ ] << 16 ) |
										  ((arrayBuf[ i++ ] & 0xFF) << 8) |
										   (arrayBuf[ i++ ] & 0xFF)) / 0x7FFFFF;
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	/*
	 *  24bit little endian
	 */
	private class ThreeLittleByteBufferHandler extends BufferHandler {
		private final byte[]		arrayBuf;
		private final int			chStep = (channels - 1) * 3;
	
		protected ThreeLittleByteBufferHandler() {
			// note : it's *not* faster to use ByteBuffer.allocate()
			// and ByteBuffer.array() than this implementation
			// (using ByteBuffer.allocateDirect() and bulk get into a separate arrayBuf)
			arrayBuf	= new byte[ byteBuf.capacity() ];
		}
		
		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, k, m, ch, chunkLength;
			float[]	b;

			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch * 3, j = offset; i < m; i += chStep, j++ ) {
						k				= (int)  (b[ j ] * 0x7FFFFF);
						arrayBuf[ i++ ] = (byte)  k;
						arrayBuf[ i++ ] = (byte) (k >> 8);
						arrayBuf[ i++ ] = (byte) (k >> 16);
					}
				}
				byteBuf.clear();
				byteBuf.put( arrayBuf, 0, m );
				byteBuf.flip();
				fch.write( byteBuf );
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * bytesPerFrame;
				byteBuf.rewind().limit( m );
				fch.read( byteBuf );
				byteBuf.flip();
				byteBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch * 3, j = offset; i < m; i += chStep, j++ ) {
						b[ j ]	= (float) ((arrayBuf[ i++ ] & 0xFF) |
										  ((arrayBuf[ i++ ] & 0xFF) << 8) |
										   (arrayBuf[ i++ ] << 16 )) / 0x7FFFFF;
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	private class IntBufferHandler extends BufferHandler {
		private final IntBuffer		viewBuf;
		private final int[]			arrayBuf;
	
		protected IntBufferHandler() {
			byteBuf.clear();
			viewBuf		= byteBuf.asIntBuffer();
			arrayBuf	= new int[ viewBuf.capacity() ];
		}

		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;

			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						arrayBuf[ i ] = (int) (b[ j ] * 0x7FFFFFFF);
					}
				}
				viewBuf.clear();
				viewBuf.put( arrayBuf, 0, m );
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
				fch.write( byteBuf );
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
				fch.read( byteBuf );
				viewBuf.clear();
				viewBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						b[ j ]	= (float) arrayBuf[ i ] / 0x7FFFFFFF;
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	private class FloatBufferHandler extends BufferHandler {
		private final FloatBuffer	viewBuf;
		private final float[]		arrayBuf;
	
		protected FloatBufferHandler() {
			byteBuf.clear();
			viewBuf		= byteBuf.asFloatBuffer();
			arrayBuf	= new float[ viewBuf.capacity() ];
		}

		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						arrayBuf[ i ] = b[ j ];
					}
				}
				viewBuf.clear();
				viewBuf.put( arrayBuf, 0, m );
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
				fch.write( byteBuf );
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
				fch.read( byteBuf );
				viewBuf.clear();
				viewBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						b[ j ]	= arrayBuf[ i ];
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	private class DoubleBufferHandler extends BufferHandler {
		private final DoubleBuffer	viewBuf;
		private final double[]		arrayBuf;
	
		protected DoubleBufferHandler() {
			byteBuf.clear();
			viewBuf		= byteBuf.asDoubleBuffer();
			arrayBuf	= new double[ viewBuf.capacity() ];
		}

		protected void writeFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						arrayBuf[ i ] = b[ j ];
					}
				}
				viewBuf.clear();
				viewBuf.put( arrayBuf, 0, m );
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
				fch.write( byteBuf );
				length -= chunkLength;
				offset += chunkLength;
			}
		}

		protected void readFrames( float[][] frames, int offset, int length ) throws IOException {
			int		i, j, m, ch, chunkLength;
			float[]	b;
		
			while( length > 0 ) {
				chunkLength = Math.min( frameBufCapacity, length );
				m			= chunkLength * channels;
				byteBuf.rewind().limit( chunkLength * bytesPerFrame );
				fch.read( byteBuf );
				viewBuf.clear();
				viewBuf.get( arrayBuf, 0, m );
				for( ch = 0; ch < channels; ch++ ) {
					b = frames[ ch ];
					if( b == null ) continue;
					for( i = ch, j = offset; i < m; i += channels, j++ ) {
						b[ j ]	= (float) arrayBuf[ i ];
					}
				}
				length -= chunkLength;
				offset += chunkLength;
			}
		}
	}

	// -------- AudioFileHeader Classes --------

	private abstract class AudioFileHeader {
		
		//protected static final long SECONDS_FROM_1904_TO_1970 = 2021253247L;
	
		protected AudioFileHeader() { /* empty */ }
		
		protected abstract void readHeader( EMDDFileDescr descr ) throws IOException;
		protected abstract void writeHeader( EMDDFileDescr descr ) throws IOException;
		protected abstract void updateHeader( EMDDFileDescr descr ) throws IOException;
		protected abstract long getSampleDataOffset();
		protected abstract ByteOrder getByteOrder();

		// WAV might overwrite this
		protected boolean isUnsignedPCM() { return false; }

		protected void readMetadata() throws IOException { /* empty */ }
		
		// WAV and AIFF might overwrite this
		protected void readMarkers() throws IOException { /* empty */ }
		
		protected void readMissingDataChunks() throws IOException { /* empty */ }

		
		protected final int readLittleUShort() throws IOException {
			final int i = raf.readUnsignedShort();
			return( (i >> 8) | ((i & 0xFF) << 8) );
		}

		protected final int readLittleInt() throws IOException {
			final int i = raf.readInt();
			return( ((i >> 24) & 0xFF) | ((i >> 8) & 0xFF00) | ((i << 8) & 0xFF0000) | (i << 24) );
		}

		
		protected final float readLittleFloat()
        throws IOException
        {
            final int i = raf.readInt();
            return( Float.intBitsToFloat( ((i >> 24) & 0xFF) | ((i >> 8) & 0xFF00) | ((i << 8) & 0xFF0000) | (i << 24) ));
        }


		protected final long readLittleLong()
        throws IOException
        {
            final long n = raf.readLong();
            return( ((n >> 56) & 0xFFL) |
                    ((n >> 40) & 0xFF00L) |
                    ((n >> 24) & 0xFF0000L) |
                    ((n >> 8)  & 0xFF000000L) |
                    ((n << 8)  & 0xFF00000000L) |
                    ((n << 24) & 0xFF0000000000L) |
                    ((n << 40) & 0xFF000000000000L) |
                     (n << 56) );
        }

		protected final void writeLittleShort( int i ) throws IOException {
			raf.writeShort( (i >> 8) | ((i & 0xFF) << 8) );
		}

		protected final void writeLittleInt( int i ) throws IOException {
			raf.writeInt( ((i >> 24) & 0xFF) | ((i >> 8) & 0xFF00) | ((i << 8) & 0xFF0000) | (i << 24) );
		}
		
		
		protected final void writeLittleLong( long n )
        throws IOException
        {
            raf.writeLong( ((n >> 56) & 0xFFL) |
                           ((n >> 40) & 0xFF00L) |
                           ((n >> 24) & 0xFF0000L) |
                           ((n >> 8)  & 0xFF000000L) |
                           ((n << 8)  & 0xFF00000000L) |
                           ((n << 24) & 0xFF0000000000L) |
                           ((n << 40) & 0xFF000000000000L) |
                            (n << 56) );
        }
		
		
		/*
		protected final String readNullTermString() throws IOException {
			final StringBuffer	buf = new StringBuffer();
			byte				b;
			
			b	= raf.readByte();
			while( b != 0 ) {
				buf.append( (char) b );
				b	= raf.readByte();
			}
			return buf.toString();
		}
		*/
		
		// --- SURF METHODS ---
		
		// get the text size in bytes when enconding is UTF-8
		protected int getSize(String src) {
			int size = 0;
			try {
				size =  src.getBytes("UTF-8").length;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return size;
		}
		
		// get the UTF-8 byte representation of the text
		protected byte[] getBytes(String src) {
			byte[] bytes = new byte[1];		
			try {
				bytes = src.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return bytes;
		}
	
		/*
		protected abstract class DataInputReader {
			protected final DataInput din;
			
			protected DataInputReader( DataInput din ) {
				this.din = din;
			}
			
			public abstract int readInt() throws IOException;
			public abstract float readFloat() throws IOException;
		}
		
		
		protected class LittleDataInputReader extends DataInputReader {
			public LittleDataInputReader( DataInput din ) {
				super( din );
			}

			public int readInt() throws IOException { return readLittleInt(); }
			public float readFloat() throws IOException { return readLittleFloat(); }
		}

		protected class BigDataInputReader extends DataInputReader {
			public BigDataInputReader( DataInput din ) {
				super( din );
			}

			public int readInt() throws IOException { return din.readInt(); }
			public float readFloat() throws IOException { return din.readFloat(); }
		}
		*/
	}

	private abstract class AbstractRIFFHeader extends AudioFileHeader {
		protected static final int ADTL_MAGIC		= 0x6164746C;	// 'adtl'
		protected static final int LABL_MAGIC		= 0x6C61626C;	// 'labl'
		protected static final int LTXT_MAGIC		= 0x6C747874;	// 'ltxt'
		
		protected static final int NOTE_MAGIC		= 0x6E6F7465;	// 'note'

		// ltxt purpose for regions
		protected static final int RGN_MAGIC		= 0x72676E20;	// 'rgn '

		// fmt format-code
		protected static final int FORMAT_PCM		= 0x0001;
		protected static final int FORMAT_FLOAT		= 0x0003;
		protected static final int FORMAT_EXT		= 0xFFFE;
		
		// info chunk
		protected static final int INFO_MAGIC 		= 0x494E464F; 	// 'INFO'
		
		// info chunk inner chunks
		protected static final int IARL_MAGIC 	= 0x4941524C;		// 'IARL' archival location
		protected static final int IART_MAGIC	= 0x49415254;		// 'IART' artist -> dataset creator
		protected static final int ICMS_MAGIC 	= 0x49434D53;		// 'ICMS' commissioner
		protected static final int ICMT_MAGIC 	= 0x49434D54;		// 'ICMT' comments
		protected static final int ICOP_MAGIC	= 0x49434F50;		// 'ICOP' copyright
		protected static final int ICRD_MAGIC	= 0x49435244;		// 'ICRD' creation date
		protected static final int IKEY_MAGIC 	= 0x494B4559;		// 'IKEY' keywords
		protected static final int INAM_MAGIC 	= 0x494E414D;		// 'INAM' subject
		protected static final int IPRD_MAGIC 	= 0x49505244;		// 'IPRD' product -> original propose of the file
		protected static final int ISBJ_MAGIC 	= 0x4953424A;		// 'ISBJ' subject -> contents of the file
		protected static final int ISFT_MAGIC 	= 0x49534654;		// 'ISFT' software -> software package used to create the file
		protected static final int ISRC_MAGIC 	= 0x49535243;		// 'ISRC' source -> original (person / organization) source of the file
		protected static final int ISRF_MAGIC 	= 0x49535246;		// 'ISRF' source form -> original form of material (.ZIP / .TXT / etc)
		
		protected long 		sampleDataOffset;
		protected long		dataLengthOffset;
        protected long		factSmpNumOffset;
		protected long		lastUpdateLength	= 0L;
		protected boolean	isFloat				= false;
		protected boolean	unsignedPCM;
				
		protected AbstractRIFFHeader() { /* empty */ }
	}
	
	private class WAVEHeader extends AbstractRIFFHeader {
		private static final int RIFF_MAGIC		= 0x52494646;		// 'RIFF'
		private static final int WAVE_MAGIC		= 0x57415645;		// 'WAVE' (offset 8)


		private static final int MDL_MAGIC		= 0x4d444c20;		// 'MDL '
		private static final int MD_MAGIC		= 0x4d444154;		// 'MDAT'
		
		// chunk identifiers
		private static final int FMT_MAGIC		= 0x666D7420;		// 'fmt '
		private static final int DATA_MAGIC		= 0x64617461;		// 'data'
		private static final int CUE_MAGIC		= 0x63756520;		// 'cue '

		// embedded LIST (peak speak) / list (rest of the universe speak) format
		private static final int LIST_MAGIC		= 0x6C697374;		// 'list'
		private static final int LIST_MAGIC2	= 0x4C495354;		// 'LIST'
		
		// inherited from the AIFF chunks (used to add comment chunks)
		private static final int COMT_MAGIC		= 0x434F4D54;		// 'COMT' comment
		
		// inherited from the AIFF chunks (used to define the annotations chunks)
		private static final int ANNO_MAGIC		= 0x414E4E4F;		// 'ANNO' annotations
		
		// some of our custom FourCC codes
		private static final int META_MAGIC		= 0x4D455441;		// 'META' metadata
		private static final int CNFG_MAGIC		= 0x434E4647;		// 'CNFG' mandatory configuration chunk
		private static final int TMST_MAGIC		= 0x544D5354;		// 'TMSP' dataset initial timestamp
		private static final int TMZN_MAGIC		= 0x544D5A4E;		// 'TMZN' dataset timezone
		private static final int SPRT_MAGIC		= 0x53505254;		// 'SPRT' sampling rate
		private static final int CHCC_MAGIC		= 0x43484343;		// 'CHCC' channel calibration constant
		
		private static final long riffLengthOffset = 4L;
		
		private long		listMagicOff		= 0L;
		private long		listMagicLen		= 0L;
		private long		mdlMagicOff		= 0L;
		private long		mdlMagicLen		= 0L;
		private long		cueMagicOff			= 0L;
		
		
		private long		annoMagicOff		= 0L;
		private long		annoMagicLen		= 0L;
		private long 		cnfgMagicOff		= 0L;
		private long		cnfgMagicLen		= 0L;
		private long 		infoMagicOff		= 0L;
		private long		infoMagicLen		= 0L;
		
		protected WAVEHeader() { /* empty */ }
		
		
		
		/**
		 * This method reads all the information on the header.
		 */
		protected void readHeader( EMDDFileDescr descr ) throws IOException {
			int		i, i1, i2, i3, chunkLen, essentials, magic, dataLen = 0, bpf = 0;
			long	len;
			
			
			
			/* WAV FILE HEADER:
			1 - 4	"RIFF"	Marks the file as a riff file. Characters are each 1 byte long.
			5 - 8	File size (integer)	Size of the overall file - 8 bytes, in bytes (32-bit integer). Typically, you'd fill this in after creation.
			9 -12	"WAVE"	File Type Header. For our purposes, it always equals "WAVE".
			13-16	"fmt "	Format chunk marker. Includes trailing null
			17-20	16	Length of format data as listed above
			21-22	1	Type of format (1 is PCM) - 2 byte integer
			23-24	2	Number of Channels - 2 byte integer
			25-28	44100	Sample Rate - 32 byte integer. Common values are 44100 (CD), 48000 (DAT). Sample Rate = Number of Samples per second, or Hertz.
			29-32	176400	(Sample Rate * BitsPerSample * Channels) / 8.
			33-34	4	(BitsPerSample * Channels) / 8.1 - 8 bit mono2 - 8 bit stereo/16 bit mono4 - 16 bit stereo
			35-36	16	Bits per sample
			37-40	"data"	"data" chunk header. Marks the beginning of the data section.
			41-44	File size (data)	Size of the data section.
			*/
			
			
			/*
			 * There's 3 subchunks:
			 * - "RIFF" chunk descriptor
			 * - "fmt" sub-chunk
			 * - "data" sub-chunk
			 * 
			 * The "fmt " subchunk describes the sound data's format.
			 * The "data" subchunk contains the size of the data and the actual sound.
			 */
			

			// Reads the first 4 bytes (Marks the file as a RIFF file)
			// The goal is to verify if it is RIFF
			raf.readInt();		// RIFF
			
			
			// Reads the next 4 bytes (Size of the overall file)
			raf.readInt();
			len	= raf.length() - 8;
			
			// Reads the next 4 bytes (File Type Header)
			// The goal is to verify if is a WAVE file
			raf.readInt();		// WAVE
			
			
			len	   -= 4;
			chunkLen = 0; // Initially, the length of the chunk is zero
			
			
			// For loop to read 2 subchunks on the header
			// Finishes when both of subchunks read and all the information of the subchunks read
			for( essentials = 2; (len > 0) && (essentials > 0); ) {
				
				// Goes to the next chunk
				// Basically it gets the length of the chunk (from file header) and increases that value on the file pointer
				if( chunkLen != 0 ) raf.seek( raf.getFilePointer() + chunkLen );	// skip to next chunk
			
				
				// (First iteration) essentials = 2: Reads the next 4 bytes (Format chunk marker - fmt)
				// (Second iteration) essentials = 1: Reads the next 4 bytes ("data" chunk header)
				magic		= raf.readInt();
				
				// (First iteration) essentials = 2: Reads the next 4 bytes (Length of chunk), once readLittleInt calls raf.readInt method.
				// (Second iteration) essentials = 1: Reads the next 4 bytes (Size of the data section).
				chunkLen	= (readLittleInt() + 1) & 0xFFFFFFFE;
				len		   -= chunkLen + 8;

				switch( magic ) {
				case FMT_MAGIC: // Verifies if it is "fmt" (fmt subchunk)
					essentials--; // Means next chunk
					// Reads the next 2 bytes (Type of format)
					i					= readLittleUShort();		// format
					// Reads the next 2 bytes (Number of channels)
					descr.channels		= readLittleUShort();		// # of channels
					// Reads the next 4 bytes (Sample rate - 32 byte integer)
					i1					= readLittleInt();			// sample rate (integer)
					// Reads the next 4 bytes (ByteRate - (Sample Rate * BitsPerSample * Channels) / 8)
					descr.rate			= i1;
					i2					= readLittleInt();			// bytes per frame and second (=#chan * #bits/8 * rate)
					// Reads the next 2 bytes (BitsPerSample * Channels) / 8
					bpf		= readLittleUShort();		// bytes per frame (=#chan * #bits/8)
					// Reads the next 2 bytes (Bits per sample)
					descr.bitsPerSample	= readLittleUShort();		// # of bits per sample
					
					// Finishes "fmt" sub-chunk
						
					if( ((descr.bitsPerSample & 0x07) != 0) ||
						((descr.bitsPerSample >> 3) * descr.channels != bpf) ||
						((descr.bitsPerSample >> 3) * descr.channels * i1 != i2) ) {
											
						throw new IOException( getResourceString( "errAudioFileEncoding" ));
					}
					
					
					// If bytes per frame = 1 -> unsignedPCM = true
					// If bytes per frame != 1 -> unsignedPCM = false
					unsignedPCM			= bpf == 1;

					chunkLen -= 16; // Finishes the information about the sub-chunk (2 + 2 + 4 + 4 + 2 + 2)

					
					// Verifies the type of format
					// There's different types of extension because Wave format is a container for many different kinds of sample formats.
					// Extra fields that are needed for one sample might not be needed for another sample format.
					// The format code indicates how the sample data for the wave file is stored.
					// The most common is PCM (which has a code of 1).
					
					/* FORMATS:
					1 - PCM
					3 - IEEE floating point
					2 - ADPCM
					7 - u-law
					65534 - WaveFormatExtensible 
					*/
					
					switch( i ) {
					case FORMAT_PCM:
						descr.sampleFormat = EMDDFileDescr.FORMAT_INT;
						break;
					case FORMAT_FLOAT:
						descr.sampleFormat = EMDDFileDescr.FORMAT_FLOAT;
						break;
					case FORMAT_EXT:
						// MORE INFO: http://wavefilegem.com/how_wave_files_work.html
						
						/* When the format is WAVE_FORMAT_EXTENSIBLE, the extension size in the format chunk should be 22, and the following fields should be included:
						Extension Size - 2 bytes - 16-bit unsigned integer (value 22)
						Valid Bits Per Sample - 2 bytes - 16-bit unsigned integer
						Channel Mask - 4 bytes - 32-bit unsigned integer
						Sub Format - 16 bytes - 16-byte GUID
						*/
						
						// The sum of 2 + 2 + 4 + 16 = 24, so, if the length of the chunk is less than 24, it means that, there's information missing.
						if( chunkLen < 24 ) throw new IOException( getResourceString( "errAudioFileIncomplete" ));
						
						// Reads the next 2 bytes (Extension size)
						i1 = readLittleUShort();	// extension size
						
						// As written before, when the format is WAVE_FORMAT_EXTENSIBLE, the extension size should be 22.
						// If it's lower, it means that something is missing
						if( i1 < 22 ) throw new IOException( getResourceString( "errAudioFileIncomplete" ));
						
						// Reads the next 2 bytes (Valid Bits Per Sample)
						i2 = readLittleUShort();	// # valid bits per sample
						
						// Reads the next 4 bytes (Channel Mask)
						raf.readInt();				// channel mask, ignore
						
						// Reads the next 2 bytes (Sub Format - GUID)
						i3 = readLittleUShort();	// GUID first two bytes
						
						/*
						The sub format identifies the format of the sample data in the data chunk.
						PCM Integer: 0x0100000000001000800000aa00389b71 (first 2 bytes is 1)
						PCM Float: 0x0300000000001000800000aa00389b71 (first 2 bytes is 3)
						A-law: 0x0600000000001000800000aa00389b71
						u-law: 0x0700000000001000800000aa00389b71
						ADPCM: 0x0200000000001000800000aa00389b71
						MPEG: 0x5000000000001000800000aa00389b71	
						*/
						
						// Gives an error if bitsPerSample are not the same as the descriptor
						// Or if the sub format is not PCM INTEGER or PMC Float.
						if( (i2 != descr.bitsPerSample) ||
							((i3 != FORMAT_PCM) &&
							(i3 != FORMAT_FLOAT)) ) throw new IOException( getResourceString( "errAudioFileEncoding" ));
						
						// Changes the sampleFormat according to the sub format (FORMAT_INT or FORMAT_FLOAT).
						// If the sub format (i3) is 1, then it's PCM INTEGER, else it's PCM Float.
						// INT or FLOAT, since now exceptions happened.
						descr.sampleFormat = i3 == FORMAT_PCM ? EMDDFileDescr.FORMAT_INT : EMDDFileDescr.FORMAT_FLOAT;
						
						chunkLen -= 10; // Finishes the information about the sub-chunk (2 + 2 + 4 + 2)
						
						break;
					default:
						// If another format that is not INT, FLOAT or EXT.
						throw new IOException( getResourceString( "errAudioFileEncoding" ));
					}
					break;

				case DATA_MAGIC: // Verifies if it is "data" (data subchunk).
					essentials--;
					// Gets the current position of the file pointer
					sampleDataOffset	= raf.getFilePointer();
					dataLen				= chunkLen; // Size of the subchunk data
					break;
				
				case CUE_MAGIC:
					// Gets the current position of the file pointer
					cueMagicOff			= raf.getFilePointer();
					break;
					
					
					
				// Missing data list
				/* case MDL_MAGIC:
					mdlMagicOff = raf.getFilePointer();
					mdlMagicLen = chunkLen;
					i	= raf.readInt();
					chunkLen -= 4;
					System.out.println("ENCONTREI");
					// System.out.println(raf.readInt());

					System.out.println("aa" + mdlMagicLen);
					
					
					break; */

					
					
				// ????
				case LIST_MAGIC:
				case LIST_MAGIC2:
					i	= raf.readInt();
					chunkLen -= 4;
					if( i == MDL_MAGIC ) {
						mdlMagicOff = raf.getFilePointer();
						mdlMagicLen = chunkLen;
					} // if( i == ADTL_MAGIC )
					if( i == ADTL_MAGIC ) {
						listMagicOff = raf.getFilePointer();
						listMagicLen = chunkLen;
					} // if( i == ADTL_MAGIC )
					if(i == ANNO_MAGIC) {
						annoMagicOff = raf.getFilePointer();
						annoMagicLen = chunkLen;
						// System.out.println("anno: " + annoMagicLen);
					} // if ( i == ANNO_MAGIC )
					if(i == CNFG_MAGIC) {
						cnfgMagicOff = raf.getFilePointer();
						cnfgMagicLen = chunkLen;
						// System.out.println("cnfg: " + cnfgMagicLen);
						// make this change the essentials to account for the config chunk
					}
					if( i == INFO_MAGIC ) {
						infoMagicOff = raf.getFilePointer();
						infoMagicLen = chunkLen;
					}
					break;
		
				default:
					break;
				} // switch( magic )
			} // for( essentials = 2; (len > 0) && (essentials > 0); )
			if( essentials > 0 ) throw new IOException( getResourceString( "errAudioFileIncomplete" ));
			
			
			// The length of the file (number of samples) is calculated using information about the header.
			// dataLen is the size of the subchunk data
			// bpf is the number of bytes per frame from the subchunk format (fmt)
			descr.length	= dataLen / bpf;
		
		}
		
		
		/**
		 * This method writes the header in the audio file.
		 */
		@SuppressWarnings("unchecked")
		protected void writeHeader( EMDDFileDescr descr ) throws IOException {
			int					i, i1, i2, i3;
			Region				region;
			Marker				marker;
			Annotation			annotation;
			Info		SURF_info;
			Config config;
			List<Marker>		labels, notes;
			List<Region>		regions;
			List<Annotation>	comments, metadata;
			
			
			List<MissingData> mdlist;
			MissingData md;
			
			long			pos, pos2;
			//Object			o;
			
			// check rate because original wave only supports integer greater or equal to 1
			descr.SURF_sample_rate = (float) descr.rate;
			if(descr.SURF_sample_rate >= 1)
				descr.rate = descr.SURF_sample_rate;
			else
				descr.rate = 1;
			
			
			
			// writeInt is the opposite as readInt. Writes an int (4 bytes).

			isFloat = descr.sampleFormat == EMDDFileDescr.FORMAT_FLOAT;	// floating point requires FACT extension
			
			// RIFF CHUNK
			
			// RIFF (4 bytes)
			raf.writeInt( RIFF_MAGIC );
			// File size (4 bytes)
			raf.writeInt( 0 );				// Laenge ohne RIFF-Header (Dateilaenge minus 8); unknown now
			// File type header - WAVE (4 bytes)
			raf.writeInt( WAVE_MAGIC );

			// FMT subchunk
			
			// Format chunk marker - "fmt " (4 bytes) - there's a space after 't' 
			raf.writeInt( FMT_MAGIC );
			
			// Length of format data
			// 18 if it's float, 16 if it's not float
			writeLittleInt( isFloat ? 18 : 16 );	// FORMAT_FLOAT has extension of size 0
			
			// Type of format (if it's float - 0, else 1 that means PCM)
			writeLittleShort( isFloat ? FORMAT_FLOAT : FORMAT_PCM );
			
			// Number of channels (2 bytes)
			writeLittleShort( descr.channels );
			
			// Sample rate - 4 bytes
			i1 = (int) (descr.rate + 0.5);
			writeLittleInt( i1 );
			
			
			// Signed right shift (>> 3) it's the same as division by 8
			i2 = (descr.bitsPerSample >> 3) * descr.channels;
			// ( sample rate (i1) * bitsPerSample * channels (i2) ) / 8
			writeLittleInt( i1 * i2 );
			
			// ( bitsPerSample * channels ) / 8
			writeLittleShort( i2 );
			
			// Bits per sample
			writeLittleShort( descr.bitsPerSample );
			
			if( isFloat ) raf.writeShort( 0 );
			
			
			
			
			config = (Config) descr.getProperty(EMDDFileDescr.KEY_CONFIG);

			if (config == null) {
				config = new Config();
			}

		
			// mandatory config chunk
			raf.writeInt(LIST_MAGIC);
			pos	= raf.getFilePointer();
			raf.writeInt(0);
			raf.writeInt(CNFG_MAGIC);
				
			// write initial timestamp
			// if(afd.SURF_initial_timestamp == "") {
			//if(config.surf_initial_timestamp == null) {
			//	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				// afd.SURF_initial_timestamp = df.format(Calendar.getInstance().getTime());
			//	config.surf_initial_timestamp = df.format(Calendar.getInstance().getTime());
			//}
			// i3 = getSize( afd.SURF_initial_timestamp) + 1;
			i3 = getSize( config.surf_initial_timestamp) + 1;
			raf.writeInt( TMST_MAGIC );
			writeLittleInt( i3 );
			// raf.writeBytes( afd.SURF_initial_timestamp );
			raf.writeBytes( config.surf_initial_timestamp );
			if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
			
			// write timezone
			// i3 = getSize( afd.SURF_timezone ) + 1;
			// if(afd.SURF_timezone == "") {
			//if(config.surf_timezone == null) {
				// afd.SURF_timezone = Calendar.getInstance().getTimeZone().getID();
			//	config.surf_timezone = Calendar.getInstance().getTimeZone().getID();
			//}
			i3 = getSize( config.surf_timezone ) + 1;
			raf.writeInt( TMZN_MAGIC );
			writeLittleInt( i3 );
			// raf.write( getBytes( afd.SURF_timezone ) );
			//raf.write( getBytes( config.surf_timezone ) );
			raf.writeBytes( config.surf_timezone );
			if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
			
			// write sampling rate
			i3 = Float.SIZE / 8; 
			raf.writeInt( SPRT_MAGIC );
			writeLittleInt( i3 );
			// raf.writeFloat(afd.SURF_sample_rate);
			raf.writeFloat(config.surf_sampling_rate);
			// System.out.println("i3 sampling rate: " + i3);
			// System.out.println(i3 & 1);
				
			// write channel calibration -> CHECK IF LENGTH = NUM CHANNELS!!!!
			i3 = Float.SIZE / 8;
			// if (afd.SURF_channel_calibration == null) {
			if (config.surf_channel_callibration == null) {
				// afd.SURF_channel_calibration = new float[]{1,1};
				config.surf_channel_callibration = new float[]{1,1};
			}
			for(int c = 0; c < afd.channels; c++) {
				raf.writeInt( CHCC_MAGIC );
				writeLittleInt( i3 );
				// raf.writeFloat(afd.SURF_channel_calibration[c]);
				raf.writeFloat(config.surf_channel_callibration[c]);
			}
			
			// update this list size
			pos2 = raf.getFilePointer();
			i	 = (int) (pos2 - pos - 4);
			// System.out.println("i config: " + i);
			if( (i & 1) == 1 ) {
				raf.write( 0 );	// padding byte
				pos2++;
			}
			raf.seek( pos );
			writeLittleInt( i );
			raf.seek( pos2 );
			
			
			
			
			// MISSING DATA CHUNK
			
			raf.writeInt( LIST_MAGIC );
			pos	= raf.getFilePointer();
			raf.writeInt( 0 );
			
			raf.writeInt(MDL_MAGIC);
			// pos	= raf.getFilePointer();
			// long missingDataListSize = raf.getFilePointer();
			// writeLittleInt(0);
			
			mdlist = (List<MissingData>) descr.getProperty( EMDDFileDescr.KEY_MISSINGDATA );
			int mdSize = 0;
			
			// For each missing data
			if (mdlist != null) {
				for (i = 0; i < mdlist.size(); i++) {
					md = (MissingData) mdlist.get(i);
					mdSize = getSize( md.toJSON() ) + 1;
					raf.writeInt(MD_MAGIC);
					writeLittleInt( mdSize );
					raf.write( getBytes( md.toJSON() ) );
					if( (mdSize & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
			}
			
			
			// update 'mdl' chunk size
			pos2 = raf.getFilePointer();
			i	 = (int) (pos2 - pos - 4);			
			if( (i & 1) == 1 ) {
				raf.write( 0 );	// padding byte
				pos2++;
			}
			raf.seek( pos );
			writeLittleInt( i );
			raf.seek( pos2 );
			
			
			
			
			// cue Chunk
			labels  	= (List<Marker>) descr.getProperty( EMDDFileDescr.KEY_LABELS ); 	// appliance activity  -> LABEL chunks
			regions  	= (List<Region>) descr.getProperty( EMDDFileDescr.KEY_REGIONS ); 	// user activities 	 -> LABELED TEXT chunks
			notes  		= (List<Marker>) descr.getProperty( EMDDFileDescr.KEY_NOTES ); 		// localized metadata 	 -> NOTE chunks
			comments 	= (List<Annotation>) descr.getProperty(EMDDFileDescr.KEY_COMMENTS);	// comments in the annotations chunk
			metadata 	= (List<Annotation>) descr.getProperty(EMDDFileDescr.KEY_METADATA);	// metadata in the annotations chunks
			SURF_info 	= (Info) descr.getProperty(EMDDFileDescr.KEY_INFO);

			
			if( ((labels != null) && !labels.isEmpty()) 
					|| ((regions != null) && !regions.isEmpty()) 
					|| ((notes != null) && !notes.isEmpty())) {
				if( labels == null ) labels 	= new ArrayList<Marker>();
				if( regions == null ) regions 	= new ArrayList<Region>();
				if( notes == null ) notes 		= new ArrayList<Marker>();
				
				raf.writeInt( CUE_MAGIC );
				i2	= labels.size() + regions.size() + notes.size();
				writeLittleInt( 24 * i2 + 4 );
				writeLittleInt( i2 );
				
				for( i = 0, i1 = 1; i < labels.size(); i++, i1++ ) {
					marker = (Marker) labels.get( i );
					writeLittleInt( i1 );
					writeLittleInt( i1 );
					raf.writeInt( DATA_MAGIC );
					raf.writeLong( 0 );	// ignore dwChunkStart, dwBlockStart
					writeLittleInt( (int) marker.pos );
				}
				
				for( i = 0; i < regions.size(); i++, i1++ ) {
					region = (Region) regions.get( i );
					writeLittleInt( i1 );
					writeLittleInt( i1 );
					raf.writeInt( DATA_MAGIC );
					raf.writeLong( 0 );	// ignore dwChunkStart, dwBlockStart
					writeLittleInt( (int) region.span.getStart() ); // WRITES ACCORDING TO THE POSITION IN THE SPAN
				}
				
				for( i = 0; i < notes.size(); i++, i1++ ) {
					marker = (Marker) notes.get( i );
					writeLittleInt( i1 );
					writeLittleInt( i1 );
					raf.writeInt( DATA_MAGIC );
					raf.writeLong( 0 );	// ignore dwChunkStart, dwBlockStart
					writeLittleInt( (int) marker.pos );
				}
				
				raf.writeInt( LIST_MAGIC );
				pos	= raf.getFilePointer();
				raf.writeInt( 0 );
				raf.writeInt( ADTL_MAGIC );
				
				for( i = 0, i1 = 1; i < labels.size(); i++, i1++ ) {
					marker	= (Marker) labels.get( i );
					i3 			= getSize( marker.name ) + 5;
					raf.writeInt( LABL_MAGIC );
					writeLittleInt( i3 );
					writeLittleInt( i1 );
					raf.write( getBytes( marker.name ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				for( i = 0; i < notes.size(); i++, i1++ ) {
					marker	= (Marker) notes.get( i );
					i3			= getSize( marker.name ) + 5;
					raf.writeInt( NOTE_MAGIC );
					writeLittleInt( i3 );
					writeLittleInt( i1 );
					raf.write( getBytes( marker.name) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				for( i = 0; i < regions.size(); i++, i1++ ) {
					region	= (Region) regions.get( i );
					raf.writeInt( LTXT_MAGIC );
					i3		= getSize( region.name ) + 21;
					writeLittleInt( i3 );
					writeLittleInt( i1 );
					writeLittleInt( (int) region.span.stop );
					raf.writeInt( RGN_MAGIC );
					raf.writeLong( 0 );		// wCountry, wLanguage, wDialect, wCodePage
					raf.write( getBytes( region.name ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// update 'list' chunk size
				pos2 = raf.getFilePointer();
				i	 = (int) (pos2 - pos - 4);			
				if( (i & 1) == 1 ) {
					raf.write( 0 );	// padding byte
					pos2++;
				}
				raf.seek( pos );
				writeLittleInt( i );
				raf.seek( pos2 );
				
			} // if marker or region list not empty
			
			if( ((metadata != null) && !metadata.isEmpty()) 
					|| ((comments != null) && !comments.isEmpty()) ) {
				if( metadata == null ) metadata = new ArrayList<Annotation>();
				if( comments == null ) comments = new ArrayList<Annotation>();
				
				raf.writeInt(LIST_MAGIC);
				pos	= raf.getFilePointer();
				raf.writeInt(0);
				raf.writeInt(ANNO_MAGIC);
				
				// add the metadata chunks				
				for( i = 0, i1 = 1; i < metadata.size(); i++, i1++ ) {
					annotation	= (Annotation) metadata.get( i );
					i3 = getSize( annotation.content ) + 1;
					raf.writeInt(META_MAGIC);
					writeLittleInt( i3 );
					// System.out.println("metadata: " + i3);
					// System.out.println(i3 & 1);
					raf.write( getBytes( annotation.content ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				for( i = 0, i1 = 1; i < comments.size(); i++, i1++ ) {
					annotation	= (Annotation) comments.get( i );
					// i3		= annotation.content.length() + 1;
					i3 = getSize( annotation.content ) + 1;
					raf.writeInt(COMT_MAGIC);
					writeLittleInt( i3 );
					// System.out.println("comment: " + i3);
					// System.out.println(i3 & 1);
					raf.write( getBytes( annotation.content ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// update this list size
				pos2 = raf.getFilePointer();
				i	 = (int) (pos2 - pos - 4);
				if( (i & 1) == 1 ) {
					raf.write( 0 );	// padding byte
					pos2++;
				}
				raf.seek( pos );
				writeLittleInt( i );
				raf.seek( pos2 );				
				
			} // if metadata or comment list not empty
			
			
			// INFO CHUNK - not mandatory
			if( SURF_info != null) {
				raf.writeInt(LIST_MAGIC);
				pos	= raf.getFilePointer();
				raf.writeInt(0);
				raf.writeInt(INFO_MAGIC);
				
				// write archival location
				if(SURF_info.archival_location != "") {
					i3 = getSize(SURF_info.archival_location) + 1;
					raf.writeInt(IARL_MAGIC);
					writeLittleInt(i3);
					raf.write(getBytes(SURF_info.archival_location));
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write dataset creator
				if(SURF_info.file_creator != "") {
					i3 = getSize(SURF_info.file_creator) + 1;
					raf.writeInt(IART_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.file_creator ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write commissioner
				if(SURF_info.commissioner != "") {
					i3 = getSize(SURF_info.commissioner) + 1;
					raf.writeInt(ICMS_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.commissioner ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write comments
				if(SURF_info.comments != "") {
					i3 = getSize(SURF_info.comments) + 1;
					raf.writeInt(ICMT_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.comments ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write copyright
				if(SURF_info.copyright != "") {
					i3 = getSize(SURF_info.copyright) + 1;
					raf.writeInt(ICOP_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.copyright ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write creation date
				if(SURF_info.creation_date != "") {
					i3 = getSize(SURF_info.creation_date) + 1;
					raf.writeInt(ICRD_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.creation_date ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write keywords
				if(SURF_info.keywords != "") {
					i3 = getSize(SURF_info.keywords) + 1;
					raf.writeInt(IKEY_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.keywords ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write subject / name
				if(SURF_info.name != "") {
					i3 = getSize(SURF_info.name) + 1;
					raf.writeInt(INAM_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.name ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write product
				if(SURF_info.product != "") {
					i3 = getSize(SURF_info.product) + 1;
					raf.writeInt(IPRD_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.product ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write subject
				if(SURF_info.subject != "") {
					i3 = getSize(SURF_info.subject) + 1;
					raf.writeInt(ISBJ_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.subject ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write software
				if(SURF_info.software != "") {
					i3 = getSize(SURF_info.software) + 1;
					raf.writeInt(ISFT_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.software ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write source
				if(SURF_info.source != "") {
					i3 = getSize(SURF_info.source) + 1;
					raf.writeInt(ISRC_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.source ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// write source form
				if(SURF_info.source_form != "") {
					i3 = getSize(SURF_info.source_form) + 1;
					raf.writeInt(ISRF_MAGIC);
					writeLittleInt(i3);
					raf.write( getBytes( SURF_info.source_form ) );
					if( (i3 & 1) == 0 ) raf.writeByte( 0 ); else raf.writeShort( 0 );
				}
				
				// update this list size
				pos2 = raf.getFilePointer();
				i	 = (int) (pos2 - pos - 4);
				if( (i & 1) == 1 ) {
					raf.write( 0 );	// padding byte
					pos2++;
				}
				raf.seek( pos );
				writeLittleInt( i );
				raf.seek( pos2 );	
			} // if info chunk not null
			
			
			// data Chunk (Header)
			raf.writeInt( DATA_MAGIC );
			dataLengthOffset = raf.getFilePointer();
			raf.writeInt( 0 );
			sampleDataOffset = raf.getFilePointer();
			
			updateHeader( descr );
		}
		
		protected void updateHeader( EMDDFileDescr descr ) throws IOException {
			long oldPos	= raf.getFilePointer();
			long len	= raf.length();
			if( len == lastUpdateLength ) return;
			
			if( len >= riffLengthOffset + 4 ) {
				raf.seek( riffLengthOffset );
				writeLittleInt( (int) (len - 8) );								// RIFF Chunk len
			}
			if( len >= dataLengthOffset + 4 ) {
				raf.seek( dataLengthOffset );
				writeLittleInt( (int) (len - (dataLengthOffset + 4)) );			// data Chunk len
			}
			raf.seek( oldPos );
			lastUpdateLength = len;
		}
		
		protected long getSampleDataOffset() {
			return sampleDataOffset;
		}
		
		protected ByteOrder getByteOrder() {
			return ByteOrder.LITTLE_ENDIAN;
		}

		protected boolean isUnsignedPCM() {
			return unsignedPCM;
		}
		
		
		
		protected void readMissingDataChunks() throws IOException {
					
			final Map<Integer, Integer>	mapCues			= new HashMap<Integer, Integer>();
			final Map<Integer, Integer>	mapCueLengths	= new HashMap<Integer, Integer>();
			final Map<Integer, String>	mapCueNames		= new HashMap<Integer, String>();
			final Map<Integer, Integer>	mapCueTypes		= new HashMap<Integer, Integer>();
			final long	oldPos			= raf.getFilePointer();
			final List<MissingData>	missingdata;
			
			int			i, i1, i2, i3, i4, i5;
			Object		o;
			String		str;
			int			type;
			byte[]		strBuf			= null;
			
			
			/* if( mdlMagicOff > 0L ) {
				raf.seek( mdlMagicOff );
				for( long chunkLen = mdlMagicLen; chunkLen >= 8; ) {
					i	= raf.readInt();				// sub chunk ID
					i1	= readLittleInt();
					i2	= (i1 + 1) & 0xFFFFFFFE;		// sub chunk length
					chunkLen -= 8;
					switch( i ) {
					case MD_MAGIC:
						i3		  = readLittleInt();	// dwIdentifier
						i1		 -= 4;
						i2	     -= 4;
						chunkLen -= 4;
						if( strBuf == null || strBuf.length < i1 ) {
							strBuf  = new byte[ Math.max( 64, i1 )];
						}
						raf.readFully( strBuf, 0, i1 );	// null-terminated
						mapCueNames.put( new Integer( i3 ), new String( strBuf, 0, i1 - 1 ));
						
						mapCueTypes.put(new Integer( i3 ), new Integer ( MD_MAGIC ));
						
						chunkLen -= i1;
						i2		 -= i1;
						break;
						
					default:
						break;
					}
					if( i2 != 0 ) {
						raf.seek( raf.getFilePointer() + i2 );
						chunkLen -= i2;
					}
				} // while( chunkLen >= 8 )
			} */
			
			
			// load annotations
			missingdata = new ArrayList<MissingData>();
			if( mdlMagicOff > 0L ) {
				raf.seek( mdlMagicOff );
				for( long chunkLen = mdlMagicLen; chunkLen >= 8; ) {
					i	= raf.readInt();		// sub chunk ID
					i1	= readLittleInt();
					i2	= (i1 + 1) & 0xFFFFFFFE;	// sub chunk length
					chunkLen -= 8;
					switch( i ) {
					case MD_MAGIC:
						if( strBuf == null || strBuf.length < i1 ) {
							strBuf  = new byte[ Math.max( 64, i1 )];
						}
						raf.readFully( strBuf, 0, i1 );	// null-terminated
						// System.out.println("b" + new String( strBuf, 0, i1 - 1 ));
						missingdata.add(new MissingData(new String( strBuf, 0, i1 - 1 )));							
						chunkLen -= i1;
						i2		 -= i1;
						break;
					default:
						break;
					}
					if( i2 != 0 ) {
						raf.seek( raf.getFilePointer() + i2 );
						chunkLen -= i2;
					}
				}
			}
			if( !missingdata.isEmpty() ) afd.setProperty( EMDDFileDescr.KEY_MISSINGDATA, missingdata );
			
			
			
			/* if( cueMagicOff > 0L ) {
				raf.seek( cueMagicOff );
				i	= readLittleInt();	// num cues
				for( int j = 0; j < i; j++ ) {
					i1	= readLittleInt();	// dwIdentifier
					raf.readInt();			// dwPosition (ignore, we don't use playlist)
					i2	= raf.readInt();	// should be 'data'
					raf.readLong();			// ignore dwChunkStart and dwBlockStart
					i3	= readLittleInt();	// dwSampleOffset (fails for 64bit space)
					if( i2 == DATA_MAGIC ) {
						mapCues.put( new Integer( i1 ), new Integer( i3 ));
					}
				}
			} */
			
			
			
			// resolve markers and regions
			/* if( !mapCueNames.isEmpty() ) {
				missingdata = new ArrayList<MissingData>();
				
				for( Iterator<Integer> iter = mapCues.keySet().iterator(); iter.hasNext(); ) {
					o	= iter.next();
					i	= ((Integer) mapCues.get( o )).intValue();	// start frame
					str	= (String) mapCueNames.get( o );
					type= ((Integer) mapCueTypes.get(o));
					o	= mapCueLengths.get( o );
					
					switch(type) {
					case MD_MAGIC:
						if( str == null) str = "THIS IS A MISSING DATA CHUNK";
						missingdata.add(new MissingData(str));
						break;
					default:
						break;
					}
	
				}
				
				System.out.println("ready" + missingdata.size());

				if( !missingdata.isEmpty() ) afd.setProperty( EMDDFileDescr.KEY_MISSINGDATA, missingdata );
			} */
			
			
		}
		
		
		// LP: added cnfgMagicOff == 0 to allow reading EMDDF Config
		protected void readMarkers() throws IOException {
			if( (listMagicOff == 0L) && (annoMagicOff == 0L) && (cnfgMagicOff == 0)) return;
			
			final Map<Integer, Integer>	mapCues			= new HashMap<Integer, Integer>();
			final Map<Integer, Integer>	mapCueLengths	= new HashMap<Integer, Integer>();
			final Map<Integer, String>	mapCueNames		= new HashMap<Integer, String>();
			final Map<Integer, Integer>	mapCueTypes		= new HashMap<Integer, Integer>();
			final long	oldPos			= raf.getFilePointer();
			final List<Marker>	markers, notes;
			final List<Annotation> comments, metadata;
			final List<Region>	regions;
			final Info SURF_info;
			final Config config;
			
			int			i, i1, i2, i3, i4, i5;
			Object		o;
			String		str;
			int			type;
			byte[]		strBuf			= null;

			try {
				if( listMagicOff > 0L ) {
					raf.seek( listMagicOff );
					for( long chunkLen = listMagicLen; chunkLen >= 8; ) {
						i	= raf.readInt();				// sub chunk ID
						i1	= readLittleInt();
						i2	= (i1 + 1) & 0xFFFFFFFE;		// sub chunk length
						chunkLen -= 8;
						switch( i ) {
						case LABL_MAGIC:
							i3		  = readLittleInt();	// dwIdentifier
							i1		 -= 4;
							i2	     -= 4;
							chunkLen -= 4;
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							mapCueNames.put( new Integer( i3 ), new String( strBuf, 0, i1 - 1 ));
							
							mapCueTypes.put(new Integer( i3 ), new Integer ( LABL_MAGIC ));
							
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case NOTE_MAGIC:
							i3		  = readLittleInt();	// dwIdentifier
							i1		 -= 4;
							i2	     -= 4;
							chunkLen -= 4;
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							mapCueNames.put( new Integer( i3 ), new String( strBuf, 0, i1 - 1 ));
							
							mapCueTypes.put(new Integer( i3 ), new Integer ( NOTE_MAGIC ));
							
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case LTXT_MAGIC:
							i3			= readLittleInt();	// dwIdentifier
							i4			= readLittleInt();	// dwSampleLength (= frames)
							i5			= raf.readInt();	// dwPurpose
							raf.readLong();					// skip wCountry, wLanguage, wDialect, wCodePage
							i1			-= 20;
							i2			-= 20;
							chunkLen	-= 20;
							o			 = new Integer( i3 );
							if( (i1 > 0) && !mapCueNames.containsKey( o )) {	// don't overwrite names
								if( strBuf == null || strBuf.length < i1 ) {
									strBuf  = new byte[ Math.max( 64, i1 )];
								}
								raf.readFully( strBuf, 0, i1 );	// null-terminated
								mapCueNames.put( (Integer) o, new String( strBuf, 0, i1 - 1 ));
								
								mapCueTypes.put(new Integer( i3 ), new Integer ( LTXT_MAGIC ));
								
								chunkLen -= i1;
								i2		 -= i1;
							}
							if( (i4 > 0) || (i5 == RGN_MAGIC) ) {
								mapCueLengths.put( (Integer) o, new Integer( i4 ));
							}
							break;
							
						default:
							break;
						}
						if( i2 != 0 ) {
							raf.seek( raf.getFilePointer() + i2 );
							chunkLen -= i2;
						}
					} // while( chunkLen >= 8 )
				}
				
				if( cueMagicOff > 0L ) {
					raf.seek( cueMagicOff );
					i	= readLittleInt();	// num cues
					for( int j = 0; j < i; j++ ) {
						i1	= readLittleInt();	// dwIdentifier
						raf.readInt();			// dwPosition (ignore, we don't use playlist)
						i2	= raf.readInt();	// should be 'data'
						raf.readLong();			// ignore dwChunkStart and dwBlockStart
						i3	= readLittleInt();	// dwSampleOffset (fails for 64bit space)
						if( i2 == DATA_MAGIC ) {
							mapCues.put( new Integer( i1 ), new Integer( i3 ));
						}
					}
				}
	
				// resolve markers and regions
				if( !mapCues.isEmpty() ) {
					markers = new ArrayList<Marker>();
					regions	= new ArrayList<Region>();
					notes 	= new ArrayList<Marker>();
					
					for( Iterator<Integer> iter = mapCues.keySet().iterator(); iter.hasNext(); ) {
						o	= iter.next();
						i	= ((Integer) mapCues.get( o )).intValue();	// start frame
						str	= (String) mapCueNames.get( o );
						type= ((Integer) mapCueTypes.get(o));
						o	= mapCueLengths.get( o );
						
						switch(type) {
						case LABL_MAGIC:
							if( str == null) str = "THIS IS A LABEL CHUNK";
							markers.add(new Marker( i, str));
							break;
						case NOTE_MAGIC:
							if( str == null) str = "THIS IS A NOTE CHUNK";
							notes.add(new Marker( i, str));
							break;
						case LTXT_MAGIC:
							if( str == null ) str = "THIS IS A LTXT CHUNK";
							regions.add( new Region( new Span( i, ((Integer) o).intValue() ), str )); // change here to the original span
							break;
						default:
							break;
						}
		
					}
					if( !markers.isEmpty() ) afd.setProperty( EMDDFileDescr.KEY_LABELS, markers );
					if( !regions.isEmpty() ) afd.setProperty( EMDDFileDescr.KEY_REGIONS, regions );
					if( !notes.isEmpty() ) afd.setProperty( EMDDFileDescr.KEY_NOTES, notes );
				}
				
				// load annotations
				comments = new ArrayList<Annotation>();
				metadata = new ArrayList<Annotation>();
				if( annoMagicOff > 0L ) {
					raf.seek( annoMagicOff );
					for( long chunkLen = annoMagicLen; chunkLen >= 8; ) {
						i	= raf.readInt();		// sub chunk ID
						i1	= readLittleInt();
						i2	= (i1 + 1) & 0xFFFFFFFE;	// sub chunk length
						chunkLen -= 8;
						switch( i ) {
						case COMT_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							comments.add(new Annotation(new String( strBuf, 0, i1 - 1 )));							
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case META_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							metadata.add(new Annotation(new String( strBuf, 0, i1 - 1 )));
							chunkLen -= i1;
							i2		 -= i1;
							break;
						default:
							break;
						}
						if( i2 != 0 ) {
							raf.seek( raf.getFilePointer() + i2 );
							chunkLen -= i2;
						}
					}
				}
				if( !comments.isEmpty() ) afd.setProperty( EMDDFileDescr.KEY_COMMENTS, comments );
				if( !metadata.isEmpty() ) afd.setProperty( EMDDFileDescr.KEY_METADATA, metadata );
				
				config = new Config();
				if(cnfgMagicOff > 0) {
					//config.num_channels = afd.channels;
					float[] cc = new float[afd.channels];
					int cc_count = 0;
					raf.seek( cnfgMagicOff );
					for( long chunkLen = cnfgMagicLen; chunkLen >= 8; ) {
						i	= raf.readInt();		// sub chunk ID
						i1	= readLittleInt();
						i2	= (i1 + 1) & 0xFFFFFFFE;	// sub chunk length
						chunkLen -= 8;
						switch( i ) {
						case TMST_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							// afd.SURF_initial_timestamp = new String( strBuf, 0, i1 - 1 );
							config.surf_initial_timestamp = new String( strBuf, 0, i1 - 1 );
							//System.out.println(new String( strBuf, 0, strBuf.length ));
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case TMZN_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							
							//raf.readFully( strBuf, i1-1, 30 );	// null-terminated
							raf.readFully(strBuf, 0, i1);
							// afd.SURF_timezone = raf.readLine();
							// afd.SURF_timezone = new String( strBuf, 0, strBuf.length ).split("SPRT")[0].toString();
							//config.surf_timezone = new String( strBuf, 0, strBuf.length ).split("SPRT")[0].toString();
							config.surf_timezone = new String( strBuf, 0, i1 - 1 );
							//System.out.println(config.surf_timezone);
							//System.out.println(new String( strBuf, 0, i1 - 1 ));
							//System.out.println(new String( strBuf, 0, strBuf.length ));
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case SPRT_MAGIC:
							// afd.SURF_sample_rate = raf.readFloat();
							config.surf_sampling_rate = raf.readFloat();
							//System.out.println(config.surf_sampling_rate);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case CHCC_MAGIC:
							//System.out.println("read cc");
							cc[cc_count] = raf.readFloat();
							chunkLen -= i1;
							i2		 -= i1;
							//System.out.println(raf.readFloat());
							//System.out.println(cc_count + " " + cc[cc_count]);
							cc_count ++;
							break;
						default:
							//System.out.println("Not a valid MAGIC");
							break;
						}
						if( i2 != 0 ) {
							raf.seek( raf.getFilePointer() + i2 );
							chunkLen -= i2;
						}
					}
					// afd.SURF_channel_calibration = cc;
					config.surf_channel_callibration = cc;
				}
				// else {
					// throw new IOException( getResourceString( "errAudioFileIncomplete" ));
				// }
				//TODO ELSE THROW ERROR
				afd.setProperty(EMDDFileDescr.KEY_CONFIG, config);
				
				
				if(infoMagicOff > 0) {
					raf.seek( infoMagicOff );
					SURF_info = new Info();
					for( long chunkLen = infoMagicLen; chunkLen >= 8; ) {
						i	= raf.readInt();		// sub chunk ID
						i1	= readLittleInt();
						i2	= (i1 + 1) & 0xFFFFFFFE;	// sub chunk length
						chunkLen -= 8;
						// System.out.println("FOURCC:" + i);
						// System.out.println("FourCC String: " + FourCCtoString(i));
						switch( i ) {						
						case IARL_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.archival_location = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.archival_location);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case IART_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.file_creator = new String( strBuf, 0, i1 - 1,"UTF-8" );
							// System.out.println(SURF_info.file_creator);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ICMS_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.commissioner = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.commissioner);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ICMT_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.comments = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.comments);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ICOP_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.copyright = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.copyright);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ICRD_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.creation_date = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.creation_date);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case IKEY_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.keywords = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.keywords);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case INAM_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.name = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.name);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case IPRD_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.product = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.product);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ISBJ_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.subject = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.subject);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ISFT_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.software = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.software);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ISRC_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.source = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.source);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						case ISRF_MAGIC:
							if( strBuf == null || strBuf.length < i1 ) {
								strBuf  = new byte[ Math.max( 64, i1 )];
							}
							raf.readFully( strBuf, 0, i1 );	// null-terminated
							SURF_info.source_form = new String( strBuf, 0, i1 - 1 );
							// System.out.println(SURF_info.source_form);
							chunkLen -= i1;
							i2		 -= i1;
							break;
						default:
							System.out.println("Not a valid MAGIC");
							break;
						}
						if( i2 != 0 ) {
							raf.seek( raf.getFilePointer() + i2 );
							chunkLen -= i2;
						}
					}
					afd.setProperty(EMDDFileDescr.KEY_INFO, SURF_info);
				}
			}
			finally {
				raf.seek( oldPos );
			}
		}
		
		public String FourCCtoString(int value) {
			String s = "";
			s += (char) ((value >> 24) & 0xFF);
			s += (char) ((value >> 16) & 0xFF);
			s += (char) ((value >> 8) & 0xFF);
			s += (char) (value & 0xFF);
			return s;
		}
	} // class WAVEHeader
	
	
	
	private class RawHeader extends AudioFileHeader
    {
        protected RawHeader() { /* empty */ }

        // this never get's called because
        // retrieveType will never say it's a raw file
        protected void readHeader( EMDDFileDescr descr ) throws IOException
        { /* empty */ }

        // naturally a raw file doesn't have a header
        protected void writeHeader( EMDDFileDescr descr ) throws IOException
        { /* empty */ }

        protected void updateHeader( EMDDFileDescr descr ) throws IOException
        { /* empty */ }

        protected long getSampleDataOffset()
        {
            return 0L;
        }

        protected ByteOrder getByteOrder()
        {
            return ByteOrder.BIG_ENDIAN;		// XXX check compatibility, e.g. with csound linux
        }
    } // class RawHeader
	

	// http://www.vcs.de/fileadmin/user_upload/MBS/PDF/Whitepaper/Informations_about_Sony_Wave64.pdf
    private class Wave64Header extends AbstractRIFFHeader
    {
        private static final int RIFF_MAGIC1a	= 0x72696666;	// 'riff'
        private static final int RIFF_MAGIC1b	= 0x2E91CF11;
        private static final long RIFF_MAGIC1	= 0x726966662E91CF11L;
        private static final long RIFF_MAGIC2	= 0xA5D628DB04C10000L;

        private static final long WAVE_MAGIC1	= 0x77617665F3ACD311L;	// 'wave'-XXXX
        private static final long WAVE_MAGIC2	= 0x8CD100C04F8EDB8AL;

        // chunk identifiers
        private static final long FMT_MAGIC1	= 0x666D7420F3ACD311L;	// 'fmt '-XXXX
        private static final long FMT_MAGIC2	= 0x8CD100C04F8EDB8AL;
        private static final long FACT_MAGIC1	= 0x66616374F3ACD311L;	// 'fact'-XXXX
        private static final long FACT_MAGIC2	= 0x8CD100C04F8EDB8AL;
        private static final long DATA_MAGIC1	= 0x64617461F3ACD311L;	// 'data'-XXXX
        private static final long DATA_MAGIC2	= 0x8CD100C04F8EDB8AL;

//		private static final long LIST_MAGIC1	= 0x6C6973742F91CF11L; // 'list'-XXXX
//		private static final long LIST_MAGIC2	= 0xA5D628DB04C10000L;
        private static final long MARKER_MAGIC1	= 0x5662F7AB2D39D211L;
        private static final long MARKER_MAGIC2	= 0x86C700C04F8EDB8AL;

        private final Charset charset = Charset.forName( "UTF-16LE" );
        private long		markersOffset			= 0L;
        private static final long riffLengthOffset	= 16L;

        protected Wave64Header() { /* empty */ }

        protected void readHeader( EMDDFileDescr descr )
        throws IOException
        {
            int		i, i1, i2, i3, essentials, bpf = 0;
            long	len, magic1, magic2, chunkLen, dataLen = 0;

            raf.readLong(); raf.readLong();		// riff
            len	= readLittleLong();
            raf.readLong(); raf.readLong();		// wave
            len	   -= 40;
            chunkLen = 0;

//// System.out.println( "len = " + len );

            for( essentials = 2; (len >= 24) && (essentials > 0); ) {
                if( chunkLen != 0 ) raf.seek( raf.getFilePointer() + chunkLen );	// skip to next chunk

                magic1		= raf.readLong();
                magic2		= raf.readLong();
                chunkLen	= (readLittleLong() + 7) & 0xFFFFFFFFFFFFFFF8L;

//// System.out.println( "magic1 = " + magic1 + "; chunkLen = " + chunkLen + "; pos = " + raf.getFilePointer() );

                len		   -= chunkLen;
                chunkLen   -= 24;

                if( magic1 == FMT_MAGIC1 && magic2 == FMT_MAGIC2 ) {
                    essentials--;
                    i					= readLittleUShort();		// format
                    descr.channels		= readLittleUShort();		// # of channels
                    i1					= readLittleInt();			// sample rate (integer)
                    descr.rate			= i1;
                    i2					= readLittleInt();			// bytes per frame and second (=#chan * #bits/8 * rate)
                    bpf					= readLittleUShort();		// bytes per frame (=#chan * #bits/8)
                    descr.bitsPerSample	= readLittleUShort();		// # of bits per sample
                    if( ((descr.bitsPerSample & 0x07) != 0) ||
                        ((descr.bitsPerSample >> 3) * descr.channels != bpf) ||
                        ((descr.bitsPerSample >> 3) * descr.channels * i1 != i2) ) {

                        throw new IOException( getResourceString( "errAudioFileEncoding" ));
                    }
                    unsignedPCM			= bpf == 1;

                    chunkLen -= 16;

                    switch( i ) {
                    case FORMAT_PCM:
                        descr.sampleFormat = EMDDFileDescr.FORMAT_INT;
                        break;
                    case FORMAT_FLOAT:
                        descr.sampleFormat = EMDDFileDescr.FORMAT_FLOAT;
                        break;
                    case FORMAT_EXT:
                        if( chunkLen < 24 ) throw new IOException( getResourceString( "errAudioFileIncomplete" ));
                        i1 = readLittleUShort();	// extension size
                        if( i1 < 22 ) throw new IOException( getResourceString( "errAudioFileIncomplete" ));
                        i2 = readLittleUShort();	// # valid bits per sample
                        raf.readInt();				// channel mask, ignore
                        i3 = readLittleUShort();	// GUID first two bytes
                        if( (i2 != descr.bitsPerSample) ||
                            ((i3 != FORMAT_PCM) &&
                            (i3 != FORMAT_FLOAT)) ) throw new IOException( getResourceString( "errAudioFileEncoding" ));
                        descr.sampleFormat = i3 == FORMAT_PCM ? EMDDFileDescr.FORMAT_INT : EMDDFileDescr.FORMAT_FLOAT;
                        chunkLen -= 10;
                        break;
                    default:
                        throw new IOException( getResourceString( "errAudioFileEncoding" ));
                    }

                } else if( magic1 == DATA_MAGIC1 && magic2 == DATA_MAGIC2 ) {
                    essentials--;
                    sampleDataOffset	= raf.getFilePointer();
                    dataLen				= chunkLen;

                } else if( magic1 == MARKER_MAGIC1 && magic2 == MARKER_MAGIC2 ) {
                	markersOffset			= raf.getFilePointer();
                }
            } // for( essentials = 2; (len > 0) && (essentials > 0); )
            if( essentials > 0 ) throw new IOException( getResourceString( "errAudioFileIncomplete" ));

            descr.length = dataLen / bpf;
        }

        protected void writeHeader( EMDDFileDescr descr )
        throws IOException
        {
            // final List		markers, regions;
            int				i1, i2;
            String			str;
            Region			region;
            Marker			marker;
            Annotation		annotation;
            MissingData		md;
            
            List<Marker>		labels, notes;
			List<Region>		regions;
			List<Annotation>	comments, metadata;
			List<MissingData>	missingdata;
			 
            
            long			pos, pos2, n1, n2;

            isFloat = descr.sampleFormat == EMDDFileDescr.FORMAT_FLOAT;	// floating point requires FACT extension
            raf.writeLong( RIFF_MAGIC1 );
            raf.writeLong( RIFF_MAGIC2 );
            raf.writeLong( 40 );		// Laenge inkl. RIFF-Header (Dateilaenge); unknown now
            raf.writeLong( WAVE_MAGIC1 );
            raf.writeLong( WAVE_MAGIC2 );

            // ---- fmt Chunk ----
            raf.writeLong( FMT_MAGIC1 );
            raf.writeLong( FMT_MAGIC2 );
            writeLittleLong( isFloat ? 42 : 40 );  // FORMAT_FLOAT has extension of size 0
            writeLittleShort( isFloat ? FORMAT_FLOAT : FORMAT_PCM );
            writeLittleShort( descr.channels );
            i1 = (int) (descr.rate + 0.5);
            writeLittleInt( i1 );
            i2 = (descr.bitsPerSample >> 3) * descr.channels;
            writeLittleInt( i1 * i2 );
            writeLittleShort( i2 );
            writeLittleShort( descr.bitsPerSample );

            if( isFloat ) {
//				raf.writeShort( 0 );
                raf.writeLong( 0 ); // actually a short, but six extra bytes to align to 8-byte boundary
            }

            // ---- fact Chunk ----
            if( isFloat ) {
                raf.writeLong( FACT_MAGIC1 );	
                raf.writeLong( FACT_MAGIC2 );
                writeLittleLong( 32 );
                factSmpNumOffset = raf.getFilePointer();
//				raf.writeInt( 0 );
                raf.writeLong( 0 ); // i guess it should be long???
            }

            // -- marker Chunk ----
            // markers  = (List) descr.getProperty( SURFFileDescr.KEY_MARKERS );
            // regions  = (List) descr.getProperty( SURFFileDescr.KEY_REGIONS );
            labels  	= (List<Marker>) descr.getProperty( EMDDFileDescr.KEY_LABELS ); 	// appliance activity  -> LABEL chunks
			regions  	= (List<Region>) descr.getProperty( EMDDFileDescr.KEY_REGIONS ); 	// user activities 	 -> LABELED TEXT chunks
			notes  		= (List<Marker>) descr.getProperty( EMDDFileDescr.KEY_NOTES ); 		// localized metadata 	 -> NOTE chunks
			comments 	= (List<Annotation>) descr.getProperty(EMDDFileDescr.KEY_COMMENTS);	// comments in the annotations chunk
			metadata 	= (List<Annotation>) descr.getProperty(EMDDFileDescr.KEY_METADATA);	// metadata in the annotations chunks
			missingdata 	= (List<MissingData>) descr.getProperty(EMDDFileDescr.KEY_MISSINGDATA);	// missingdata
                       
            if( ((missingdata != null) && !missingdata.isEmpty()) || ((labels != null) && !labels.isEmpty()) || ((regions != null) && !regions.isEmpty()) || ((notes != null) && !notes.isEmpty()) || ((comments != null) && !comments.isEmpty()) || ((metadata != null) && !metadata.isEmpty())) {

                final CharsetEncoder	enc		= charset.newEncoder();
                CharBuffer				cbuf	= null;
                ByteBuffer				bbuf	= null;
                
                // Saves all the information of the cue chunk (labels, notes, regions, comments and metadata)
                final List[] cues = {
                    // markers == null ? Collections.EMPTY_LIST : markers,
                    regions == null ? Collections.EMPTY_LIST : regions,
                    labels == null ? Collections.EMPTY_LIST : labels,
                    notes == null ? Collections.EMPTY_LIST : notes,
                    comments == null ? Collections.EMPTY_LIST : comments,
                    metadata == null ? Collections.EMPTY_LIST : metadata,
                    missingdata == null ? Collections.EMPTY_LIST : missingdata
                };
                
                
                

                raf.writeLong( MARKER_MAGIC1 );
                raf.writeLong( MARKER_MAGIC2 );

                pos	= raf.getFilePointer();
                raf.writeLong( 0 ); // updated afterwards
                i2	= cues[ 0 ].size() + cues[ 1 ].size() + cues[2].size() + cues[3].size() + cues[4].size() + cues[5].size();
                writeLittleInt( i2 );
                // CUE64 structures
                
                                
                
                for( int i = 0, id = 1; i < 6; i++ ) {
                    for( int j = 0; j < cues[ i ].size(); j++, id++ ) {
                    	switch (i) {
	                    	case 0: // Regions
	                    		 region = (Region) cues[ i ].get( j );
	                             n1		= region.span.start;
	                             n2		= region.span.getLength(); // start + getLength = stop
	                             str	= region.name;
	                             break;
	                    	case 1: // Labels
	                    		 marker = (Marker) cues[ i ].get( j );
	                             n1		= marker.pos;
	                             n2		= 0;
	                             str	= marker.name;
	                             break;
	                    	case 2: // Notes 
	                    		marker = (Marker) cues[ i ].get( j );
	                             n1		= marker.pos;
	                             n2		= 1;
	                             str	= marker.name;
	                             break;
	                    	case 3: // Comments
	                    		 annotation = (Annotation) cues[ i ].get( j );
	                             n1		= -1; // Not used
	                             n2		= 2;
	                             str	= annotation.content;
	                             break;
	                    	case 5:
	                    		 md = (MissingData) cues[ i ].get( j );
	                             n1		= -1; // Not used
	                             n2		= 5;
	                             str	= md.toJSON();
	                             break;
	                    	default: // Metadata
	                    		 annotation = (Annotation) cues[ i ].get( j );
	                             n1		= -1; // Not used
	                             n2		= 3;
	                             str	= annotation.content;
	                    		break;
                    	}
                        /* if( i == 0 ) { // Regions
                            marker	= (Marker) cues[ i ].get( j );
                            n1		= marker.pos;
                            n2		= -1;
                            str		= marker.name;
                        } else if (i == 1) { // Labes
                        	
                        } else if (i == 2) { // Notes
                        	
                        } else if (i == 3) { // Comments
                        	
                        } else if (i == 4) { // Metadat
                        	
                        } else { // i == 5
                            region = (Region) cues[ i ].get( j );
                            n1		= region.span.start;
                            n2		= region.span.getLength();
                            str		= region.name;

                        } */
                        writeLittleInt( id );	// marker ID
                        raf.writeInt( 0 );		// padding
                        writeLittleLong( n1 );	// position
                        writeLittleLong( n2 );	// length

                        if( (cbuf == null) || (cbuf.capacity() < str.length()) ) {
                            cbuf = CharBuffer.allocate( str.length() + 8 );
                            bbuf = ByteBuffer.allocate( (cbuf.capacity() + 1) << 1 );
                        }
                        cbuf.clear();
                        cbuf.put( str );
                        cbuf.flip();
                        bbuf.clear();
                        enc.reset();
                        enc.encode( cbuf, bbuf, true );
                        enc.flush( bbuf );
                        bbuf.putShort( (short) 0 ); // null term
                        bbuf.flip();

                        writeLittleInt( bbuf.remaining() );
                        raf.writeInt( 0 );		// padding
//// System.out.println( "writing " + bbuf.remaining() + " bytes at " + fch.position() );
                        fch.write( bbuf );
                    }
                }

                // update chunk size
                pos2 = raf.getFilePointer();
                n1	 = pos2 - pos;
//// System.out.println( "n1 = " + n1 + "; pos = " + pos + "; pos2 = " + pos2 + "; pad = " + (int) (((n1 + 7) & 0xFFFFFFFFFFFFFFF8L) - n1) );
                final int pad = (int) (((n1 + 7) & 0xFFFFFFFFFFFFFFF8L) - n1);
                for( int i = 0; i < pad; i++ ) raf.write( 0 );	// padding byte

                raf.seek( pos );
                writeLittleLong( n1 + 16 );
//				writeLittleLong( n1 + 16 + pad );
                raf.seek( pos2 + pad );

            } // if marker or region list not empty

            // data Chunk (Header)
            raf.writeLong( DATA_MAGIC1 );
            raf.writeLong( DATA_MAGIC2 );
            dataLengthOffset = raf.getFilePointer();
            raf.writeLong( 24 );
            sampleDataOffset = raf.getFilePointer();

            updateHeader( descr );
        }

        protected void updateHeader( EMDDFileDescr descr )
        throws IOException
        {
            final long oldPos	= raf.getFilePointer();
            final long len		= raf.length();
            if( len == lastUpdateLength ) return;
            final long lenM8	= len - 8;

            if( lenM8 >= riffLengthOffset ) {
                raf.seek( riffLengthOffset );
                writeLittleLong( len );		// riff Chunk len
            }
            if( isFloat && (lenM8 >= factSmpNumOffset) ) {
                raf.seek( factSmpNumOffset );
                writeLittleLong( descr.length * descr.channels );			// fact: Sample-Num XXX check multich.!
            }
            if( lenM8 >= dataLengthOffset ) {
                raf.seek( dataLengthOffset );
                writeLittleLong( len - (dataLengthOffset - 16) );	// data Chunk len
            }
            raf.seek( oldPos );
            lastUpdateLength = len;
        }

        protected long getSampleDataOffset()
        {
            return sampleDataOffset;
        }

        protected ByteOrder getByteOrder()
        {
            return ByteOrder.LITTLE_ENDIAN;
        }

        protected boolean isUnsignedPCM()
        {
            return unsignedPCM;
        }

        protected void readMarkers()
        throws IOException
        {
        	// System.out.println( "markersOffset = " + markersOffset );
        
            
            if( markersOffset == 0L ) return;

            // final List				markers	= new ArrayList();
            // final List				regions	= new ArrayList();
            
            List<Marker> labels = new ArrayList(), notes = new ArrayList();
            List<Region> regions = new ArrayList();
            List<Annotation> comments = new ArrayList(), metadata = new ArrayList();
            List<MissingData> missingdata = new ArrayList();
            
            final CharsetDecoder	dec		= charset.newDecoder();
            CharBuffer				cbuf	= null;
            ByteBuffer				bbuf	= null;
            long					n1, n2;
            int						numBytes;
            String					str;
            CoderResult				result;

            final long oldPos = raf.getFilePointer();
            try {
                raf.seek( markersOffset );
                for( int numCues = readLittleInt(), cue = 0; cue < numCues; cue++ ) {
//// System.out.println( "cue " + (cue+1) + " of " + numCues );
                    raf.readInt();					// marker ID (ignore)
                    raf.readInt(); 					// padding
                    n1			= readLittleLong();	// pos
                    n2			= readLittleLong();	// length (-1 for markers)
                    numBytes	= readLittleInt();	// size of name string in bytes
                    raf.readInt(); 					// padding

                    if( bbuf == null || bbuf.capacity() < numBytes ) {
                        bbuf = ByteBuffer.allocate( numBytes + 16 );
                        cbuf = CharBuffer.allocate( bbuf.capacity() >> 1 );
                    }

                    bbuf.rewind().limit( numBytes );

//// System.out.println( "reading " + bbuf.remaining() + " bytes from " + fch.position() );

                    fch.read( bbuf );
                    if( (numBytes >= 2) &&
                        (bbuf.get( numBytes - 2 ) == 0) &&
                        (bbuf.get( numBytes - 1 ) == 0) ) { // null term

                        bbuf.rewind().limit( numBytes - 2 );
                    } else {
                        bbuf.flip();
                    }
                    cbuf.clear();
                    dec.reset();
                    result = dec.decode( bbuf, cbuf, true );
                    if( result.isError() ) {
                        throw new IOException( "Error Reading Cue Name" +
                           (result.isMalformed() ? ": Malformed Input" :
                           (result.isOverflow() ? ": Overflow" :
                           (result.isUnderflow() ? ": Underflow" :
                           (result.isUnmappable() ? ": Unmappable" : "")))));
                    }
                    dec.flush( cbuf );
                    cbuf.flip();
                    str = cbuf.toString();

                    
                    // System.out.println( "n1 = " + n1 + "; n2 = " + n2 + "; name  = '" + str + "'" );
                    
                    
                    if (n2 == 0) { // Labels
                    	 labels.add(new Marker(n1, str));
                    } else if (n2 == 1) { // Notes
                    	notes.add(new Marker(n1, str));
                    } else if (n2 == 2) { // Comments
                    	comments.add(new Annotation(str));
                    } else if (n2 == 3) { // Metadata
                    	metadata.add(new Annotation(str));
                    } else if (n2 == 5) { // MissingData
                    	missingdata.add(new MissingData(str));
                    } else { // Regions
                        regions.add(new Region(new Span(n1, n1 + n2), str));
                    }
                 
                    
                }

                afd.setProperty(EMDDFileDescr.KEY_LABELS, labels);
                afd.setProperty(EMDDFileDescr.KEY_NOTES, notes);
                afd.setProperty(EMDDFileDescr.KEY_REGIONS, regions);
                afd.setProperty(EMDDFileDescr.KEY_COMMENTS, comments);
                afd.setProperty(EMDDFileDescr.KEY_METADATA, metadata);
                afd.setProperty(EMDDFileDescr.KEY_MISSINGDATA, missingdata);

            }
            finally {
                raf.seek( oldPos );
            }
        }
    } // class Wave64Header

} // class AudioFile