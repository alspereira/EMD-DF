/*     */ package emddf.file;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Comparator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Annotation
/*     */   implements Cloneable, Comparable<Object>, Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1259571589708158936L;
/*  35 */   public static final Comparator<Object> nameComparator = new NameComparator();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public final String content;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final int TYPE_UNKNOWN = -1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final int TYPE_COMMENT = 0;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final int TYPE_METADATA = 1;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 	/**
			 * Constructor of an Annotation. Receives as an argument one string with the content.
			 * @param content
			 */
/*     */   public Annotation(String content)
/*     */   {
/*  65 */     this.content = content;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ /**
			 * Constructor of an Annotation. Receives as an argument another annotation, and copies its content.
			 * @param content
			 */
/*     */   public Annotation(Annotation orig)
/*     */   {
/*  75 */     this.content = orig.content;
/*     */   }
/*     */   
/*     */ 

/*     */ 
/*     */   public Object clone()
/*     */     throws CloneNotSupportedException
/*     */   {
	 			// Calls the clone of the superclasse (in this case, since it doesn't extend from another class, his superclass is the "father" of all classes - class Object)
				// The clone method creates a copy of one object and returns it.
				return super.clone();
/*     */   }
/*     */   
	
			/**
			 * It returns an hash code for the content.
			 */
/*     */   public int hashCode() {
/*  91 */     return this.content.hashCode();
/*     */   }

			/**
			 * This method returns if one object has the same content as another one.<br>
			 * If one object is null or its class isn't Annotation, then, of course they are not equal.<br>
			 * Otherwise, it verifies if the content are the same.
			 */
/*     */   public boolean equals(Object o) {
/*  95 */     if ((o != null) && ((o instanceof Annotation))) {
/*  96 */       Annotation m = (Annotation)o;
/*  97 */       return this.content.equals(m.content);
/*     */     }
/*  99 */     return false;
/*     */   }
/*     */   
/*     */ 


			// Returns 0 when compares 2 objects.
/*     */   public int compareTo(Object o)
/*     */   {
/* 118 */     return 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 

			/**
			 * This method adds one Annotation to a list of annotations.<br>
			 * It returns the quantity of annotations in the list, before adding this one.<br>
			 * @param annotations
			 * @param annotation
			 * @return number of annotations before add this annotation
			 */
/*     */   public static int add(List<Annotation> annotations, Annotation annotation)
/*     */   {
/* 131 */     annotations.add(annotation);
/* 132 */     return annotations.size() - 1;
/*     */   }
/*     */   


			/**
			 * This method allows to find the first annotation in the list, with the content given in the params, starting in the given index (startIndex).<br>
			 * Returns -1 if it doesn't find annotations with that content.<br>
			 * In the other hand, if there's one annotation with that content, returns the position of the annotation (index) of the list, where the content was found.
			 * @param annotations
			 * @param content
			 * @param startIndex
			 * @return
			 */
/*     */   public static int find(List<Annotation> annotations, String content, int startIndex)
/*     */   {
/* 154 */     for (int i = startIndex; i < annotations.size(); i++) {
/* 155 */       if (((Annotation)annotations.get(i)).content.equals(content))
/* 156 */         return i;
/*     */     }
/* 158 */     return -1;
/*     */   }
/*     */   

			/**
			 * This class allows to compare 2 objects. They can be of type String or Annotation.<br>
			 * It uses string compareTo method (0 if they have the same size, positive if the argument's size is greater or negative if the argument's size is lower).<br>
			 * If both of type String, it compares the strings.<br>
			 * If both of type Annotation, it compares their content.<br>
			 * If one of type Annotation and the other one of type String, it compares the string to the content of the annotation.
			 */
/*     */   private static class NameComparator
/*     */     implements Comparator<Object>
/*     */   {
/*     */     public int compare(Object o1, Object o2)
/*     */     {
/* 167 */       if ((o1 instanceof String)) {
/* 168 */         if ((o2 instanceof String))
/* 169 */           return ((String)o1).compareTo((String)o2);
/* 170 */         if ((o2 instanceof Annotation))
/* 171 */           return ((String)o1).compareTo(((Annotation)o2).content);
/* 172 */         if ((o1 instanceof Annotation)) {
/* 173 */           if ((o2 instanceof String))
/* 174 */             return ((Annotation)o1).content.compareTo((String)o2);
/* 175 */           if ((o2 instanceof Annotation)) {
/* 176 */             return ((Annotation)o1).content.compareTo(((Annotation)o2).content);
/*     */           }
/*     */         }
/*     */       }
/* 180 */       throw new ClassCastException();
/*     */     }
/*     */     
/*     */     public boolean equals(Object o) {
/* 184 */       return (o != null) && ((o instanceof NameComparator));
/*     */     }
/*     */   }
/*     */ }


/* Location:              /Users/lucaspereira/Desktop/emdf.df.jar!/emddf/file/Annotation.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */