package emddf.file;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class MissingData {

	private String initial_timestamp = "";
	private long initial_sample = 0;

	
	public MissingData(String aInitialTimestamp, long aInitialSample) {
		this.initial_timestamp = aInitialTimestamp;
		this.initial_sample = aInitialSample;
	}
	
	
	public MissingData(String aContent) {
		
		JSONObject json = new JSONObject();
		try {
			 json = (JSONObject) parseJson(aContent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error parsing json!");
			e.printStackTrace();
		}
		
		this.initial_timestamp = json.get("initial_timestamp").toString();
		this.initial_sample = Long.valueOf(json.get("initial_sample").toString()).longValue();
	}

	
	private Object parseJson(String json) throws Exception{
		JSONParser jsonParser = new JSONParser();
		return jsonParser.parse(json);
	}
	
	
	public String toString() {
		return initial_timestamp + " " + initial_sample;
	}
	
	
	public String toJSON() {
		
		JSONObject jsonObjMD = new JSONObject();
		jsonObjMD.put("initial_timestamp", initial_timestamp);
		jsonObjMD.put("initial_sample", initial_sample);
		
		return jsonObjMD.toString();
	}

	
	public String getInitialTimestamp() {
		return initial_timestamp;
	}
	
	public long getInitialSample() {
		return initial_sample;
	}




}
