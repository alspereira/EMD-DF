/**
 * Contains tools to read information from an existing EMD-DF file.
 */
package emddf.api.read;