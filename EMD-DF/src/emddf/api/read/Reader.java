package emddf.api.read;

import emddf.file.Annotation;
import emddf.file.Config;
import emddf.file.Info;
import emddf.file.MissingData;
import emddf.file.EMDDFile;
import emddf.file.EMDDFileDescr;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import de.sciss.io.Marker;
import de.sciss.io.Region;

/**
 * Used to read information from an existing EMD-DF file.
 * In order to read the metadata of a file, the method {@code readMarkers()} must be called
 * In order to read the samples of the file, the method {@code ReadSync()} must be called
 * @author Nuno Velosa
 *
 */
public class Reader {
	
	private File file;
	
	// The size of the buffer is initialized with value 1000.
	private int bufferSize = 600000;
	
	
	// Number of samples remaining when numBuffers buffers are full with bufferSize samples
    private int samplesRemaining = 0; 
	
	
	// Saves all the buffers that contains frames
	private List<ArrayBlockingQueue<float[][]>> allBuffers;
	
	// SURF File
	private EMDDFile SURF_file_IN;
	
	// In the description of the file, there's an hashmap to save some properties of the file.
	// There's also some variables like bitsPerSample, channels, type, rate, sampleFormat, length, etc.
	// There are 2 constructors. One without parameters, where the hashmap is initialized.
	// The another constructor receives a SURFFileDescr object (and it's basically a copy)
	private EMDDFileDescr SURF_descr_IN;
	
	
	/**
	 * Constructor to initialize the information about the file.
	 * @param file The file we want to read
	 */
	public Reader(File file) {
		this.file = file;
		// This try statement is used to check if the file exists. So, if it doesn't exist, one IO exception will occur.
		try {
			
			allBuffers = new ArrayList<ArrayBlockingQueue<float[][]>>();
		
			// Creates one audio file just to read
			// Receives the file and creates one SURFFile (audio file) with read only mode.
			// Creates the descriptor empty (the properties hashmap is empty)
			this.SURF_file_IN = EMDDFile.openAsRead(file);

			
			if (SURF_file_IN.getDescr().type != SURF_file_IN.getDescr().TYPE_RAW) {
				
				// Reads the markers from the header of the audio file (labels, comments, etc)
				this.SURF_file_IN.readMarkers();

			}
			
			// Returns the description of the file (type SURFFileDescr) like bitsPerSample, sampleFormat, length, channels, rate, etc.
			this.SURF_descr_IN = this.SURF_file_IN.getDescr();
			
			bufferSize = (int) Math.min(SURF_descr_IN.length, 600000);

		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @implNote We have 2 constructors. One where you just send the file, and the other one where you send the file and the size of the buffer.
	 * It means that, when you create a new file, you can just specify the file or both (file and the buffer size).
	 */
	public Reader(File file, int bufferSize) {
		this.file = file;
		this.bufferSize = (int) Math.min(bufferSize, 600000); // buffer size can't be more than 600 000
	}
	/**
	 * It closes the random access file stream (Java.io.RandomAccessFile) using a try-catch statement.
	 */
	public void close() {
		this.SURF_file_IN.cleanUp();
	}
	  
	
	
	
	 
	/**
	 * @implNote One description contains one map with some properties, like labels, notes, regions, etc.
	 * @implNote Each element of an hashmap contains a key and a value and we can get the value with the key.
	 * @implNote So, in this case, the key is labels and the value is the value saved.
	 * @implNote Returns the value of hashmap where the key is labels.
	 * @return The labels of the file
	 */
	public ArrayList<Marker> getLabels() {
		ArrayList<Marker> labels = (ArrayList<Marker>)this.SURF_descr_IN.getProperty("labels");
	 	return labels;
	}
	  
	/**
	 * Returns the value of hashmap where the key is notes.
	 * @return The notes of the file
	 */
	public ArrayList<Marker> getNotes() {
		ArrayList<Marker> notes = (ArrayList<Marker>)this.SURF_descr_IN.getProperty("notes");
		return notes;
	}
	 
	/**
	 * Returns the value of hashmap where the key is regions.
	 * @return The regions of the file
	 */
	public ArrayList<Region> getRegions() {
		ArrayList<Region> regions = (ArrayList<Region>)this.SURF_descr_IN.getProperty("regions");
		return regions;
	}
	 
	/**
	 * Returns the value of hashmap where the key is comment.
	 * @return The comments of the file
	 */
	public ArrayList<Annotation> getComments() {
		ArrayList<Annotation> comments = (ArrayList<Annotation>)this.SURF_descr_IN.getProperty("comment");
		return comments;
	}

	/**
	 * Returns the value of hashmap where the key is metadata.
	 * @return The metadata of the file
	 */
	public ArrayList<Annotation> getMetadata() {
		ArrayList<Annotation> metadata = (ArrayList<Annotation>)this.SURF_descr_IN.getProperty("metadata");
		return metadata;
	}
	  
	/**
	 * Returns the value of hashmap where the key is info.
	 * @return The info of the file
	 */
	public Info getInfo() {
		Info SURFinfo = (Info)this.SURF_descr_IN.getProperty("info");
		return SURFinfo;
	}
	
	/**
	 * Returns the value of hashmap where the key is missing_data.
	 * @return The missing data from the file.
	 */
	public ArrayList<MissingData> getMissingData() {
		ArrayList<MissingData> missingdata = (ArrayList<MissingData>)this.SURF_descr_IN.getProperty("missingdata");
		return missingdata;
	}
	 
	
	/**
	 * Creates a config with the information of the SURFFile. Basically it copies the data from the SURFFile to the config object, and returns it.
	 * @return a config object
	 */
	public Config getConfig() {
		
		if (!isEMDDF()) {
			// this is not an EMDDF file. Return null
			return null;
		}
		else {
			// this is an EMDDF file. Create Config
		/*Config cnfg = new Config(
					this.SURF_descr_IN.SURF_initial_timestamp, 
					this.SURF_descr_IN.SURF_timezone, 
					this.SURF_descr_IN.SURF_sample_rate, 
					this.SURF_descr_IN.SURF_channel_calibration, 
					this.SURF_descr_IN.channels, 
					(float)this.SURF_descr_IN.rate, 
					this.SURF_descr_IN.bitsPerSample
					);
		
			return cnfg;*/
			return (Config) this.SURF_descr_IN.getProperty(EMDDFileDescr.KEY_CONFIG);
		}
	}
	
	// check if it is a valid EMDDF file
	private boolean isEMDDF64() {
		return (this.SURF_descr_IN.SURF_initial_timestamp != null &&
				this.SURF_descr_IN.SURF_timezone != null &&
				this.SURF_descr_IN.SURF_sample_rate > 0  &&
				this.SURF_descr_IN.SURF_channel_calibration != null);
	}
	
	private boolean isEMDDF() {
		Config cnfg = (Config) this.SURF_descr_IN.getProperty(EMDDFileDescr.KEY_CONFIG);
		if (cnfg == null)
			return false;
		else 
			return cnfg.isEMDDF();
	}
	
	public long getFinalTimestamp() {
		long initial = Long.parseLong("1446840524599");
		long finalTimestamp = initial + Math.round((getNumFrames()/getRate())*1000);
		return finalTimestamp;
	}
	
	/**
	 * Gets the initial timestamp of the missing data chunk which sample is 
	 * @param aSample
	 */
	public MissingData getInitialTimestamp (long aSample) {
		//long initial = Long.parseLong("1446840524599");
		Config conf = (Config) this.SURF_descr_IN.getProperty(EMDDFileDescr.KEY_CONFIG);
		
		String initialTimestamp = conf.surf_initial_timestamp;
		//String initialTimestamp = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new java.util.Date (initial));
		// Timestamp do ficheiro
		MissingData timestamp = new MissingData(initialTimestamp, 0);
		int i = 0;
		
		try {
			for (i=0; i<getMissingData().size(); i++) {
				
				if (aSample >= getMissingData().get(i).getInitialSample()) {
					
					if (aSample < getMissingData().get(i+1).getInitialSample()) {
						return getMissingData().get(i);
					}
					
					// I++
					
				} else {
					return timestamp;
				}
				
			}
			
		} catch (NullPointerException e) {
			
		} catch (IndexOutOfBoundsException e) {
			return getMissingData().get(i);
		}
		
		return timestamp;
		
	}
	
	
	
	
	/**
	 * This method returns a timestamp of a label taking in account the missing data chunks. 
	 * @param aSample
	 * @return
	 * @throws ParseException 
	 */
	public String getTimestamp (long aSample) throws ParseException {
		
		MissingData md = getInitialTimestamp(aSample);
		
		long epoch = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(md.getInitialTimestamp()).getTime() / 1000;
		
		// System.out.println(md.getInitialTimestamp());
		//System.out.println(epoch);
		// System.out.println(Math.round((1000*(aSample - md.getInitialSample()))/getRate() + epoch));
		
		long num = Math.round((1000*(aSample - md.getInitialSample()))/getRate() + epoch*1000);
		//System.out.println("num is "+ num);
		String date = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new java.util.Date (num));
		return date;
		
	}
	  
	
	/**
	 * Starts the thread, giving the number of samples.
	 * Reads the file without using syncronization.
	 * @param buffer_size The desired buffer size 
	 * @param start The position to start reading
	 * @param samples The number of samples to read
	 */
	public void Read(int buffer_size, int start, int samples) {
		
		ArrayBlockingQueue<float[][]> buffer = new ArrayBlockingQueue<float[][]>(buffer_size);

		
	    new Thread(new ReaderThread1(buffer, start, samples)).start();
	}
	
	/**
	 * Starts the thread, without giving the number of samples (num_samples = 0).
	 * Reads the file without using syncronization.
	 * @param buffer_size The desired buffer size 
	 * @param start The position to start reading
	 */
	public void Read(int buffer_size, int start) {
		
		ArrayBlockingQueue<float[][]> buffer = new ArrayBlockingQueue<float[][]>(buffer_size);

		
	     new Thread(new ReaderThread1(buffer, start, 0)).start();
	}
	   
	/**
	 * Needs to specify as an argument, the number of samples.
	 * Reads the file using syncronization.
	 * @param buffer_size The desired buffer size 
	 * @param start The position to start reading
	 * @param samples The number of samples to read
	 */
	public void ReadSync(int buffer_size, int start, int samples) {
		
		ArrayBlockingQueue<float[][]> buffer = new ArrayBlockingQueue<float[][]>(buffer_size);

	    ReadFile(buffer, start, samples);
	}
	
	
	/**
	 * Doesn't need to specify the number of samples (num_samples = 0).
	 * Reads the file using syncronization.
	 * @param buffer_size The desired buffer size 
	 * @param start The position to start reading
	 */
	public void ReadSync(int buffer_size, int start) {
		
		ArrayBlockingQueue<float[][]> buffer = new ArrayBlockingQueue<float[][]>(buffer_size);

		ReadFile(buffer, start, 0);
	}
	
	
	/**
	 * This method allows to read one file.
	 * @param buffer The buffer to place the samples
	 * @param start The position to start reading
	 * @param samples The number of samples to read
	 */
	private void ReadFile(ArrayBlockingQueue<float[][]> buffer, int start, int samples) {
		ArrayBlockingQueue<float[][]> surfData = buffer;
				
	    int offset = start;
	    //int totalSamples = 0;
	    int numSamples = 0;
	    int numBuffers = 0;
	    int channels = 0;	
	    	    
	    try {
	    	// Creates the file just for read
	    	EMDDFile surfFile = EMDDFile.openAsRead(this.file);
	    		    	
	    	// Gets the number of channels of this file
	    	channels = surfFile.getChannelNum();
	    	
	    	//totalSamples = (int)surfFile.getFrameNum();
	    	
	    	// If there are samples, the number of samples is the number received as a parameter
	    	// If there are not samples, the number of samples is the number of frames 
	    	if (samples == 0) {
	    		numSamples = (int)surfFile.getFrameNum() - offset;
	    	} else {
	    		numSamples = samples;
	    	}

	    	
	    	
	    	// The samples will be saved in buffers. The buffers have a constant size.
    		// To know the number of buffers, it's just number of samples / buffer size.
	    	if (numSamples % this.bufferSize == 0) {
	    		// If the rest of the division is 0 then the number of buffers is a integer
	    		// And these buffers are full (so, there's no samples remaining)
	    		numBuffers = numSamples / this.bufferSize;
	
	    	}
	    	else {
	    		// In this case, we use ceil to round up. Because of this, the buffers created, will have some space not used (yet).
	    		numBuffers = (int)Math.ceil(numSamples / this.bufferSize);

	    		// Samples remaining are the space of the buffer that is not used yet.
	    		// Means the number of positions available in the buffer for new samples.
	    		// It is a negative number (because in this case, the buffers are not full)
	    		samplesRemaining = numSamples - numBuffers * this.bufferSize;

	    	}
	    	
    		
	
	    	
	    	// Two-dimensional array with channels * bufferSize positions
	    	// It is used to save the frames
	    	// frames[0][] - first channel
	    	// frames[1][] - second channel, etc.
	    	float[][] frames = new float[channels][this.bufferSize];
	      
	    	// Moves the file pointer to a specific frame.
	    	// The offset (equivalent to start) is the index of the frame.
	    	// It calculates the physical file pointer and moves the file pointer according to that value.
	    	// This physical file pointer (in bytes) is the offset position (in bytes) from the beginning of the file.
	    	surfFile.seekFrame(offset);
	    		    	
	    	int saveNumBuffers = numBuffers;
	    	
	    	
	    	while (numBuffers > 0) {
	    		
	    		// While there's buffers available, it creates a new two-dimensional array
	    		frames = new float[channels][this.bufferSize];
	    		// Reads the frames from 0 to bufferSize, of the file "surfFile" and saves in array "frames"
	    		surfFile.readFrames(frames, 0, this.bufferSize);

	    		
	    		// Goes to next frame (in this case, next buffer, since length = bufferSize) after read the frames.
	    		numBuffers--;
	    		// Saves the buffer before reinitialize/rewrite the two-dimensional array
	    		surfData.put(frames);

	    		// Initializes arrayBlockingQueue that will save the buffers
	    		// (Because java works with pointers, and when surfData is cleared, allBuffers.get(0), for example, is also cleared)
	    		ArrayBlockingQueue<float[][]> copy = new ArrayBlockingQueue<float[][]>(1000);


	    		// This arraylist is used to save some buffers before clear it.
	    		// The surfData is an ArrayBlockingQueue. It means that, when the size of the ArrayBlockingQueue reaches the maximum (ArrayBlockingQueue is full), it blocks.
	    		// When 1000 buffers saved in the ArrayBlockingQueue, it adds the ArrayBlockingQueue to an ArrayList and resets the ArrayBlockingQueue for the next buffers.
	    		if (surfData.size() == 1000) {
	    				    			
	    			int totalSize = surfData.size();

	    			
	    			for (int i=0; i<totalSize; i++) {
	    				copy.add(surfData.take());
	    			}
	    			

	    			
	    			allBuffers.add(copy); // Copy, not surfData, so we can clear surfData without changing allBuffers
	    			surfData.clear();
	    			
	    		} else if (surfData.size() == saveNumBuffers - allBuffers.size()*1000) {
	    			// Add the rest of the samples (less than 1000)
	    			// So it means that it is the last element
	    			
	    			// allBuffers.add(surfData);
	    			
	    		}
	    		
	    			    		
	    	}

	    	
	    	// If there's samples remaining, it will read this remaining samples (from 0 to samplesRemaining)
	    	// It's like the example above but there's no buffers and it ends in samplesRemaining instead of bufferSize
	       if (samplesRemaining > 0) {
	    	   frames = new float[channels][samplesRemaining];
	    	   surfFile.readFrames(frames, 0, samplesRemaining);
	    	   surfData.put(frames);
	    	   
	    	   
	    	   // System.out.println("LLL" + surfData.size());
	    	   allBuffers.add(surfData);
	      }
	       
	       
	      
	       surfFile.cleanUp(); // Close the random access file stream
	    }
	    catch (IOException|InterruptedException e) {
	      e.printStackTrace();
	      System.exit(-1);
	    }
	 }
	
	
	
	
	/**
	 * @return List that contains all the samples.
	 */
	public List<ArrayBlockingQueue<float[][]>> getSamplesList() {
		return allBuffers;
		
	}
	
	
	
	/**
	 * @return Number of channels in the audio file.
	 */
	public int getNumChannels() {
		return SURF_file_IN.getChannelNum();	
	}
	
	
	
	/**
	 * @return The number of samples in the audio file.
	 */
	public long getNumFrames() {
		return SURF_descr_IN.length;
	}
	
	
	
	/**
	 * @return The samplerate (frequency in Hz).
	 */
	public double getRate() {
		
		return SURF_descr_IN.rate;
		
	}
	
	
	/**
	 * @return The bits per sample of the file
	 */
	
	public double getBitsPerSample() {
		return SURF_descr_IN.bitsPerSample;
	}
	
	
	
	/**
	 * @return Return the filetype. <ul><strong><li>0 - WAVE</li> <li>5 - W64</li></strong></ul>

	 */
	public int getAudioType() {
		return SURF_descr_IN.getType();
	}
	
	
	
	
	/**
	 * @return The type of the audio file (in text).
	 */
	public String getType() {
		if (SURF_descr_IN.getType() == SURF_descr_IN.TYPE_RAW) {
			return "RAW";
		} else if (SURF_descr_IN.getType() == SURF_descr_IN.TYPE_UNKNOWN) {
			return "UNKNOWN";
		} else if (SURF_descr_IN.getType() == SURF_descr_IN.TYPE_WAVE) {
			return "WAVE";
		} else if (SURF_descr_IN.getType() == SURF_descr_IN.TYPE_WAVE64) {
			return "WAVE64";
		} else {
			return "UNKNOWN";
		}
	}
	
	
	/**
	 * @return The number of samples of the audio file.
	 */
	public int getNumSamples() {
		
		int c = 0;
		ArrayBlockingQueue<float[][]> temp;
		
		for (int i=0; i<allBuffers.size(); i++) { // allBuffers saves each 1000 buffers as an element

			temp = allBuffers.get(i); // Gets an ArrayBlockingQueue with probably 1000 buffers or less (if there's no more than 1000 buffers)
			
			for (Object obj : temp) { // For each ArrayBlockingQueue (1000 buffers), get's the bidimensional array saved in each buffer
				
				float[][] tempFrame = (float[][]) obj; // Cast
				
				// Each frames[][]
				
				for (int m=0; m<getNumChannels(); m++) { // Number of channels
										
					for (int k=0; k < tempFrame[m].length; k++) { // Number of samples in each channel
						c++;
					}
										
				}
				
			}
			
		
		}
		
		return c;
	
	}
	
	
	
	
	/**
	 * This method shows/prints all the samples of the audio file in the console.
	 */
	public void printSamples() {
		
		ArrayBlockingQueue<float[][]> temp;
		
		
		for (int i=0; i<allBuffers.size(); i++) { // allBuffers saves each 1000 buffers as an element

			temp = allBuffers.get(i); // Gets an ArrayBlockingQueue with probably 1000 buffers or less (if there's no more than 1000 buffers)
			
			for (Object obj : temp) { // For each ArrayBlockingQueue (1000 buffers), get's the bidimensional array saved in each buffer
				
				float[][] tempFrame = (float[][]) obj; // Cast
								
				for (int m=0; m<getNumChannels(); m++) { // Number of channels
										
					for (int k=0; k < tempFrame[m].length; k++) { // Number of samples in each channel
						System.out.println(tempFrame[m][k]);
					}
										
				}
				
			}
			
		
		}
		
				
	}
	
	
	
	
	
	
	/**
	 * Thread to read the file
	 * This class has the run method and implements Runnable, since it is a thread.
	 */
	 private class ReaderThread1 implements Runnable {
		 
		 // ArrayBlockingQueue it's basically a queue (which type is 2-dimensional float array) with a fixed size (queue + array). It blocks when the queue is full.
		 private ArrayBlockingQueue<float[][]> surfData;
	   
		 int offset = 0;
		 int numSamples = 0;
	    
		 // I made a change here, in the type of _surfData
		 // Receives the arguments to the ReadFile method.
		 public ReaderThread1(ArrayBlockingQueue<float[][]> _surfData, int _offset, int _numSamples) {
			 this.offset = _offset;
			 this.surfData = _surfData;
			 this.numSamples = _numSamples;
		 }
	  
		 // The method run is what the thread runs. 
		 // In this case, it reads the file in the thread
		 public void run() {
			 Reader.this.ReadFile(this.surfData, this.offset, this.numSamples);
		 }
	  }
 }