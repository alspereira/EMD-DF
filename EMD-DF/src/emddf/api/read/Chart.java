package emddf.api.read;


import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.axis.AAxis;
import info.monitorenter.gui.chart.axis.AxisLinear;
import info.monitorenter.gui.chart.labelformatters.LabelFormatterDate;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyMinimumViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.util.Range;

import javax.swing.JFrame;
import javax.swing.JLabel;




/**
 * @author lucaspereira
 *
 */
public class Chart extends JFrame implements Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int chartSize;
	private int numChannels;

	private Chart2D pChart = new Chart2D();

	private ITrace2D pTrace;
	private ITrace2D qTrace;
	private ITrace2D[] traces;
	
	AAxis yAxis;
	AAxis xAxis;
	
	
	Reader reader;
	
	public List<ArrayBlockingQueue<float[][]>> samplesQueue;
	
	
	private Color[] colors;
	
	public Chart(int maxChartSamples, int numChannels, Reader reader) {
        this.chartSize = maxChartSamples;
        this.numChannels = numChannels;
        this.reader = reader;
        colors = new Color[] {Color.RED, Color.BLUE, Color.CYAN, Color.ORANGE, Color.GREEN, Color.MAGENTA, Color.YELLOW};
        init();
	}
	
	public void setPowerSamplesQueue(List<ArrayBlockingQueue<float[][]>> powerSamplesQueue) {
		this.samplesQueue = powerSamplesQueue;
	}
	
	private void init() {
		traces = new Trace2DLtd[numChannels];
		
		
		for (int i=0; i<numChannels; i++) {
			traces[i] = new Trace2DLtd(chartSize);
		}
		
		
		// pTrace = new Trace2DLtd(chartSize);
    	// qTrace = new Trace2DLtd(chartSize);

    	yAxis = new AxisLinear();
    	xAxis = new AxisLinear();
    	//yAxis.getAxisTitle().setTitle("Power (W | VAR)");
    	 
    	pChart.setAxisY(yAxis);
    	//pChart.setAxisYLeft(yAxis);
    	
    	xAxis.setFormatter(new LabelFormatterDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")));
        //xAxis.getAxisTitle().setTitle("Time");
        //pChart.setAxisXBottom(xAxis);
        pChart.setAxisX(xAxis);
        
		yAxis.setRangePolicy(new RangePolicyMinimumViewport(new Range(-1, 1)));
		
		//pChart.addTrace(pTrace, xAxis, yAxis);
		//pChart.addTrace(qTrace, xAxis, yAxis);
		
		
		for (int i=0; i<numChannels; i++) {
			pChart.addTrace(traces[i]);
		}
		
		// pChart.addTrace(pTrace);
		// pChart.addTrace(qTrace);
		
		// qTrace.setColor(Color.RED);
		// pTrace.setColor(Color.BLUE);
	
		
		for (int i=0; i<numChannels; i++) {
			traces[i].setColor(colors[i]);
			traces[i].setName("Channel " + (i+1));
		}
		// pTrace.setName("Real Power (W)");
		// qTrace.setName("Reactive Power (VAR)");
		
		this.getContentPane().add(pChart);

		this.setSize(800,300);
		this.setVisible(true);
	}
	
	public List<ArrayBlockingQueue<float[][]>> getPowerSamplesQueue() {
		if(this.samplesQueue == null)
			this.samplesQueue = new ArrayList<ArrayBlockingQueue<float[][]>>();
		
		return this.samplesQueue;
	}
	

	@Override
	public void run() {
		
	
		ArrayBlockingQueue<float[][]> temp;


		int counter = 0;
		
		for (int i=0; i<samplesQueue.size(); i++) { // allBuffers saves each 1000 buffers as an element
			
			temp = samplesQueue.get(i); // Gets an ArrayBlockingQueue with probably 1000 buffers or less (if there's no more than 1000 buffers)
			
			float[][] tempFrame = new float[1][1];

			// for (Object obj : temp) { // For each ArrayBlockingQueue (1000 buffers), get's the bidimensional array saved in each buffer
			while (temp.size() > 0) {
				
				System.out.println("sq" + samplesQueue.size() + "\nt" + temp.size());
			
				try {
					tempFrame = temp.take();
					System.out.println("pause");
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// float[][] tempFrame = (float[][]) obj; // Cast
																		
					for (int k=0; k < tempFrame[0].length; k++) { // Number of samples in each channel

						counter++;

						for (int m=0; m<tempFrame.length; m++) { // Number of channels
							
							
							// VER O PROBLEMA
							/*try {
								traces[m].addPoint(Long.parseLong(reader.getTimestamp(counter)[1]), tempFrame[m][k]);
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} */
							// traces[m].addPoint(1604075968000l+(50000*k), tempFrame[m][k]);
						
						}

					}
														
			}
			
		
		}
		
		// float[][] z= new float[2][2];
		
		/* try {
			z = samplesQueue.take();
		} catch (InterruptedException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */
		/* System.out.println("ESt� alguem em casa"+ z.length);
		for(int i=0; i<z.length; i++) {
			pTrace.addPoint(1604075968000l+z.length, z[i][1]);
		} */
		
		
		/* for (Object obj : z) { // For each ArrayBlockingQueue (1000 buffers), get's the bidimensional array saved in each buffer
			
			float[][] tempFrame = (float[][]) obj; // Cast
							
			for (int m=0; m<1; m++) { // Number of channels
									
				for (int k=0; k < tempFrame[m].length; k++) { // Number of samples in each channel
					System.out.println(tempFrame[m][k]);
				}
									
			}
			
		} */
		
		
		/*Integer ps;
		while(true) {
			try {
				ps = samplesQueue.take();
				
			} catch (InterruptedException e1) {
					e1.printStackTrace();
			}
		}*/
	}
}
