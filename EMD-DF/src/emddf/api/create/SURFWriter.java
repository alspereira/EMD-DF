package emddf.api.create;
 
import emddf.file.Annotation;
import emddf.file.Info;
import emddf.file.MissingData;
import emddf.file.EMDDFile;
import emddf.file.EMDDFileDescr;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import de.sciss.io.Marker;
import de.sciss.io.Region;
 
/**
 *  This class is a thread, since it implements Runnable and has the method run.
 */
public class SURFWriter implements Runnable {
	
	private File file_OUT; // Normal file
	private EMDDFile SURF_file_OUT; // SURF File
	private EMDDFileDescr SURF_descr_OUT; // SURF File description
	private String file_OUT_path = ""; // The path of the file
	private String file_OUT_name = ""; // The name of the file
   
	private ArrayBlockingQueue<float[][]> source;
	private List<ArrayBlockingQueue<float[][]>> list;
   
	private long maxFileSizeInSamples;
	private int maxFileSizeInMegaBytes = 1024;
	private boolean mustQuit = false; // Variable to stop the thread
	
	private int option;
	private int fileType;
	
	
	// Constructor to initialize the variables of the description of the format of the file
	// When an ArrayBlockingQueue has 1000 or less buffers, this constructor can be used (receives one ArrayBlockingQue).
	// If the ArrayBlockingQueue has more than 1000 buffers, it's better to create one list of ArrayBlockingQueues (where each one doesn't have more than 1000 buffers)
	/**
	 * Constructor to initialize the variables of the description of the format of the file
	 * @implNote When an ArrayBlockingQueue has 1000 or less buffers, this constructor can be used (receives one ArrayBlockingQue).
	 * @implNote If the ArrayBlockingQueue has more than 1000 buffers, it's better to create one list of ArrayBlockingQueues (where each one doesn't have more than 1000 buffers) 
	 * @param fileType The  filetype of the file to convert.<ul><strong><li>0 - WAVE</li> <li>5 - W64</li></strong></ul>
	 * @param filePath The path in which the file should be created
	 * @param fileName The filename of the new file
	 * @param source An ArrayBlockingQueue
	 * @param sampleRate  The sample rate of the file
	 * @param bitsPerSample Bits per sample of the audio file
	 * @param channels Number of channels of the audio file
	 * @param maxFileSizeMB The maximum size a file should have, if this value is exceeded, more files will be created
	 */
	public SURFWriter(int fileType, String filePath, String fileName, ArrayBlockingQueue<float[][]> source, int sampleRate, int bitsPerSample, int channels, int maxFileSizeMB) {
		this.source = source;
		this.maxFileSizeInMegaBytes = maxFileSizeMB;
		
		
		this.option = 1;
		this.fileType = fileType;
		
		
		file_OUT_path = filePath;
		file_OUT_name = fileName;
		
		// Calculates the max size in samples (the maximum quantity of samples that one file can have)
		// 1 MB = 1000000 bytes
		this.maxFileSizeInSamples = (this.maxFileSizeInMegaBytes * 1000000 / (channels * (bitsPerSample / 8)));
     
		this.SURF_descr_OUT = new EMDDFileDescr();
		this.SURF_descr_OUT.rate = sampleRate;
		this.SURF_descr_OUT.bitsPerSample = bitsPerSample;
     	this.SURF_descr_OUT.channels = channels;
     	this.SURF_descr_OUT.type = fileType;
     	
     	if (SURF_descr_OUT.SURF_channel_calibration == null) {
     		SURF_descr_OUT.SURF_channel_calibration = new float[]{1,1};
		}
     	
     	// Verifies if it's FORMAT_INT or FORMAT_FLOAT
     	if (bitsPerSample <= 16) // Integer
     		this.SURF_descr_OUT.sampleFormat = 0; // Format Integer
     	else
     		this.SURF_descr_OUT.sampleFormat = 1; // Format Float
	}
	
	
	
	
	
	/**
	 * Constructor to initialize the variables of the description of the format of the file
	 * @implNote When an ArrayBlockingQueue has more than 1000 buffers, this constructor should be used (receives one List of ArrayBlockingQueue).
	 * @implNote Since, each ArrayBlockingQueue should have the maximum of 1000 buffers
	 * @param fileType The  filetype of the file to convert.<ul><strong><li>0 - WAVE</li> <li>5 - W64</li></strong></ul>
	 * @param list A list of ArrayBlockingQueues
	 * @param sampleRate  The sample rate of the file
	 * @param bitsPerSample Bits per sample of the audio file
	 * @param channels Number of channels of the audio file
	 * @param maxFileSizeMB The maximum size a file should have, if this value is exceeded, more files will be created
	 */
	 
	public SURFWriter(int fileType, String filePath, String fileName, List<ArrayBlockingQueue<float[][]>> list, int sampleRate, int bitsPerSample, int channels, int maxFileSizeMB) {
		this.list = list;
		this.maxFileSizeInMegaBytes = maxFileSizeMB;
		
		
		this.option = 2;
		this.fileType = fileType;
		
		
		file_OUT_path = filePath;
		file_OUT_name = fileName;
		
		
		// Calculates the max size in samples (the maximum quantity of samples that one file can have)
		this.maxFileSizeInSamples = (this.maxFileSizeInMegaBytes * 1000000 / (channels * (bitsPerSample / 8)));
     
		this.SURF_descr_OUT = new EMDDFileDescr();
		this.SURF_descr_OUT.rate = sampleRate;
		this.SURF_descr_OUT.bitsPerSample = bitsPerSample;
     	this.SURF_descr_OUT.channels = channels;
     	this.SURF_descr_OUT.type = fileType;
     	
     	if (SURF_descr_OUT.SURF_channel_calibration == null) {
     		SURF_descr_OUT.SURF_channel_calibration = new float[]{1,1};
		}
     	
     	// Verifies if it's FORMAT_INT or FORMAT_FLOAT
     	if (bitsPerSample <= 16) // Integer
     		this.SURF_descr_OUT.sampleFormat = 0; // Format Integer
     	else
     		this.SURF_descr_OUT.sampleFormat = 1; // Format Float
	}
	
   
	
	/**
	 * This method allows to add labels to the file. 
	 * @param labels A list of markers(labels) to add to the file.
	 * @implNote This list of labels is saved in one hashmap where are the properties of the file (properties).The key of this hashmap is "labels" and the value is the list "labels". 
	 */
	public void setLabels(List<Marker> labels) {
		this.SURF_descr_OUT.setProperty("labels", labels);
	}
   
	
	/**
	 * This method allows to add notes to the file.
	 * @param notes A list of markers(notes) to add to the file.
	 * @implNote Basically notes are lists (since the file can have more than one note).
	 * @implNote And this list of notes is saved in one hashmap where are the properties of the file (properties).
	 * @implNote The key of this hashmap is "notes" and the value is the list "notes".
	 */
   public void setNotes(List<Marker> notes) {
	   this.SURF_descr_OUT.setProperty("notes", notes);
   }
   
   
   /**
	 * This method allows to add regions to the file
	 * @param regions A list of markers(regions) to add to the file.
	 * @implNote Basically regions are lists (since the file can have more than one region).
	 * @implNote And this list of regions is saved in one hashmap where are the properties of the file (properties).
	 * @implNote The key of this hashmap is "regions" and the value is the list "regions".
	 */
   public void setRegions(List<Region> regions) {
	   this.SURF_descr_OUT.setProperty("regions", regions);
   }
   
   
   /**
	 * This method allows to add comments to the file.
	 * @param comments A list of markers(comments) to add to the file.
	 * @implNote Basically comments are lists (since the file can have more than one comment).
	 * @implNote And this list of comments is saved in one hashmap where are the properties of the file (properties).
	 * @implNote The key of this hashmap is "comments" and the value is the list "comments".
	 */
   public void setComments(List<Annotation> comments) {
	   this.SURF_descr_OUT.setProperty("comment", comments);
   }
   
   
   /**
	 * This method allows to add metadatas to the file.
	 * @param metadata A list of markers(metadata) to add to the file.
	 * @implNote Basically metadatas are lists (since the file can have more than one metadata).
	 * @implNote And this list of metadatas is saved in one hashmap where are the properties of the file (properties).
	 * @implNote The key of this hashmap is "metadata" and the value is the list "metadata".
	 */
   public void setMetadata(List<Annotation> metadata) {
	   this.SURF_descr_OUT.setProperty("metadata", metadata);
   }
   
   
   public void setMissingData(List<MissingData> missingdata) {
	   this.SURF_descr_OUT.setProperty("missingdata", missingdata);
   }
   
   
   /**
	 * This method allows to add some information to the file.
	 * Namely, file creator, keywords, name, product, subject, creation_date, comments, etc.
	 * @see emddf.file.Info
	 */
   public void setInfo(Info SURFInfo) {
	   this.SURF_descr_OUT.setProperty("info", SURFInfo);
   }
   
   
   /**
    * This method allows to start the thread.
    */
   public void start() {
	   new Thread(this).start();
   }
   
   
   /**
    * This method allows to stop the thread, using a boolean variable (mustQuit).
    * When mustQuit = true, the threads stops.
    */
   public void quit() {
	   this.mustQuit = true;
   }
   
   
   /**
    * This is the run method where the thread runs. The thread runs in a while loop, namely, while mustQuit = false.
    * When this method is running the file is getting created.
    */
   public void run() {
	   long unixTimestamp = 0L;
	   int fileCount = 1;
	   long sampleCount = 0L;
	   
	   int i = 0;
	       
	   // When mustQuit = true, this loop stops, so, the thread stops
	   while (!this.mustQuit) {
		   
		   // Creates one file with the following path (and name): path/timestamp_name-fileCount.wav
		   
		   String extension;
		   if (fileType == SURF_descr_OUT.TYPE_WAVE64) {
			   extension = ".w64";
		   } else {
			   extension = ".wav";
		   }
		   
		   this.file_OUT = new File(this.file_OUT_path + "/" + unixTimestamp + "_" + this.file_OUT_name + "-" + fileCount + "" + extension);

		   // Saves the file in the EMDDFile descriptor, to verify if already exists or not (in openAsWrite method).
		   this.SURF_descr_OUT.file = this.file_OUT;
		   try {
			   
			   // Before creating, verifies if the audio file already exists. If yes, it deletes
			   // Creates one audio file for read and write
			   // Receives the descriptor (where can get the file) and creates one SURFFile (audio file) with read write mode.
			   // The descriptor of this audio file is the argument of openAsWrite (SURF_descr_OUT).
			   this.SURF_file_OUT = EMDDFile.openAsWrite(this.SURF_descr_OUT);
			   
			   
			   // While the quantity of samples is lower than the maximum
			   // The goal of this is to create more than one audio file if there's more than maxFileSizeInSamples samples
			   // Create new file where the maximum of samples is that value
			   while (sampleCount < this.maxFileSizeInSamples) {
				   
				   float[][] tempFrames = null;
				   
				   	if (option == 1) {
				   		
						tempFrames = (float[][])this.source.take();

				   		if (this.source.size() == 0) {
						   sampleCount = maxFileSizeInSamples;
						   quit();
				   		}
				   
			   		} if (option == 2) {
					 
					   // tempFrames saves the first element of source (the first frame, so, a set of samples) - I GUESS
					   tempFrames = (float[][])this.list.get(i).take();
					   
					   if (i == list.size()-1 && list.get(i).size() == 0) { // If it's the last list and all the elements have been wrote in the file (take removes the element that is in the head)
						   sampleCount = maxFileSizeInSamples; // Stops the second while
						   quit(); // Stops the first while
					   } else if (this.list.get(i).size() == 0) { // If it's not the last list but all the elements have been wrote in the file
						   i++; // Next list of ArrayBlockingQueue
					   }
					   
			   		}
				   
				   
					   
				   // Length of tempFrames is saved in tempFramesLength (how many samples in this frame) - I GUESS
				   int tempFramesLength = tempFrames[0].length;
				   // Writes in the audio file (SURF_file_OUT) the frames of tempFrames from 0 to tempFramesLength
				   this.SURF_file_OUT.writeFrames(tempFrames, 0, tempFramesLength);
				   // Increases tempFramesLength in the the quantity of samples
				   sampleCount += tempFramesLength;
					   
				   
				   
			   }
			   
			   			   
			   this.SURF_file_OUT.cleanUp(); // Closes the random access file stream
			   
			   // Resets the variable of the while loop
			   // In a new file, it needs to write from the begin (so, the variable is reseted).
			   sampleCount = 0L;
			   
			   // Starts at 1 and increments to avoid to have files with the same name.
			   // The name of the file has fileCount, so, we need to increment when the file closes (means new file). 
			   fileCount++;
			   
			   // System.out.println("file was sucessfully created"); 
			   
		   }
		   catch (IOException|InterruptedException e) {
			   e.printStackTrace();
		   }
		   
	   }
   	}
}