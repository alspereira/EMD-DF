package emddf.api.create;
 
import emddf.file.Annotation;
import emddf.file.EMDDFile;
import emddf.file.EMDDFileDescr;
import emddf.file.Info;
import emddf.file.MissingData;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.sciss.io.Marker;
import de.sciss.io.Region;
import de.sciss.io.Span;
 
/**
 * Used to merge several different files into a single file, the samplerate of the files must be the same, the labels will be updated with the new position on the file
 * @author Nuno Velosa
 *
 */
public class Merger implements Runnable {
   private File file_OUT; // Unique file
   private EMDDFile SURF_file_IN; // SURF File that will be copied
   private EMDDFileDescr SURF_descr_IN; // Descriptor of the SURF File that will be copied
   private EMDDFile SURF_file_OUT; // Unique SURF File
   private EMDDFileDescr SURF_descr_OUT; // Descriptor of the Unique SURF File (contains the type, the channels, the length, etc)
   private File[] files; // Array of files to merge
   private String fileName_out; // Name of the unique file
   
   
   List<Marker> labels, notes;
   List<Region> regions;
   List<Annotation> metadata, comments;
   List<MissingData> missingdata;
   Info info;
   
   
   long filesLengthSum;
   double rate;
   

   
   /**
    * @param files An array of the files to merge
    * @param fileName_out The path and filename that will be created
    */
   public Merger(File[] files, String fileName_out) {
     this.files = files;
     this.fileName_out = fileName_out;
     labels = new ArrayList<Marker>();
     notes = new ArrayList<Marker>();
     regions = new ArrayList<Region>();
     metadata = new ArrayList<Annotation>();
     comments = new ArrayList<Annotation>();
     missingdata = new ArrayList<MissingData>();
   }
   
   
   /**
    * Creates the file.
    */
   @Override
   public void run(){
	   try {
		   
		   // Creates the unique/merged file with the name given in the param
		   // this.file_OUT = new File("/users/lucaspereira/desktop/" + this.fileName_out + ".wav");
		   
		   this.file_OUT = new File(this.fileName_out + ".w64");
		   
		   // Receives the first file of the array and creates one SURFFile (audio file) with read only mode.
		   this.SURF_file_IN = EMDDFile.openAsRead(this.files[0]);
		   
		   // Reads the markers
		   this.SURF_file_IN.readMarkers();
		   
		   // Gets the description of the first file
		   this.SURF_descr_IN = this.SURF_file_IN.getDescr();
   
		   // The descriptor of the unique file uses the info of the descriptor of the first file
		   this.SURF_descr_OUT = new EMDDFileDescr();
		   this.SURF_descr_OUT.bitsPerSample = this.SURF_descr_IN.bitsPerSample;
		   this.SURF_descr_OUT.channels = this.SURF_descr_IN.channels;
		   this.SURF_descr_OUT.rate = this.SURF_descr_IN.rate;
		   this.SURF_descr_OUT.sampleFormat = this.SURF_descr_IN.sampleFormat;
		   this.SURF_descr_OUT.type = EMDDFileDescr.TYPE_WAVE64;
		  
		   // Adds to the descriptor of the unique file, the file created previously
		   this.SURF_descr_OUT.file = this.file_OUT;


		   
		   ArrayList<Marker> temp_labels, temp_notes;
		   ArrayList<Region> temp_regions;
		   ArrayList<Annotation> temp_comments, temp_metadata;
		   ArrayList<MissingData> temp_missingdata;
		   
		   // The variable rate is used to check if all the files that will be merged, have the same rate
		   // Otherwise, it won't be possible to merge them
		   rate = EMDDFile.openAsRead(this.files[0]).getDescr().rate; // Gets the rate of the first file, to compare with the others
		   
		   for (int i=0; i < this.files.length; i++) {
			   			   
			   // Opens each file for read
			   this.SURF_file_IN = EMDDFile.openAsRead(this.files[i]);
			   
			   if (rate != SURF_file_IN.getDescr().rate) {
				   System.out.println("It's not possible to merge files with different rates!");	
				   return;
			   }

			   // Gets their markers (labels, notes, regions, comments, etc)
			   this.SURF_file_IN.readMarkers();
			   
			   
			   this.SURF_descr_IN = this.SURF_file_IN.getDescr();
			   
			   
			   if (i == 0) { // The info of the file is the info of the first file
				   try {
					   info = (Info) SURF_descr_IN.getProperty("info");
				   } catch (NullPointerException e) {
				   }
			   }
			   
			   
			   try {
				   
				   temp_labels = (ArrayList<Marker>) SURF_descr_IN.getProperty("labels");
				   for (int j=0; j<temp_labels.size(); j++) {
					   // When the files are merged, the position of the labels are different, so, we need to add the length of the previously files to the position of label
					   Marker label = new Marker(temp_labels.get(j).pos + filesLengthSum, temp_labels.get(j).name);
					   labels.add(label);
				   }
				   
			   } catch (NullPointerException e) {
				   
			   }
			   
			   
			   try {
				   
				   temp_notes = (ArrayList<Marker>) SURF_descr_IN.getProperty("notes");
				   for (int j=0; j<temp_notes.size(); j++) {
					   // When the files are merged, the position of the notes are different, so, we need to add the length of the previously files to the position of note
					   Marker note = new Marker(temp_notes.get(j).pos + filesLengthSum, temp_notes.get(j).name);
					   labels.add(note);
				   }
				   
			   } catch (NullPointerException e) {
				   
			   }
			   
			   
			   try {
				   
				   temp_regions = (ArrayList<Region>) SURF_descr_IN.getProperty("regions");
				   for (int j=0; j<temp_regions.size(); j++) {
					   regions.add(temp_regions.get(j));
				   }
				   
			   } catch (NullPointerException e) {
				   
			   }
			   
			   
			   try {
				   
				   temp_comments = (ArrayList<Annotation>) SURF_descr_IN.getProperty("comment");
				   for (int j=0; j<temp_comments.size(); j++) {
					   comments.add(temp_comments.get(j));
				   }
				   
			   } catch (NullPointerException e) {
				   
			   }
			   
			   
			   try {
				   
				   temp_metadata = (ArrayList<Annotation>) SURF_descr_IN.getProperty("metadata");
				   for (int j=0; j<temp_metadata.size(); j++) {
					   metadata.add(temp_metadata.get(j));
				   }
				   
			   } catch (NullPointerException e) {
				   
			   }
			   
			   
			   try {
				   
				   temp_missingdata = (ArrayList<MissingData>) SURF_descr_IN.getProperty("missingdata");
				   for (int j=0; j<temp_missingdata.size(); j++) {
					   missingdata.add(temp_missingdata.get(j));
				   }
				   
			   } catch (NullPointerException e) {
				   
			   }
			   
			   
			   // After the merge of the file, its number of frames have to be added to the position of next file labels.
			   filesLengthSum += EMDDFile.openAsRead(this.files[0]).getDescr().length;
			   
			   
		   }
		   
		   
		   // Adds the markers (labels, notes, regions, comments and metadata) of every file that will be merged and adds these markers to the unique file that will be created
		   this.SURF_descr_OUT.setProperty("labels", labels);
		   this.SURF_descr_OUT.setProperty("notes", notes);
		   this.SURF_descr_OUT.setProperty("regions", regions);
		   this.SURF_descr_OUT.setProperty("comment", comments);
		   this.SURF_descr_OUT.setProperty("metadata", metadata);
		   this.SURF_descr_OUT.setProperty("info", info);
		   this.SURF_descr_OUT.setProperty("missingdata", missingdata);
		   
			
		   // Opens the file where all the data will be written
		   // Basically, receives one descriptor, get's its information and creates one SURFFile (audio file) with read write mode and this descriptor.
		   // Writes the header in the audio file
		   this.SURF_file_OUT = EMDDFile.openAsWrite(this.SURF_descr_OUT);
	   
		   
		   
		   // The files that will be merged are saved in one array.
		   // While there's files to merge, in the array
		   for (int i = 0; i < this.files.length; i++) {
			   
			   // Opens each file for read
			   this.SURF_file_IN = EMDDFile.openAsRead(this.files[i]);
			   
			   // Gets their descriptions (to know the size of the audio files)
			   this.SURF_descr_IN = this.SURF_file_IN.getDescr();

			   // Copies the frames of each file (SURF_file_IN) to the unique one (SURF_FILE_OUT)		   
			   this.SURF_file_IN.copyFrames(this.SURF_file_OUT, this.SURF_descr_IN.length);
			   
		   }
		   this.SURF_file_OUT.cleanUp(); // Closes the random access file stream
	   }
	   catch (IOException e) {
		   e.printStackTrace();
	   }
   }
 }