package emddf.api.create;

import java.io.File;
import java.util.ArrayList;

import de.sciss.io.Marker;
import de.sciss.io.Region;
import emddf.api.read.Reader;
import emddf.file.Annotation;
import emddf.file.Config;
import emddf.file.EMDDFileDescr;
import emddf.file.Info;
import emddf.file.MissingData;

public class Create {

	
	// Mandatory chunk to be a valid EMDDF file
	Config config;
	int fileType;
	String path, fileName;
	
	/**
	 * This is the constructor, that receives the path and the name of the file and adds the mandatory EMDDF chunks.
 	 * @param fileType The filetype of the file to convert.<ul><strong><li>0 - WAVE</li> <li>5 - W64</li></strong></ul>
	 * @param path The path to the file
	 * @param fileName The name of the file to update
	 */
	public Create(int fileType, String path, String fileName) {
		this.fileType = fileType;
		this.path = path;
		this.fileName = fileName;
	}
	
	/**
	 * This method is used to create the mandatory Config chunk of the EMDDF file.
	 * We have to specify  surf_initial_timestamp, surf_timezone, surf_sampling_rate, surf_channel_calibration, num_channels, sample_size and sampling_rate.
	 */
	public void setConfig(String surf_initial_timestamp, String surf_timezone, float surf_sampling_rate, float[] surf_channel_callibration, int num_channels, int sample_size, float sampling_rate) {
		
		int essentials = 4;
		
		Reader r = new Reader(getFile());
		
		// Get the number of channels in the file
		
		// INFO
		//if (r.getConfig() != null) {
		//	config = new Config(r.getConfig().surf_initial_timestamp, r.getConfig().surf_timezone, r.getConfig().surf_sampling_rate, r.getConfig().surf_channel_callibration, r.getConfig().num_channels, r.getConfig().sampling_rate, r.getConfig().sample_size);
		//}
		
		
		if (surf_initial_timestamp != "") {
			config.surf_initial_timestamp = surf_initial_timestamp;
			essentials --;
		}
		
		if (surf_timezone != "") {
			config.surf_timezone = surf_timezone;
			essentials --;
		}
		
		if (surf_sampling_rate != 0) {
			config.surf_sampling_rate = surf_sampling_rate;
			essentials --;
		}
		
		if (surf_channel_callibration != null) {
			config.surf_channel_callibration = surf_channel_callibration;
			// Check the number of channels
			essentials --;
		}
		
		// Variables that do not arrive directly from the Config Chunk
		//if (num_channels != 0) {
		//	config.num_channels = num_channels;
		//}
		
		//if (sample_size != 0) {
		//	config.sample_size = sample_size;
		//}
		
		//if (sampling_rate != 0) {
		//	config.sampling_rate = sampling_rate;
		//}
		
		if (essentials < 4) {
			// throw an exception
			System.out.println("Please add all the mandatory config chunks.");
		}
		
	}
	
	/**
	 * This method returns the file with the path and fileName given as a parameter
	 * @return A file object
	 */
	public File getFile() {
		File f;
		// Reads the labels of the file (Reads all file)
		if (fileType == EMDDFileDescr.TYPE_WAVE64) {
			f = new File(path + "" + fileName + ".w64");
		} else {
			f = new File(path + "" + fileName + ".wav");
		}
		return f;
	}
}
