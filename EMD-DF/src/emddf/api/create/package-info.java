/**
 * Contains tools to create new EMD-DF files
 */
package emddf.api.create;