package emddf.api.create;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

import emddf.api.read.Reader;
import emddf.file.EMDDFile;
import emddf.file.EMDDFileDescr;
/**
 * Used to convert  a dataset from one filetype to another
 * @author Manel Pereira
 * 
 */
public class Converter {

	public String fileToConvert;
	
	private File file_OUT;
	private EMDDFile SURF_file_IN;
	private EMDDFileDescr SURF_descr_IN; 
	private EMDDFile SURF_file_OUT; 
	private EMDDFileDescr SURF_descr_OUT;
	
	private int fileTypeToConvert;
	private String[] extensions;
	private EMDDFileDescr descr; // Just used in raw audio files (given as a parameter)
	private boolean rawFile; // Used to check if wants to convert a raw audio file or another type
	
	
	/**
	 * Used to convert raw files in another type, once raw audio file's don't have headers (just the data), so, to convert to wav, for example, needs to specify the information of the header.
	 * This constructor receives the file that will be converted, the file type in which it will be converted and the decriptor/heade of the file.
	 * @param fileToConvert The path to the file that will be converted.
	 * @param fileTypeToConvert The new filetype of the file to convert.<ul><li>0 - WAVE</li> <li>5 - W64</li></ul>
	 * @param descr The descriptor of the file.
	 */
	public Converter(String fileToConvert, int fileTypeToConvert, EMDDFileDescr descr) {
		this.fileTypeToConvert = fileTypeToConvert;
		this.fileToConvert = fileToConvert;
		this.descr = descr;
		rawFile = true;
		extensions = new String[]{"wav", "", "", "", "raw", "w64"}; // Array where the extensions of the files are saved (the index referes to the audio type value, like WAV is 0 and W64 is 5)	
	}
	
	/**
	 * Used to convert audio files (that are not raw) in another type
	 * This constructor receives the file that will be converted and the file type in which it will be converted
	 * @param fileToConvert The path to the file that will be converted.
	 * @param fileTypeToConvert The new filetype of the file to convert.<ul><li>0 - WAVE</li> <li>5 - W64</li></ul>
	 */
	public Converter(String fileToConvert, int fileTypeToConvert) {
		this.fileTypeToConvert = fileTypeToConvert;
		this.fileToConvert = fileToConvert;
		rawFile = false;
		extensions = new String[]{"wav", "", "", "", "raw", "w64"}; // Array where the extensions of the files are saved (the index referes to the audio type value, like WAV is 0 and W64 is 5)	
	}
	
	
	
	/**
	 * @param f The file we want to know the filetype
	 * @return The type of the audio that will be converted.
	 */
	public int getFileType(File f) {
		
		Reader r = new Reader(f); // This constructor reads the description of the audio file and its labels, comments, etc. (reads the header of the audio file)
		
		return r.getAudioType(); // After reading the header, returns its type (getAudioType)
				
	}
	
	
	/**
	 * This is the main method of this class. It's here where the conversion is done.
	 * After instancing this method needs to be called to do the grunt work.
	 */
	public void convert() {
		
		File f = new File(fileToConvert);
				
		/*
		 * Receives all path of the audio file and separates the file name and the file path
		 */
		String[] file = fileToConvert.split("." + extensions[getFileType(f)])[0].split("\\\\");
		String fileName = file[file.length-1];
		file = fileToConvert.split("\\.wav")[0].split(fileName);
		String filePath = file[0];
		
		try {
						
			this.file_OUT = new File(filePath + fileName + "." +  extensions[fileTypeToConvert]); // The extension of the file type in which the file will be converted
			
			this.SURF_file_IN = EMDDFile.openAsRead(f);
			
			this.SURF_file_IN.readMarkers();
			   
			this.SURF_descr_IN = this.SURF_file_IN.getDescr();
			
			if (!rawFile) { // If it's not a raw file, then doesn't need to add an header (it already has one)
				this.SURF_descr_OUT = new EMDDFileDescr();
				this.SURF_descr_OUT.rate = SURF_descr_IN.rate;
				this.SURF_descr_OUT.bitsPerSample = SURF_descr_IN.bitsPerSample;
				this.SURF_descr_OUT.channels = SURF_descr_IN.channels;
			} else {
				SURF_descr_OUT = descr;
			}
			
			this.SURF_descr_OUT.type = fileTypeToConvert; // File type to convert, received in the constructor			
	   		this.SURF_descr_OUT.file = this.file_OUT;
	   		
	   		this.SURF_file_OUT = EMDDFile.openAsWrite(this.SURF_descr_OUT);
	   		
	   		this.SURF_file_IN.copyFrames(this.SURF_file_OUT, this.SURF_descr_IN.length);
	   		// System.out.println(SURF_descr_IN.length);
			
			this.SURF_file_OUT.cleanUp(); // Closes the random access file stream
			   
		  } catch (IOException e) {
			   e.printStackTrace();
		  } 


		
	}
		
}
