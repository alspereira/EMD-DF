/**
 * Contains tools to update an existing EMD-DF file. 
 */
package emddf.api.update;