package emddf.api.update;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import de.sciss.io.Marker;
import de.sciss.io.Region;
import de.sciss.io.Span;
import emddf.api.create.SURFWriter;
import emddf.api.read.Reader;
import emddf.file.Annotation;
import emddf.file.Config;
import emddf.file.Info;
import emddf.file.MissingData;
import emddf.file.EMDDFile;
import emddf.file.EMDDFileDescr;

/**
 * Used to update an existing EMD-DF file.
 * 
 * @author Manel
 *
 */
public class Update {

	String path, fileName;
	List<Marker> labels, notes;
	List<Region> regions;
	List<Annotation> comments, metadatas;
	List<MissingData> missingdata;

	Info info;
	Config config;
	int fileType;

	private File file_OUT;
	private EMDDFile SURF_file_IN;
	private EMDDFileDescr SURF_descr_IN;
	private EMDDFile SURF_file_OUT;
	private EMDDFileDescr SURF_descr_OUT;

	/**
	 * This is the constructor, that receives the path and the name of the file,
	 * where some updates will be done.
	 * 
	 * @param fileType The filetype of the file to convert.
	 *                 <ul>
	 *                 <strong>
	 *                 <li>0 - WAVE</li>
	 *                 <li>5 - W64</li></strong>
	 *                 </ul>
	 * @param path     The path to the file
	 * @param fileName The name of the file to update
	 */
	public Update(int fileType, String path, String fileName) {
		this.fileType = fileType;
		this.path = path;
		this.fileName = fileName;
		// Initializes the arraylists
		labels = new ArrayList<Marker>();
		notes = new ArrayList<Marker>();
		regions = new ArrayList<Region>();
		comments = new ArrayList<Annotation>();
		metadatas = new ArrayList<Annotation>();
		info = new Info();

		missingdata = new ArrayList<MissingData>();
	}

	/**
	 * This method is used to check if a note already exists in a specific
	 * position/sample.
	 * 
	 * @param pos Position to check
	 * @return <code> true </code> if the position exists
	 */
	public boolean noteAlreadyExists(long pos) {
		// Checks if there's a note with the position sent as an argument
		for (Marker n : notes) {
			if (n.pos == pos)
				return true;
		}
		return false;
	}

	/**
	 * This method is used to verify if one label already exists in a specific
	 * position with the same appliance id.
	 * 
	 * @param pos  Position to check if label exists
	 * @param name The name of the label to check
	 * @return <code> true </code> if the label with that pos and that app_id
	 *         already exists (boolean)
	 */
	public boolean labelAlreadyExists(long pos, String name) {

		// Firstly, before search, there's no samples with that app_id and that pos
		boolean contains = false;

		// The argument 'name' is a String composed by a JSON text.
		// It means that it's necessary to split the JSON, to get the app_id.
		JSONObject json = new JSONObject();
		try {
			json = (JSONObject) parseJson(name);
		} catch (Exception e) {
			e.printStackTrace();
		}

		int app_id = Integer.parseInt(json.get("App_ID").toString()); // Id of the appliance (parseInt converts the
																		// string to a int)
		int label_app_id = 0;

		// For each label of the file (in the arraylist labels)
		for (Marker l : labels) {
			try {
				JSONObject jlabel = (JSONObject) parseJson(l.name);

				// System.out.println("henlo"+jlabel);
				label_app_id = Integer.parseInt(jlabel.get("App_ID").toString()); // Id of the appliance of the current
																					// label
			} catch (Exception e) {
				// e.printStackTrace();
			}
			// Verifies if the position of the current labels is the same as the position
			// where this label will be added
			// Also checks if the appliance id is the same as the id given
			// Because only one label with the same position and the same appliance id is
			// allowed
			if (l.pos == pos && label_app_id == app_id) {
				contains = true;
			}

		}

		return contains;

	}

	/**
	 * This method creates a json file with the string given. This json file is used
	 * to check all the parameters and check if the json is valid.
	 * 
	 * @return temp.json? TODO check this
	 * @throws IOException
	 */
	public String createJSONFile(String name) {

		String jsonFileName = "src/emddf/api/update/temp.json";

		try {
			File file = new File(jsonFileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(name);
			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "/temp.json"; // Must start with /. Otherwise, won't check the file

	}

	/**
	 * This method verifies if the format of the JSON text is right and if its
	 * fields are correct and not empty.
	 * 
	 * @return true if the JSON format is correct, and every field of it is not
	 *         empty and its type is correct.
	 */
	public boolean correctJSONFormat(String aName, String aSchema) throws IOException, ProcessingException {

		String jsonFileName = ""; // Name of the json file, where the json string will be checked

		jsonFileName = createJSONFile(aName); // Creates the json file with the json string gave as a param

		final JsonNode fstabSchema = Utils.loadResource(aSchema); // Opens the json schema (how the json should be, like
																	// order, required parameters, minimum, type of each
																	// parameter, etc.)
		final JsonNode jsonFile = Utils.loadResource(jsonFileName); // Opens the json file created previously

		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

		final JsonSchema schema = factory.getJsonSchema(fstabSchema); // Json Schema

		ProcessingReport report;

		report = schema.validate(jsonFile); // Validates the jsonFile with the schema given
		//System.out.println(report); // Prints the result of the verification (success
		// or fail -> if fails, why)

		// Returns true if the result of the verification is true. It is, if the json
		// file conforms to the schema.
		return report.isSuccess();

	}

	/**
	 * This method is used to convert the format yyyy-mm-dd HH:mm:ss.[0-9]{1,12} to
	 * yyyy-MM-dd'T'HH:mm:ss.[0-9]{1,12}Z Basically it adds the letter 'T' to
	 * separate the date and the time, and the letter 'Z' in the end of the date.
	 * Used by json validator to check if the date is a valid one.
	 */
	public String setDateInCorrectFormat(String date) {

		// Example of a valid date: 2011-10-21 10:18:04.040

		if (date.charAt(10) != 'T') { // If there's no 'T' separating the date and the time
			date = date.substring(0, 10) + "T" + date.substring(11); // Then adds the 'T'
		}

		if (date.length() < 24) { // If there's no 24 letters, then it means that there's no letter 'Z' in the end
									// of the date
			date = date.concat("Z"); // Adds the 'Z' in the end of the date
		}

		return date;

	}

	/**
	 * This method adds a new label (giving its position and its name - in JSON
	 * format) if the label doesn't exit yet (there's no labels with that position
	 * and that app_id - both). Also, the JSON format needs to be correct.
	 * Otherwise, the label won't be added.
	 * 
	 * @param pos
	 * @param name
	 */
	public void addLabel(long pos, String name) throws IOException, ProcessingException {

		Reader r = new Reader(getFile());
		int labelsSize = 0;
		Marker m;

		// LABELS
		try {
			labelsSize = r.getLabels().size();
		} catch (NullPointerException e) {

		}
		// This is not in the correct place
		for (int i = 0; i < labelsSize; i++) {
			m = r.getLabels().get(i);
			labels.add(new Marker(m.pos, m.name));
		}

		JSONObject json = new JSONObject();
		try {
			json = (JSONObject) parseJson(name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error parsing json!");
			e.printStackTrace();
		}
		String date = json.get("Timestamp").toString();

		// Verifies if the date in json has the format yyyy-MM-dd'T'HH:mm:ssZ.
		// It is, appends T and Z letters, if doesn't have. Otherwise, nothing happens.
		int posDate = name.indexOf(date); // The position of the date in the json (before add 'T' and 'Z'

		// Gets the day and the month of the date given
		String[] sp = date.split("-");
		int month = Integer.parseInt(sp[1]);

		sp = sp[2].split(" ");
		int day = Integer.parseInt(sp[0]);

		// Verifies if the day and the month of the date have 2 digits. If don't have,
		// it prepends a 0.
		// It's necessary because the json validator fails if there's just one digit (3
		// has to be 03, for example)
		// To know the number of digits of the integer, it is necessary to convert to
		// string
		if (String.valueOf(day).length() < 2) {
			day = Integer.parseInt("0" + day);
		}
		if (String.valueOf(month).length() < 2) {
			month = Integer.parseInt("0" + month);
		}

		date = date.substring(0, 5) + month + date.substring(4, 5) + day + date.substring(10);

		// It is necessary to add these letters to the json date because json validator
		// only returns that it's a date if the date has the format showed previously.
		date = setDateInCorrectFormat(date); // Send's the date, and returns the date with a 'T' and a 'Z' if doesn't
												// have in the right position

		// Creates the new json with the new date
		name = (name.substring(0, posDate).concat(date)).concat(name.substring(posDate + date.length() - 1));

		if (correctJSONFormat(name, "/fstab.json")) { // Verifies if the json text conforms to the json schem

			if (!labelAlreadyExists(pos, name)) { // Checks if the label doesn't exit yet, in that position and with
													// that appliance id.

				labels.add(new Marker(pos, name)); // If both correct, adds the label to the file.

			} else {

				System.out.println("A label with that position and applicance id, already exists."); // Error

			}

		} else {

			System.out.println("JSON Format is not correct."); // Error

		}

	}

	/**
	 * This method allows to add a new note, giving its position and its name. If
	 * already exists one note in the position you choosed, your note is appended to
	 * the existence one.
	 * 
	 * @param pos  Position to add the note
	 * @param name The content of the note
	 */
	public void addNote(long pos, String name) {

		Reader r = new Reader(getFile());
		int notesSize = 0;
		Marker m;

		// NOTES
		try {
			notesSize = r.getNotes().size();
		} catch (NullPointerException e) {

		}

		for (int i = 0; i < notesSize; i++) {
			m = r.getNotes().get(i);
			notes.add(new Marker(m.pos, m.name));
		}

		if (noteAlreadyExists(pos)) { // If one note in that position already exists, it appends.
			long notePos = notes.get(0).pos; // Gets the position of that note that already exists
			String noteName = notes.get(0).name; // Gets its name
			notes.remove(0); // Removes it
			notes.add(new Marker(notePos, noteName + "\n" + name)); // Adds a new marker with that position, and the
																	// name is appended
		} else {
			// If there's no notes in the position choosen, it creates a new one.
			notes.add(new Marker(pos, name));
		}

	}

	/**
	 * This method is used to change one note that already exists, instead of adding
	 * information to it. For example, if you want to remove some text of the note,
	 * this method is useful for that, once it changes all the information of the
	 * note with the giving parameter.
	 * 
	 * @param pos  The position of the note you want to change
	 * @param name The content of the new note
	 */

	public void updateNote(long pos, String name) {
		boolean removedNote = false;
		int index = 0;
		for (Marker n : notes) {
			if (n.pos == pos) {
				notes.remove(index);
				return; // Stops the for, when removes the note
			}
			index++;
		}
		// Once the name and the position of a note were been declared as "final", they
		// can't be changed (they are constants).
		// Because of this, to change the name of the note, it's necessary to remove the
		// existence one and creates another one, with the same position and different
		// name
		// To remove the note, we have to know its position/index
		if (removedNote) {
			notes.add(new Marker(pos, name));
		} else {
			System.out.println("There's no notes in that position.");
		}
	}

	/**
	 * This method is used to add a new region to an audio file.
	 * 
	 * @param span
	 * @param name
	 */
	public void addRegion(long start, long end, String name) {
		this.addRegion(new Span(start, end), name);
	}

	/**
	 * This method is used to add a new region to an audio file.
	 * 
	 * @param span
	 * @param name
	 */
	private void addRegion(Span span, String name) {

		Reader r = new Reader(getFile());
		int regionsSize = 0;
		Region g;
		boolean alreadyExists = false;

		// REGIONS
		try {
			regionsSize = r.getRegions().size();
		} catch (NullPointerException e) {

		}

		for (int i = 0; i < regionsSize; i++) {
			g = r.getRegions().get(i);
			if (g.span.start == span.start && g.span.stop == span.stop) {
				alreadyExists = true;
			}
			regions.add(new Region(g.span, g.name));
		}

		if (alreadyExists) {
			System.out.println("Already exists one region with that start and end positions.");
		} else {
			regions.add(new Region(span, name)); // Note added successfully (to the Arraylist)
		}

	}

	/**
	 * This method is used to add a new comment to an audio file.
	 * 
	 * @param content The content of the comment
	 */
	public void addComment(String content) {

		Reader r = new Reader(getFile());
		int commentsSize = 0;
		Annotation a;

		// COMMENTS
		try {
			commentsSize = r.getComments().size();
		} catch (NullPointerException e) {

		}

		for (int i = 0; i < commentsSize; i++) {
			a = r.getComments().get(i);
			comments.add(new Annotation(a.content));
		}

		comments.add(new Annotation(content));

	}

	/**
	 * This method is used to add a new metadata to an audio file.
	 * 
	 * @param content Content of the metadata
	 */
	public void addMetadata(String content) {

		Reader r = new Reader(getFile());
		int metadatasSize = 0;
		Annotation a;

		// METADATAS
		try {
			metadatasSize = r.getMetadata().size();
		} catch (NullPointerException e) {

		}

		for (int i = 0; i < metadatasSize; i++) {
			a = r.getMetadata().get(i);
			metadatas.add(new Annotation(a.content));
		}

		metadatas.add(new Annotation(content));

	}

	public void addMissingData(String aInitialTimestamp, long aInitialSample) {
		// Cria o próprio json, logo não necessita de validar o schema

		if (aInitialTimestamp != "" && aInitialSample != 0) {

			// fillMissingDataList();

			missingdata.add(new MissingData(aInitialTimestamp, aInitialSample));
		}

	}

	// This method is used to add a new missing data region.
	public void addMissingData(String aContent) throws IOException, ProcessingException {

		if (correctJSONFormat(aContent, "/schemaMD.json")) {
			// Verificar se formato está correto de acordo com o schema
			fillMissingDataList();
			missingdata.add(new MissingData(aContent));
		}

	}

	public void fillMissingDataList() {

		Reader r = new Reader(getFile());
		int missingDataSize = 0;
		MissingData m;

		try {
			missingDataSize = r.getMissingData().size();
		} catch (NullPointerException e) {

		}

		for (int i = 0; i < missingDataSize; i++) {
			m = r.getMissingData().get(i);
			missingdata.add(new MissingData(m.getInitialTimestamp(), m.getInitialSample()));
		}

	}

	/**
	 * This method is used to change the config chunk of the audio file. We have to
	 * specify surf_initial_timestamp, surf_timezone, surf_sampling_rate,
	 * surf_channel_calibration, num_channels, sample_size and sampling_rate.
	 */
	public void setConfig(String surf_initial_timestamp, String surf_timezone, float surf_sampling_rate,
			float[] surf_channel_callibration) {

		Reader r = new Reader(getFile());

		// INFO
		if (r.getConfig() != null) {
			config = r.getConfig();
			System.out.println("This is already a EMDDF File");

			// config = new Config(r.getConfig().surf_initial_timestamp,
			// r.getConfig().surf_timezone, r.getConfig().surf_sampling_rate,
			// r.getConfig().surf_channel_callibration, r.getConfig().num_channels,
			// r.getConfig().sampling_rate, r.getConfig().sample_size);
		} else {
			// this is a raw audio file without the config chunk. Must add from scratch
			System.out.println("This is NOT YET a EMDDF File");
			config = new Config();
		}

		if (surf_initial_timestamp != "") {
			config.surf_initial_timestamp = surf_initial_timestamp;
		}

		if (surf_timezone != "") {
			config.surf_timezone = surf_timezone;
		}

		if (surf_sampling_rate != 0) {
			config.surf_sampling_rate = surf_sampling_rate;
		}

		if (surf_channel_callibration != null) {
			config.surf_channel_callibration = surf_channel_callibration;
		}

	}

	/**
	 * This method is used to change the config chunk of the audio file. We have to
	 * specify surf_initial_timestamp, surf_timezone, surf_sampling_rate,
	 * surf_channel_calibration, num_channels, sample_size and sampling_rate.
	 */
	/*
	 * public void setConfig(String surf_initial_timestamp, String surf_timezone,
	 * float surf_sampling_rate, float[] surf_channel_callibration, int
	 * num_channels, int sample_size, float sampling_rate) {
	 * 
	 * Reader r = new Reader(getFile());
	 * 
	 * System.out.println("Done reading the file");
	 * 
	 * // INFO if (r.getConfig() != null) { config = r.getConfig();
	 * System.out.println("This is already a EMDDF File");
	 * 
	 * //config = new Config(r.getConfig().surf_initial_timestamp,
	 * r.getConfig().surf_timezone, r.getConfig().surf_sampling_rate,
	 * r.getConfig().surf_channel_callibration, r.getConfig().num_channels,
	 * r.getConfig().sampling_rate, r.getConfig().sample_size); } else { // this is
	 * a raw audio file without the config chunk. Must add from scratch
	 * System.out.println("This is NOT YET a EMDDF File"); config = new Config(); }
	 * 
	 * 
	 * if (surf_initial_timestamp != "") { config.surf_initial_timestamp =
	 * surf_initial_timestamp; }
	 * 
	 * if (surf_timezone != "") { config.surf_timezone = surf_timezone; }
	 * 
	 * if (surf_sampling_rate != 0) { config.surf_sampling_rate =
	 * surf_sampling_rate; }
	 * 
	 * if (surf_channel_callibration != null) { config.surf_channel_callibration =
	 * surf_channel_callibration; }
	 * 
	 * if (num_channels != 0) { config.num_channels = num_channels; }
	 * 
	 * if (sample_size != 0) { config.sample_size = sample_size; }
	 * 
	 * if (sampling_rate != 0) { config.sampling_rate = sampling_rate; }
	 * 
	 * }
	 */

	/**
	 * This method is used to change the info of the audio file. In the arguments,
	 * you can specify the info you want to change. If you want to change that value
	 * (ex. file_creator) you put in the respective param (in this case, first
	 * param), the value you want. If you don't want to change the value, you just
	 * need to put "".
	 * 
	 * @param file_creator  The creator of the file
	 * @param commissioner  The comissioner
	 * @param comments      The comments
	 * @param copyright     The copyright
	 * @param creation_date THe creation data
	 * @param keywords      The keywords of the dataset
	 * @param name          The name of the dataset
	 * @param product       The product
	 * @param subject       The subject
	 * @param software      The software
	 * @param source        The source
	 * @param source_form   The source form
	 */
	public void setInfo(String archival_location, String file_creator, String commissioner, String comments,
			String copyright, String creation_date, String keywords, String name, String product, String subject,
			String software, String source, String source_form) {

		Reader r = new Reader(getFile());

		// INFO
		if (r.getInfo() != null) {
			info.archival_location = r.getInfo().archival_location;
			info.file_creator = r.getInfo().file_creator;
			info.commissioner = r.getInfo().commissioner;
			info.comments = r.getInfo().comments;
			info.copyright = r.getInfo().copyright;
			info.creation_date = r.getInfo().creation_date;
			info.keywords = r.getInfo().keywords;
			info.name = r.getInfo().name;
			info.product = r.getInfo().product;
			info.subject = r.getInfo().subject;
			info.software = r.getInfo().software;
			info.source = r.getInfo().source;
			info.source_form = r.getInfo().source_form;
		}

		if (archival_location != "") {
			info.archival_location = archival_location;
		}
		if (file_creator != "") {
			info.file_creator = file_creator;
		}
		if (commissioner != "") {
			info.commissioner = commissioner;
		}
		if (comments != "") {
			info.comments = comments;
		}
		if (copyright != "") {
			info.copyright = copyright;
		}
		if (creation_date != "") {
			info.creation_date = creation_date;
		}
		if (keywords != "") {
			info.keywords = keywords;
		}
		if (name != "") {
			info.name = name;
		}
		if (product != "") {
			info.product = product;
		}
		if (subject != "") {
			info.subject = subject;
		}
		if (software != "") {
			info.software = software;
		}
		if (source != "") {
			info.source = source;
		}
		if (source_form != "") {
			info.source_form = source_form;
		}

	}

	/**
	 * This method returns the extension of the file given in the instance.
	 * 
	 * @return A string containing either <code> w64 </code> or <code> wav </code>
	 */
	public String getExtension() {
		String extension;
		// Reads the labels of the file (Reads all file)
		if (fileType == EMDDFileDescr.TYPE_WAVE64) {
			extension = "w64";
		} else {
			extension = "wav";
		}
		return extension;
	}

	/**
	 * This method returns the file with the path and fileName given as a parameter
	 * 
	 * @return A file object
	 */
	public File getFile() {
		File f;
		// Reads the labels of the file (Reads all file)
		if (fileType == EMDDFileDescr.TYPE_WAVE64) {
			f = new File(path + "" + fileName + ".w64");
		} else {
			f = new File(path + "" + fileName + ".wav");
		}
		return f;
	}

	/**
	 * This method is used to save all the updated information (labels, comments,
	 * regions, info, metadata, ...) of the audio file.
	 * 
	 * @throws IOException
	 * @throws ProcessingException
	 */
	public void saveChanges() {

		try {
			this.file_OUT = new File(path + fileName + "-update" + "." + getExtension()); // The extension of the file
																							// type in which the file
																							// will be converted

			this.SURF_file_IN = EMDDFile.openAsRead(getFile());

			this.SURF_file_IN.readMarkers();

			this.SURF_descr_IN = this.SURF_file_IN.getDescr();

			this.SURF_descr_OUT = new EMDDFileDescr();
			this.SURF_descr_OUT.rate = SURF_descr_IN.rate;
			this.SURF_descr_OUT.bitsPerSample = SURF_descr_IN.bitsPerSample;
			this.SURF_descr_OUT.channels = SURF_descr_IN.channels;
			this.SURF_descr_OUT.type = SURF_descr_IN.type;
			this.SURF_descr_OUT.file = this.file_OUT;

			this.SURF_descr_OUT.setProperty("labels", labels);
			this.SURF_descr_OUT.setProperty("notes", notes);
			this.SURF_descr_OUT.setProperty("comment", comments);
			this.SURF_descr_OUT.setProperty("metadata", metadatas);
			this.SURF_descr_OUT.setProperty("regions", regions);
			this.SURF_descr_OUT.setProperty("info", info);
			this.SURF_descr_OUT.setProperty("config", config);

			System.out.println("teste");
			for (int i = 0; i < missingdata.size(); i++) {
				System.out.println(missingdata.get(i));
			}
			this.SURF_descr_OUT.setProperty("missingdata", missingdata);

			this.SURF_file_OUT = EMDDFile.openAsWrite(this.SURF_descr_OUT);

			this.SURF_file_IN.copyFrames(this.SURF_file_OUT, this.SURF_descr_IN.length);

			this.SURF_file_OUT.cleanUp();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private Object parseJson(String json) throws Exception {
		JSONParser jsonParser = new JSONParser();
		return jsonParser.parse(json);
	}

}
