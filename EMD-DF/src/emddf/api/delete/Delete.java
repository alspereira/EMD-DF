package emddf.api.delete;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import de.sciss.io.Marker;
import de.sciss.io.Region;
import de.sciss.io.Span;
import emddf.api.create.SURFWriter;
import emddf.api.read.Reader;
import emddf.file.Annotation;
import emddf.file.Info;
import emddf.file.EMDDFile;
import emddf.file.EMDDFileDescr;


public class Delete {
	
	String path, fileName;
	List<Marker> labels, notes;
	List<Region> regions;
	List<Annotation> comments, metadatas;

	Info info;
	int fileType;
	
	private File file_OUT;
	private EMDDFile SURF_file_IN;
	private EMDDFileDescr SURF_descr_IN; 
	private EMDDFile SURF_file_OUT; 
	private EMDDFileDescr SURF_descr_OUT;
	
	/**
	 *  This is the constructor, that receives the path and the name of the file, where some deletions will be done.
	 * @param fileType The  filetype of the file to convert.<ul><strong><li>0 - WAVE</li> <li>5 - W64</li></strong></ul>
	 * @param path The path of the file
	 * @param fileName The name of the file
	 */
	public Delete(int fileType, String path, String fileName) {
		this.fileType = fileType;
		this.path = path;
		this.fileName = fileName;
		// Initializes the arraylists
		labels = new ArrayList<Marker>();
		notes = new ArrayList<Marker>();
		regions = new ArrayList<Region>();
		comments = new ArrayList<Annotation>();
		metadatas = new ArrayList<Annotation>();
		info = new Info();
	}
	
	
	/**
	 * Removes the info of the audio file.
	 */
	public void removeInfo() {
				
		info = null;
		
	} 
	
	
	
	/**
	 * Deletes the label at the given position.
	 * @param pos Position of the label to remove
	 */
	public void deleteLabel(long pos) {
		
		Reader r = new Reader(getFile());
		int labelsSize = 0;
		Marker m;
		
		
		try {
			labelsSize = r.getLabels().size();
		} catch (NullPointerException e) {
			
		}

		for (int i=0; i<labelsSize; i++) {
			m = r.getLabels().get(i);
			labels.add(new Marker(m.pos, m.name));
		}
		
		
		for (int i=0; i<labels.size(); i++) {
			if (labels.get(i).pos == pos) {
				labels.remove(i);
			}
		}
	}
	
	
	
	/**
	 * Deletes the label with a specific name and a specific position.
	 * @param pos Position of the label to remove
	 * @param name Name of the label to remove
	 * @implNote This method exists in case there are two labels in the same position
	 */
	public void deleteLabel(long pos, String name) {
		
		Reader r = new Reader(getFile());
		int labelsSize = 0;
		Marker m;
		
		
		try {
			labelsSize = r.getLabels().size();
		} catch (NullPointerException e) {
			
		}

		for (int i=0; i<labelsSize; i++) {
			m = r.getLabels().get(i);
			labels.add(new Marker(m.pos, m.name));
		}
		
		labels.remove(new Marker(pos, name));
		
	}
	
	
	
	/**
	 * Deletes the note at the position given as a parameter.
	 * @param pos Position of the note to remove.
	 */
	public void deleteNote(long pos) {
		
		Reader r = new Reader(getFile());
		int notesSize = 0;
		Marker m;
		

		// NOTES
		try {
			notesSize = r.getNotes().size();
		} catch (NullPointerException e) {
			
		}

		for (int i=0; i<notesSize; i++) {
			m = r.getNotes().get(i);
			notes.add(new Marker(m.pos, m.name));
		}
		
		
		
		for (int i=0; i<notes.size(); i++) {
			if (notes.get(i).pos == pos) {
				notes.remove(i);
			}
		}
	}
	
	
	
	/**
	 * Deletes the comment which the content is given as a parameter.
	 * @param content The content to be removed
	 */
	public void deleteComment(String content) {
		
		Reader r = new Reader(getFile());
		int commentsSize = 0;
		Annotation a;
		

		// COMMENTS
		try {
			commentsSize = r.getComments().size();
		} catch (NullPointerException e) {
			
		}

		for (int i=0; i<commentsSize; i++) {
			a = r.getComments().get(i);
			comments.add(new Annotation(a.content));
		}

		
		comments.remove(new Annotation(content));
	}
	
	
	
	/**
	 * Deletes the comment giving its id (starting at 0).
	 * @param id The ID of the comment to remove.
	 */
	public void deleteComment(int id) {
		
		Reader r = new Reader(getFile());
		int commentsSize = 0;
		Annotation a;
		

		// COMMENTS
		try {
			commentsSize = r.getComments().size();
		} catch (NullPointerException e) {
			
		}

		for (int i=0; i<commentsSize; i++) {
			a = r.getComments().get(i);
			comments.add(new Annotation(a.content));
		}
		
		comments.remove(id);
	}
	
	
	
	/**
	 * Deletes the metadata with the content given as a parameter.
	 * @param content The content of the metadata that should be removed
	 */
	public void deleteMetadata(String content) {
		
		Reader r = new Reader(getFile());
		int metadatasSize = 0;
		Annotation a;
		
		// METADATAS
		try {
			metadatasSize = r.getMetadata().size();
		} catch (NullPointerException e) {
			
		}

		for (int i=0; i<metadatasSize; i++) {
			a = r.getMetadata().get(i);
			metadatas.add(new Annotation(a.content));
		}				
				
		metadatas.remove(new Annotation(content));
	}
	
	
	
	/**
	 * Deletes the metadata giving its id (starting at 0).
 	 * @param id The ID of the metadata to remove.

	 */
	public void deleteMetadata(int id) {

		Reader r = new Reader(getFile());
		int metadatasSize = 0;
		Annotation a;
		
		// METADATAS
		try {
			metadatasSize = r.getMetadata().size();
		} catch (NullPointerException e) {
			
		}

		for (int i=0; i<metadatasSize; i++) {
			a = r.getMetadata().get(i);
			metadatas.add(new Annotation(a.content));
		}			
		
		metadatas.remove(id);
	}
	
	
	
	/**
	 * Deletes the region in a specific span.
	 */
	public void deleteRegion(Span span) {
		
		Reader r = new Reader(getFile());
		int regionsSize = 0;
		Region g;
		
		// REGIONS
		try {
			regionsSize = r.getRegions().size();
		} catch (NullPointerException e) {
			
		}
		// System.out.println("Num Regions: " + regionsSize);

		for (int i=0; i<regionsSize; i++) {
			g = r.getRegions().get(i);
			regions.add(new Region(g.span, g.name));
		}

		
		
		for (int i=0; i<regions.size(); i++) {
			if (regions.get(i).span == span) { // Two spans are equal if they have the same start and the same end.
				regions.remove(i);
			}
		}
	}
	
	
	/**
	 * This method returns the extension of the file given as a parameter.
	 * @return A string with the file type either <code>w64</code> or <code>wav</code>
	 */
	public String getExtension() {
		String extension;
		// Reads the labels of the file (Reads all file)
		if (fileType == EMDDFileDescr.TYPE_WAVE64) {
			extension = "w64";
		} else {
			extension = "wav";
		}
		return extension;
	}
	
	
	/**
	 * This method returns the file with the path and fileName given as a parameter in the constructor
	 * @return The file object  
	 * 
	 */
	public File getFile() {
		File f;
		// Reads the labels of the file (Reads all file)
		if (fileType == EMDDFileDescr.TYPE_WAVE64) {
			f = new File(path + "" + fileName + ".w64");
		} else {
			f = new File(path + "" + fileName + ".wav");
		}
		return f;
	}
	
	
	/**
	 * This method is used to save all the deleted information (labels, comments, regions, info, metadata, ...) of the audio file.
	 * @throws IOException
	 * @throws ProcessingException
	 */
	public void saveChanges() {
		
		try {
			
			
			this.file_OUT = new File(path + fileName + "-update" + "." + getExtension()); // The extension of the file type in which the file will be converted
			
			this.SURF_file_IN = EMDDFile.openAsRead(getFile());
			
			this.SURF_file_IN.readMarkers();
			   
			this.SURF_descr_IN = this.SURF_file_IN.getDescr();
			
			this.SURF_descr_OUT = new EMDDFileDescr();
			this.SURF_descr_OUT.rate = SURF_descr_IN.rate;
			this.SURF_descr_OUT.bitsPerSample = SURF_descr_IN.bitsPerSample;
			this.SURF_descr_OUT.channels = SURF_descr_IN.channels;
			this.SURF_descr_OUT.type = SURF_descr_IN.type;		
	   		this.SURF_descr_OUT.file = this.file_OUT;
	   		
			this.SURF_descr_OUT.setProperty("labels", labels);
			this.SURF_descr_OUT.setProperty("notes", notes);
			this.SURF_descr_OUT.setProperty("comment", comments);
	   		this.SURF_descr_OUT.setProperty("metadata", metadatas);
	   		this.SURF_descr_OUT.setProperty("regions", regions);
	   		this.SURF_descr_OUT.setProperty("info", info);

	   		this.SURF_file_OUT = EMDDFile.openAsWrite(this.SURF_descr_OUT);
	   		
	   		this.SURF_file_IN.copyFrames(this.SURF_file_OUT, this.SURF_descr_IN.length);
			
			this.SURF_file_OUT.cleanUp();
			   
		  } catch (IOException e) {
			   e.printStackTrace();
		  } 
		
		
	}
	
	
}
