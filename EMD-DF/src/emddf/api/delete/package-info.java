/**
 * Contains tools to delete information from an EMD-DF file
 */
package emddf.api.delete;