package emddf.api.bridge;

import emddf.api.update.Update;
import emddf.file.Annotation;
import emddf.file.Info;
import emddf.file.MissingData;

import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.util.Iterator;

import de.sciss.io.Marker;
import de.sciss.io.Region;
import de.sciss.io.Span;
import emddf.api.read.Reader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
public class Integrator {
	static boolean devMode = false;
	final String[] metadata_types = {"labels","comments","metadata", "notes","region","missing_data","sample_to_timestamp"};
	public static void main(String[] args) {
		if(args.length == 0 ) {
			System.out.println("No arguments supplied");
			return;
		}
		String operation_mode;
		if (!devMode)
			operation_mode = args[0];
		else
			operation_mode = "read";
		if(operation_mode.compareTo("update") == 0) {
			String path;
			String name;
			int filetype;
			String label_filename;
			if(!devMode) {
				path = args[1];
				name = args[2];
				filetype = Integer.parseInt(args[3]);
				label_filename = args[4];
			}else {
				path = "F:/";
				name = "in";
				filetype = 0;
				label_filename = "F:/test.json";
			}
			update(path,name,filetype,label_filename);
			
		}else if (operation_mode.compareTo("read") == 0) {
			String filepath;
			String propertyToRead;
			
			if(!devMode) {
				filepath = args[1];
				propertyToRead = args[2];
			}else {
				filepath ="F:/in-update.wav";
				propertyToRead = "all";
			}
			
			read(filepath, propertyToRead,args);
		}				
	}
	
	private static void update(String path, String name, int filetype, String metadata_filename) {
		Update u = new Update(filetype, path, name);
		
		try {
			JSONObject metadata = (JSONObject) readJson(metadata_filename);
			//metadata is the full text from the metadata file
			Iterator<Object> it = metadata.keySet().iterator();
			//it then will be iterated 
			Object cur_key;
			while (it.hasNext()) {
				cur_key = it.next();	
				JSONArray full_json = (JSONArray) metadata.get(cur_key);
				Iterator<Object> level_1 = full_json.iterator();
				while(level_1.hasNext()) {
 
					JSONObject lvl_1 = (JSONObject) level_1.next();
					if(cur_key.equals("labels")) {
	
						long position = Long.parseLong(lvl_1.get("Position").toString());
						u.addLabel(position,lvl_1.toString());

					}else if(cur_key.equals("comments")) {
						
						u.addComment(lvl_1.toString());
						
					}else if(cur_key.equals("metadata")) {
						
						u.addMetadata(lvl_1.toString());
						
					}else if(cur_key.equals("notes")) {
						
						long position = Long.parseLong(lvl_1.get("Position").toString());
						
						u.addNote(position, lvl_1.toString());
						
					}else if(cur_key.equals("region")) {
						
						long start = Long.parseLong(lvl_1.get("Start").toString());
						long stop = Long.parseLong(lvl_1.get("Stop").toString());

						Span a = new Span(start,stop);
						//u.addRegion(a, lvl_1.toString());
						u.addRegion(start, stop, lvl_1.toString());
						
					}else if(cur_key.equals("info")) {
						
						String archival_location = lvl_1.get("archival_location").toString();
						String file_creator = lvl_1.get("file_creator").toString();
						String commissioner = lvl_1.get("comissioner").toString();
						String comments = lvl_1.get("comments").toString();
						String copyright = lvl_1.get("copyright").toString();
						String creation_date = lvl_1.get("creation_date").toString();
						String keywords = lvl_1.get("keywords").toString();
						String filename = "";
						String product = lvl_1.get("product").toString();
						String subject = lvl_1.get("subject").toString();
						String software = lvl_1.get("software").toString();
						String source = lvl_1.get("source").toString();
						String source_form = lvl_1.get("source_form").toString();
						
						// LP: este metadata_file name vem de onde?
						u.setInfo(archival_location, file_creator, commissioner, comments, copyright, creation_date, keywords, metadata_filename, product, subject, software, source, source_form);
					}
					
				}
			}
			System.out.println("{success:True}");
		} catch (Exception e) {
			//e.printStackTrace();
			System.err.println("Error trying to parse the JSON file!");
			System.exit(1);
		}
		u.saveChanges();
		
	}

	private static void read(String path, String propertyToRead, String[] args) {
		File f = new File(path);
		Reader r = new Reader(f);
		try {
			if(propertyToRead.equals("labels")){
				for (Marker l : r.getLabels()) {
					System.out.println(l.name);
				}
			}else if(propertyToRead.equals("comments")){
				for (Annotation l : r.getComments()) {
					System.out.println(l.content);
				}
			}else if(propertyToRead.equals("regions")){
				for (Region l : r.getRegions()) {
					System.out.println(l.name);
				}
			}else if(propertyToRead.equals("notes")){
				for (Marker l : r.getNotes()) {
					System.out.println(l.name);
				}
			}else if(propertyToRead.equals("metadata")){
				for (Annotation l : r.getMetadata()) {
					System.out.println(l.content);
				}
			}else if(propertyToRead.equals("info")){
				Info a = r.getInfo();
				System.out.println(a.toString());
			}else if(propertyToRead.equals("all")){
				read(path,"labels",args);
				read(path,"comments",args);
				read(path,"regions",args);
				read(path,"notes",args);
				read(path,"metadata",args);
				read(path,"info",args);
			}else if(propertyToRead.equals("missing_data")){
				for(MissingData m: r.getMissingData()) {
					System.out.println(m.toJSON());
				}
			}else if(propertyToRead.equals("sample_to_timestamp")){
				try {
					System.out.println("{success:True,result:"+r.getTimestamp(Long.parseLong(args[3]))+"}");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				System.out.println("There is no such property as " + propertyToRead );
			}
		} catch (NullPointerException e) {
			System.out.println("{success:False,error:'No value in "+propertyToRead+"'}");
		}
		
	}
	
	private static JSONObject readJson(String filename) throws Exception {
	    FileReader reader = new FileReader(filename);
	    JSONParser jsonParser = new JSONParser();
	    return (JSONObject) jsonParser.parse(reader);
	}
	private static Object parseJson(String json) throws Exception{
		JSONParser jsonParser = new JSONParser();
		return jsonParser.parse(json);
	}
	

}

