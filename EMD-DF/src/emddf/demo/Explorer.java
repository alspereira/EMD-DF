package emddf.demo;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.text.Keymap;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import de.sciss.io.Marker;
import de.sciss.io.Span;
import emddf.api.create.Converter;
import emddf.api.create.Merger;
import emddf.api.create.SURFWriter;
import emddf.api.delete.Delete;
import emddf.api.read.Chart;
import emddf.api.read.Reader;
import emddf.api.update.Update;
import emddf.file.EMDDFile;
import emddf.file.EMDDFileDescr;
import emddf.file.MissingData;

import java.sql.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import org.json.simple.JSONObject;



public class Explorer {
	public static void main(String[] args) throws IOException, ProcessingException, ParseException {

		Scanner scan = new Scanner(System.in);
		int option;
		
		Chart samplesChart;
		
		// DEMO
		
		do {
		
			System.out.println("---- Menu ----");
			System.out.println("1. Read File");
			System.out.println("2. Write File");
			System.out.println("3. Update File");
			System.out.println("4. Convert File");
			System.out.println("5. Merge Files");
			System.out.println("0. Quit");
			System.out.println("--------------");
	
	        System.out.print("Option: ");
	        option = scan.nextInt();
	                
	        switch (option) {
		        case 1: // read an existing file
		        		        	
		            System.out.print("File Name: ");
		        	String file = scan.next();
	
		        	File f = new File(file);
		        	Reader r = new Reader(f);
		        	
		        	
		        	if (r.getConfig().surf_initial_timestamp != null) {
		        	
			        	r.ReadSync(1000, 0, 20000);
			        	System.out.println("Initial Timestap: " + r.getConfig().surf_initial_timestamp);
			        	
			        	System.out.println("Number of Samples: " + r.getNumSamples());
			    		System.out.println("Number of Frames: " + r.getNumFrames());
			    		System.out.println("Rate: " + r.getRate());
			    		System.out.println("Bits per Sample: " + r.getBitsPerSample());
			    		System.out.println("Type: " + r.getType());
			    		
			    		try {
			    			System.out.println("MD: "+ r.getMissingData().size());
			    			for (int i=0; i<r.getMissingData().size(); i++) {
			    				System.out.println("MD " + (i+1) + ": " + (r.getMissingData().get(i)));
			    			}
			    		} catch (NullPointerException e) {
			    			System.out.println("Num MD: 0");
			    		}
			    		
			    		try {
			    			System.out.println("Num Labels: " + r.getLabels().size());
			    			int num_labels = Math.min(5, r.getLabels().size());
			    			System.out.println("First " +  num_labels + " Labels:");
			    			for (int i=0; i < num_labels; i++) {
			    				System.out.println("Pos: " + r.getLabels().get(i).pos + " , Name: " + r.getLabels().get(i).name);
			    			}
			    		} catch (NullPointerException e) {
			    			System.out.println("Num Labels: 0");
			    		}
			    		
			    		try {
			    			System.out.println("Num Regions: " + r.getRegions().size());
		    				System.out.println("Regions: ");
			    			for (int i=0; i<r.getRegions().size(); i++) {
		    					System.out.println("Name: " + r.getRegions().get(i).name);
		    				}
			    			} catch (NullPointerException e) {
			    				System.out.println("Num Regions: 0");
			    			}
			    		
			    		try {
			    			System.out.println("Num Notes: " + r.getNotes().size());
		
			    			System.out.println("Notes:");
			    			for (int i=0; i<r.getNotes().size(); i++) {
			    				System.out.println("Pos: " + r.getNotes().get(i).pos + " , Name: " + r.getNotes().get(i).name);
			    			}
			    		} catch (NullPointerException e) {
			    			System.out.println("Num Notes: 0");
			    		}
			    		
			    		try {
			    			System.out.println("Num Comments: " + r.getComments().size());
		
			    			System.out.println("Comments:");
			    			for (int i=0; i<r.getComments().size(); i++) {
			    				System.out.println("Content: " + r.getComments().get(i).content);
			    			}
			    		} catch (NullPointerException e) {
			    			System.out.println("Num Comments: 0");
			    		}
			    		
			    		try {
			    			System.out.println("INFO:");
		
			    			System.out.println(r.getInfo().toString());
			    			
			    		} catch (NullPointerException e) {
			    			System.out.println("No Info Chunks");
			    		}
			    		
			    		samplesChart = new Chart(5000, r.getNumChannels(), r);
			    		samplesChart.setPowerSamplesQueue(r.getSamplesList());
			    		
						new Thread(samplesChart).start();
		    		
		        	} else {
		        		System.out.println("Audio File Incomplete: No Config Information.");
	        		}
	        
	        
		        	break;
		        case 2: // Creates a new file from scratch using dummy data
		        	
		        	ArrayBlockingQueue<float[][]> buffer = new ArrayBlockingQueue<float[][]>(2000);
		    		Random rand = new Random();

		    		
		    		float[][] x = new float[1][600];
		    		
		    		for (int i=0; i<600; i++) {
		    			x[0][i] = rand.nextFloat();
		    		}

		    		for (int i=0; i<2000; i++) {
		    			buffer.add(x);
		    		}
		    		
		    		
		    		Marker l1 = new Marker(234234, "LABEL 1");
		    		Marker l2 = new Marker(12345, "LABEL 2");
		    		List<Marker> list = new ArrayList<>();
		    		List<MissingData> md = new ArrayList<>();
		    		
		    		list.add(l1);
		    		list.add(l2);
		    		
		    		md.add(new MissingData("2011-10-21 10:18:04.040", 506));
		    		md.add(new MissingData("2011-10-21 10:18:19.052", 2000));
		    		
		    		System.out.print("Path: ");
		        	String path = scan.next();
		        	
		        	System.out.print("File Name: ");
		        	String fileName = scan.next();
		        	
		        	System.out.print("Sampling Rate: ");
		        	int samplingRate = scan.nextInt();
		        	
		        	System.out.print("Bits per Sample: ");
		        	int bitsPerSample = scan.nextInt();
		        	
		        	System.out.print("File Type (0 - WAVE, 5 - WAVE64): ");
		        	int fileType = scan.nextInt();
		        	
		        	if (fileType == 0 || fileType == 5) {
			    		SURFWriter sw = new SURFWriter(fileType, path, fileName, buffer, samplingRate, bitsPerSample, 1, 1024);
			    		sw.setLabels(list);
			    		sw.setMissingData(md);
			    		sw.start();
		        	} else {
		        		System.out.println("Incorrect File Type");
		        		
		        	}
		    		
		        	break;
		        case 3: // Updates an existing file
		        	
		        	
		        	System.out.print("Path: ");
		            String filePath = scan.next();
		        	  
		        	System.out.print("File Name: ");
		    		String nameOfFile = scan.next();
		    		
		    		System.out.print("File Type (0 - Wave, 5 - Wave64): ");
		    		int type = scan.nextInt();
		    		
		    		if (type == 0 || type == 5) {
		    			
		    		
		    			Update u = new Update(type, filePath, nameOfFile);
		    		
		    		
			        	do {
			        		
				        	System.out.println("---- Menu ----");
				    		System.out.println("1. Add Label");
				    		System.out.println("2. Add Note");
				    		System.out.println("3. Add Region");
				    		System.out.println("4. Add Comment");
				    		System.out.println("5. Add Metadata");
				    		System.out.println("6. Add Missing Data");
				    		System.out.println("0. Quit");
				    		System.out.println("--------------");
		
				    		System.out.print("Option: ");
				            option = scan.nextInt();
				        
				            		       
				    		int pos = 0;
				    		String name = "", content = "";
				    		String initialTimestamp;
				    		int initialSample;
				    		
				            switch (option) {
				            	case 1:
				            		
				            		System.out.print("Pos: ");
						            pos = scan.nextInt();
						            
						            System.out.print("Name: ");
						            name = scan.next();
						            
				            		u.addLabel(pos, name);
				            		
				            		break;
				            	case 2:
				            		
				            		System.out.print("Pos: ");
						            pos = scan.nextInt();
						            
						            System.out.print("Name: ");
						            name = scan.next();
						            
				            		u.addNote(pos, name);
				            		
				            		break;
				            	case 3:
				            		
				            		System.out.print("Start: ");
						            long start = scan.nextInt();
						            
						            System.out.print("End: ");
						            long end = scan.nextInt();
						            
						            System.out.print("Name: ");
						            name = scan.next();
						            
				            		//u.addRegion(new Span(start, end), name);
				            		u.addRegion(start, end, name);
						            break;
				            	case 4:
				            		
				            		System.out.print("Content: ");
						            content = scan.next();
						            
				            		u.addComment(content);
				            		
				            		break;
				            	case 5:
				            		
				            		System.out.print("Content: ");
						            content = scan.next();
						            
				            		u.addMetadata(content);
				            		
				            		break;
				            	case 6:
				            		
				            		System.out.print("Initial Timestamp: ");
						            initialTimestamp = scan.next();
						            scan.next();
						            System.out.print("Initial Sample: ");
						            initialSample = scan.nextInt();
						            
				            		u.addMissingData(initialTimestamp, initialSample);
				            		
				            		break;
				            	default:
				            		
				            		break;
				            }
				            
				            scan.next();
				            
			        	} while (option != 0);
			    		
				        option = 10;
				        
			    		u.addComment("aaa");
			    		u.addMissingData("2011-10-21 10:18:04.040", 503);  
			    		u.addMissingData("2011-10-21 10:18:04.040", 46238);
			    		u.addMissingData("2011-10-21 10:18:04.040", 4822278);
			    		u.addLabel(128459, "{\"SURF_ID\":453,\"App_ID\":55,\"App_Label\":\"Monitor\",\"Position\":128459,\"Timestamp\":\"2012-04-21 12:18:04.040\",\"Delta_P\":\"126.041\",\"Delta_Q\":\"-48.887\",\"Type\":1}");
			    		u.saveChanges();
				        
		    		} else {
		    			
		    			System.out.println("The file type is not correct.");
		    		}

		        	break;
		        case 4: // Convert between formats
		        			        		
		        	System.out.println("---- Menu ----");
		    		System.out.println("1. Wave To Wave64");
		    		System.out.println("2. Wave64 to Wave");
		    		System.out.println("3. Wave to Raw");
		    		System.out.println("4. Wave64 to Raw");
		    		System.out.println("5. Raw to Wave");
		    		System.out.println("6. Raw to Wave64");
		    		System.out.println("--------------");

		    		System.out.print("Option: ");
	
		            option = scan.nextInt();
		            
		            
		            System.out.print("File Name: ");
		        	String name = scan.next();
		            
		        	int typef = 0;
		        	
		            switch (option) {
		            	case 1:	
		            		typef = 5;
		            		break;
		            	case 2:
		            		typef = 0;
		            		break;
		            	case 3:
		            		typef = 4;
		            		break;
		            	case 4:
		            		typef = 4;
		            		break;
		            	case 5:
		            		typef = 0;
		            		break;
		            	case 6:
		            		typef = 5;
		            		break;
		            	default:
		            		break;
		            }
		            
            		Converter conv = new Converter(name, 4);
            		conv.convert();
            		
            		System.out.println("The file was converted successfully!");

			        
		        	break;
		        case 5: // Merge dataset files
		        	
		        	System.out.print("File Name: ");
		        	String mergedFileName = scan.next();
		        	File[] vf = new File[2];
		        	
		        	System.out.print("First File: ");
		        	vf[0] = new File(scan.next());
		        	
		        	System.out.print("Second File: ");
		        	vf[1] = new File(scan.next());
		        	
		    		Merger m = new Merger(vf, mergedFileName);
		    		m.run();
		    		
		        	break;
		        default:
		        	break;
	        }
	        
        
		} while (option != 0);

	
        // Closing Scanner after the use
        scan.close();
	}
}
