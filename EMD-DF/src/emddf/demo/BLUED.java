package emddf.demo;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import emddf.api.update.Update;


public class BLUED {
	
	static Update uA;
	static String path = "assets/";
	static String labels_path = "BLUED_labels.db";
	static SimpleDateFormat dateFormat 	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	// Adds labels that are loaded from a database. Note that since the sampling rate of the file is the original
	// i.e., (60Hz) it is not necessary to adjust the position of the labels.
	public static void addLabels() {
		 Connection c = null;
		 Statement stmt = null;
		 
		 try {
			 Class.forName("org.sqlite.JDBC");
			 c = DriverManager.getConnection("jdbc:sqlite:"+ path + labels_path);
			 c.setAutoCommit(false);

			 stmt = c.createStatement();
			 ResultSet rs = stmt.executeQuery( "SELECT * FROM blued_labels_ALL WHERE Phase = 'A';" );
	  
			 while ( rs.next() ) { 
				 int id 				= rs.getInt("ID");
				 int  app_id 			= rs.getInt("App_ID");
				 String app_label  		= rs.getString("App_Label");
				 int  position 			= rs.getInt("Position");
				 String timestamp 		= rs.getString("Timestamp");
				 float delta_p 			= rs.getFloat("Delta_P");
				 float delta_q 			= rs.getFloat("Delta_Q");
				 int type				= (delta_p > 0) ? 1: -1;
			         
				 String labelName = "{\"ID\":" + id + 
						 ",\"App_ID\":" + app_id + 
						 ",\"App_Label\":\"" + app_label + 
						 "\",\"Position\":" + position + 
						 ",\"Timestamp\":\"" + timestamp + 
						 "\",\"Delta_P\": " + delta_p + 
						 ",\"Delta_Q\":" + delta_q + 
						 ",\"Type\":" + type + 
						 "}";
				 
				 System.out.println(labelName);
				 uA.addLabel(position, labelName);
			 }
			 rs.close();
		     stmt.close();
		     c.close();
		   } catch ( Exception e ) {
			   	System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			   	System.exit(0);
		   }
		   System.out.println("Operation done successfully");
	}
	
	public static void addActivity(){
		
		
	}
	
	// Adds default metadata to the file using selected RIFF chunks
	public static void setInfo() {
		
		String archival_location 	= "http://nilm.cmubi.org";
		String comments 			= "This is the EMD-DF64 version of the BLUED dataset phase A at 60 Hz";
		String commissioner 		= "Mario Berg�s (marioberges@cmu.edu)";
		String copyright 			= "Inherited from the original source";
		String creation_date 		= dateFormat.format( Calendar.getInstance().getTime() );
		String file_creator			= "Lucas Pereira (lucas.pereira@iti.larsys.pt)";
		String keywords 			= "NILM, dataset, event-based";
		String name 				= "EMD-DF64-BLUED: Phase A";
		String product 				= "Event-based NILM performance evaluation";
		String software 			= "EMD-DF64";
		String subject 				= "Phase A: Real and Reactive power at 60 Hz";
		String source 				= "BLUED: Building-Level fUlly labeled Electricity Disaggregation Dataset";
		String source_form 			= "Matlab (.mat)";
		
		uA.setInfo(archival_location, file_creator, commissioner, comments, copyright, 
				creation_date, keywords, name, product, subject, 
				software, source, source_form);
	}
	

	
	public static void main(String[] args) {
	 
	/**
	  * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: BLUED TO EMD-DF DEMO ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	  * Takes a raw wave files and turns into an EMD-DF file. If the file is already in EMD-DF format it is updated accordingly.
	  */
		 	 
		 // To test with an annotated EMD-DF file (i.e., mandatory and optional chunks)
		 //String file_name = "BLUED_A_emddf";
		 
		 // To test with a clean EMD-DF file (i.e., only mandatory chunks)
		 //String file_name = "BLUED_A_emddf_clean";
		
		// To test with an annotated EMD-DF file (i.e., mandatory and optional chunks)
		String file_name = "BLUED_A_raw";
		 
		 
		 // We use the Update Class to take care of this task
		 uA = new Update(0, path, file_name);
		 
		 // Add the mandatory Config File
		 float cc[] = new float[]{19200, 19200};
		 
		 uA.setConfig("2011-10-20 11:58:32.634", "EST", 60f, cc);
		 	 
		 // Add Labels
		 addLabels();
		 
		 // This adds a note in a specific position of the dataset file.
		 uA.addNote(84532, "{'ID':1001, 'Position':84532, 'Timestamp':'2011-10-20 12:22:01.000', "
		 		+ "'Text':'This is a note on a Refrigerator Event'}");
		 
		 // This adds to regions in the beginning of the dataset file.
		 String activity_text = "{'ID':10020, 'Activity_ID':111, "
					+ "'Activity_Label':'Working on the computer', "
					+ "'Start_Position':19394633, 'End_Position':19395633,"
					+ "'Start_Timestamp':'2011-10-24 05:45:57.040',"
					+ "'End_Timestamp': '2011-10-24 06:23:18.056',"
					+ "'Appliance_Activity_IDs':[1101, 1109, 1203],"
					+ "'Total_Power':10000}";
		 
		 uA.addRegion(19394633, 19395633, activity_text);
		 //uA.addRegion(84532, 140874, "The refrigerator was working between 2011-10-20 12:22:01 and 2011-10-20 12:37:40");
		 //uA.addRegion(307367, 362133, "The refrigerator was working AGAIN between 2011-10-20 13:23:55 and 2011-10-20 13:39:08");
		 
		 // Add the optional Info chunks
		 setInfo();
		 
		 // Example of adding some comments
		 uA.addComment("This file was created to demonstrate the conversion of BLUED to EMD-DF64.");
		 uA.addComment("For the original BLUED please refer to http:nilm.cmubi.org.");
		 
		 // Saves the changes. I.e., Generates a new file with the new information.
		 uA.saveChanges();
		 
		 System.out.println("Done");
		
	}
}
