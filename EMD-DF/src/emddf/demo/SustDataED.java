package emddf.demo;


import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import emddf.api.create.Merger;
import emddf.api.read.Reader;
import emddf.api.update.Update;


public class SustDataED {

	public static void main(String[] args) throws IOException, ProcessingException, ParseException {        
        
		
		String path = "assets/";
        //String path1 = "J:/";
		File folder = new File(path);
		File files[] = folder.listFiles();
		String fileName = files[0].getName().split(".w64")[0];
		//System.exit(0);
		//String fileName = "1446840524599";
		
		Reader r = new Reader(files[0]);
		
		Update u = new Update(5, path, fileName);
		
		HashMap<Long, Long> forbiddenRegions = new HashMap<Long, Long>();
		long numSamples = 0;
	
		int i = 0;
		for (final File fileEntry : files) {
	        // System.out.println(fileEntry.getName());
	        
			try {
				
				Reader read = new Reader(fileEntry);
				
				long timestamp = Long.parseLong(files[i+1].getName().split(".w64")[0]);
				System.out.println("Timestamp of file: "+timestamp);
				String date = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date (timestamp));
	
				numSamples += read.getNumFrames();
				
				//long startTimestamp = Long.parseLong(read.getTimestamp(numSamples)[1]);
				long startTimestamp = 0;
				
				forbiddenRegions.put(startTimestamp, timestamp);
				System.out.println("Regiao inadmissivel: ["+startTimestamp+","+timestamp+"]");
		        u.addMissingData(date, numSamples+1);  
		        
			} catch (IndexOutOfBoundsException e) {
				System.out.println(e.getMessage());
			}
			
	        i++;
	    }
		
				
		
		String name;
		Timestamp initialTimeStamp = null;
		long initialTimeStampInMiliSecs = 0;
   	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

        
        try {
       	   	    
	        String initial_timestamp = "2011-10-20 11:58:32.634";		         
	   	    java.util.Date parsedDate = dateFormat.parse(initial_timestamp);
	   	    initialTimeStamp = new java.sql.Timestamp(parsedDate.getTime());
	   	    initialTimeStampInMiliSecs = initialTimeStamp.getTime();
	   	    
	   	} catch(Exception e) { //this generic but you can control another types of exception
	   	    // look the origin of excption 
	   	}

        
        
        Timestamp TimeStamp;
        long TimeStampInMiliSecs = 0;
		
		
		
		Connection c = null;
		   Statement stmt = null;
		   try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:J:\\datasets\\SustDataED.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");
				int ki=0;
		      stmt = c.createStatement();
		      ResultSet rs = stmt.executeQuery( "Select * from plugwise_label, device where device.id = plugwise_label.device_id;" );
		      SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		      while ( rs.next() ) {
		    	  
		      	ki++;
		         int id = rs.getInt("id");
		         int  app_id = rs.getInt("device_id");
		         String app_label  = rs.getString("name");
		         //long position = Math.round((1000*(aSample - md.getInitialSample()))/getRate() + epoch*1000);
	         	 
		         String timestamp = rs.getString("timestamp");//tem que ser unix
		         long unixTimestamp =dateFormat2.parse(timestamp).getTime();
		         long position = Math.round(r.getRate()/1000*(unixTimestamp - dateFormat.parse(r.getInitialTimestamp(1).getInitialTimestamp()).getTime()+r.getInitialTimestamp(1).getInitialSample()));
		         System.out.println("Position is "+position);
		         float delta_p = rs.getFloat("delta");
		         int type = rs.getInt("event_type");
		         long epoch = dateFormat2.parse(timestamp).getTime();
		         boolean isToInclude = true;
		         for (Map.Entry<Long, Long> entry : forbiddenRegions.entrySet()) {
		        	 
		        	 if(epoch > entry.getKey() && epoch <= entry.getValue()) {
		        	 	isToInclude = false;
		        	 	break;
		        	 }	 
		         }
	         	 if(!isToInclude){
	         	 	System.out.println("Label " + epoch +" ignorada.");
	        	 	continue;
	        	 }
		        
 		         try {
		         			 	   	    
		 	   	    java.util.Date parsedDate = dateFormat.parse(timestamp);
		 	   	    TimeStamp = new java.sql.Timestamp(parsedDate.getTime());
		 	   	    TimeStampInMiliSecs = TimeStamp.getTime();
		 	   	    
		 	   	} catch(Exception e) { //this generic but you can control another types of exception
		 	   	    // look the origin of excption 
		 	   	}
		         

		         String labelName = "{\"SURF_ID\":" + id + ",\"App_ID\":" + app_id + ",\"App_Label\":\"" + app_label + "\",\"Position\":" + position + ",\"Timestamp\":\"" + timestamp + "\",\"Delta_P\": " + delta_p + ",\"Type\":" + type + "}";
		         
		        // System.out.println("a" + labelName);	 
		         u.addLabel(position, labelName);
		        	 
		         

		         
		      }
		      rs.close();
		      stmt.close();
		      c.close();
		   } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		   }
		
		System.out.println("Operation done successfully");
		
		
		u.saveChanges();
		
		Merger m = new Merger(files, "J:\\datasets\\SustDataPM_W64\\SUSTDATA_PM");
		System.out.println("Merging...");
		m.run();
		
	}

}
