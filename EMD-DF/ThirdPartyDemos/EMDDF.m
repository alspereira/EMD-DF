classdef EMDDF
    %EMDDF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Filepath
        JarLocation = "emddf_int.jar";
        folder_path
        filename
        typeOfFile
        option
    end
    
    methods
        function obj = EMDDF(Filepath)
            %EMDDF Construct an instance of this class
            %   Creates the object with the filepath to an EMDDF audiofile
            obj.Filepath = strrep(Filepath, "\", "\\");
            split_path =regexp(Filepath,filesep,'split');
            obj.filename = string(split_path(end));
            split_path(end) = [];  % remove last position
            obj.folder_path = strrep(strjoin(strcat(split_path,"\")), ' ', '');
            filename_arr = regexp(obj.filename,'\.','split');
            obj.typeOfFile = filename_arr(end);
            if(strcmp(obj.typeOfFile,'wav'))
                obj.option = 1;
            else
                if(strcmp(obj.typeOfFile,'w64'))
                obj.option =5;
                end
            end
        end
        
        function outputArg = getAll(obj,propertyt, sample)
            %GetAll Base method for all the reading functions
            if exist('sample','var')
                command = strcat('java -jar ', {' '}, obj.JarLocation, ' read ', {' "'}, obj.Filepath,{'" '},propertyt, {' '}, string(sample));
            else
                command = strcat('java -jar ', {' '}, obj.JarLocation, ' read ', {' "'}, obj.Filepath,{'" '},propertyt);
            end
            fprintf(command);
            [~, cmout] = system(command);
            outputArg = cmout;
        end
        
        function out = getMetadata(obj)
            %GetMetadata Returns all metadata in json.
            out = getAll(obj,'metadata');
        end
        function out = getLabels(obj)
            %GetLabels Returns all labels in json.
            out = getAll(obj,'labels');
        end
        function out = getComments(obj)
            %getComments Returns all comments in json.
            out = getAll(obj,'comments');
        end
        function out = getNotes(obj)
            %getNotes Returns all notes in json.
            out = getAll(obj,'notes');
        end
        function out = getInfo(obj)
            %getInfo Returns all info in json.
            out = getAll(obj,'info');
        end
        function out = getRegions(obj)
            %getInfo Returns all regions in json.
            out = getAll(obj,'regions');
        end
        function out = getSampleTimestamp(obj, sample)
            %getSampleTimestamp Returns the sample's timestamp in json.
            out = getAll(obj,'sample_to_timestamp',sample);
        end
        function out = createTemplateFile(obj,templateFileLocation)
            % CreateTemplateFile
            % Creates a template file called template.json with the
            % possible options an EMD-DF file can have.
            % NOTE:
            % You may remove the metadata you don't want,
            % but the info chunk must have all the keys
            fid = fopen(templateFileLocation, 'wt' );
            
            str = sprintf('{',...
                        '"labels":[',...
                        '{"ID":,"App_ID":,"App_Label":"","Timestamp":"2011-10-12 10:18:04.040","Position":,"Delta_P":"","Delta_Q":"","Type":},',...
                        '],',...
                        '"comments":[',...
                        '{"Content":""},',...
                        '],',...
                        '"region":[',...
                        '{"Start":,"Stop":,"Content":""},',...
                        '],',...
                        '"notes":[',...
                        '{"Position":,"Content":""},',...
                        '],',...
                        '"metadata":[',...
                        '{"Content":""},',...
                        '],',...
                        '"info":[',...
                        '{',...
                        '"archival_location": "",',...
                        '"file_creator":"",',...
                        '"comissioner":"",',...
                        '"comments":"",',...
                        '"copyright":"",',...
                        '"creation_date":"",',...
                        '"keywords":"",',...
                        '"product":"",',...
                        '"subject":"",',...
                        '"software":"",',...
                        '"source":"",',...
                        '"source_form":"",',...
                        '"filename":""',...
                        '}',...
                        ']',...
                        '}');
            sprintf(fid,str);
            fclose(fid);
        end
        function out = addChunks(obj, templatefile_path)
            % Adds new information according to `labels_path`
            % to the chunks of the soundfile.
            % templatefile_path should be the ABOSULTE PATH to json file according to the ``createTemplateFile`` method.
            
            % NOTE:
            % - Can only write in wave and wave64 files.
            
            templatefile_path = strrep(templatefile_path, "\", "\\");
            command = strcat('java -jar ',obj.JarLocation, ' update ', obj.folder_path,' ',obj.filename,' ',obj.option, ' ', templatefile_path);
            [~, cmout] = system(command);
            out = cmout;
        end
        
        
    end
end

