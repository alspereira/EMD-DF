% load the file
emddf_file = EMDDF("..\assets\BLUED_A_emddf.wav");

% display the file contents
display(emddf_file)

% get the timestamp of the first sample
initial_timestamp = getSampleTimestamp(emddf_file,1)

% get metadata from the default INFO chunk
%info = getInfo(emddf_file)
info = strsplit(getInfo(emddf_file),"\n")';

% get the labels, split and transpose
 labels = strsplit(getLabels(emddf_file),"\n")';
 
 % get comments if any
 comments = getComments(emddf_file);
 
 % get notes
 notes = getNotes(emddf_file);
 
 % get regions (user activity)
 activities = getRegions(emddf_file);
 
 
